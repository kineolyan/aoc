(ns aoc.day-24-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-24 :as c]))

(def test-input-1
  "sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew")

(deftest parsing
  (testing "input to seq"
    (is (= '(:se :sw :nw :ne :e :w :e)
           (c/input->seq "seswnwneewe")))))

(deftest geometry
  (testing "+"
    (is (= [3 -1]
           (c/hexa+ [0 1] [3 -2]))))
  
  (testing "getting z"
    (is (= -1 (c/hexa-z [0 1]))))
  
  (testing "after moves"
    (is (= [2 -1]
           (c/get-target-coordinate [:ne :ne :se])))))

(deftest evolution
  (testing "flipping tiles"
    (is (= {'(0 0) :black '(1 2) :black}
           (c/flip-tile {'(0 0) :black}
                        '(1 2))))
    (is (= {'(1 2) :black}
           (c/flip-tile {'(0 0) :black '(1 2) :black}
                        '(0 0))))))

(deftest solving
  (testing "part 1"
    (is (= 10 (c/solve test-input-1)))))
