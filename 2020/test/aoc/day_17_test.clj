(ns aoc.day-17-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-17 :as c]))

(defn make-grid
  [points]
  (into {} (map #(vector % :here) points)))

(deftest geometry
  (testing "adding points"
    (is (= (c/geo+ (c/->Point 1 2 3) (c/->Point 10 20 30))
           (c/->Point 11 22 33))))
  (testing "boundaries"
    (let [grid (make-grid [(c/->Point -1 10 0)
                  (c/->Point 10 20 -3)
                  (c/->Point 3 15 2)])]
      (is (= (c/get-boundaries grid :x)
             '(-1 10)))
      (is (= (c/get-boundaries grid :y)
             '(10 20)))
      (is (= (c/get-boundaries grid :z)
             '(-3 2))))))

(deftest parsing
  (testing "parse row"
    (is (= (c/parse-row 3 "#..#")
           [(c/->Point 0 3 0) (c/->Point 3 3 0)])))
  (testing "parse grid"
    (is (= (c/parse-grid "##.\n.#.\n...")
           {(c/->Point 0 0 0) :here
            (c/->Point 1 0 0) :here
            (c/->Point 1 1 0) :here}))))

(def test-input-1
  ".#.
..#
###")

(deftest evolution
  (testing "can activate"
    (let [grid (make-grid [(c/->Point 1 0 0)
                           (c/->Point 2 1 0)
                           (c/->Point 0 2 0)
                           (c/->Point 1 2 0)
                           (c/->Point 2 2 0)])]
      (is (c/activate? grid (c/->Point 1 3 1)))
      (is (c/activate? grid (c/->Point 1 3 -1)))
      (is (not (c/activate? grid (c/->Point -1 -1 -1))))))
  (testing "some input"
    (let [grid (make-grid [(c/->Point 1 0 0)
                           (c/->Point 2 1 0)
                           (c/->Point 0 2 0)
                           (c/->Point 1 2 0)
                           (c/->Point 2 2 0)])
          expected (make-grid [(c/->Point 0 1 -1); z = -1
                               (c/->Point 2 2 -1)
                               (c/->Point 1 3 -1)
                               (c/->Point 0 1 0); z = 0
                               (c/->Point 2 1 0)
                               (c/->Point 1 2 0)
                               (c/->Point 2 2 0)
                               (c/->Point 1 3 0)
                               (c/->Point 0 1 1); z = 1
                               (c/->Point 2 2 1)
                               (c/->Point 1 3 1)])]
      (is (= (c/evolve-grid grid) expected))))
  (testing "simulation"
    (let [grid (make-grid [(c/->Point 1 0 0)
                           (c/->Point 2 1 0)
                           (c/->Point 0 2 0)
                           (c/->Point 1 2 0)
                           (c/->Point 2 2 0)])
          expected (make-grid [(c/->Point 0 1 -1); z = -1
                               (c/->Point 2 2 -1)
                               (c/->Point 1 3 -1)
                               (c/->Point 0 1 0); z = 0
                               (c/->Point 2 1 0)
                               (c/->Point 1 2 0)
                               (c/->Point 2 2 0)
                               (c/->Point 1 3 0)
                               (c/->Point 0 1 1); z = 1
                               (c/->Point 2 2 1)
                               (c/->Point 1 3 1)])]
      (is (= (c/simulate grid 1) expected)))))

(comment
  (c/evolve-grid (c/parse-grid test-input-1))
  
  (c/simulate (c/parse-grid test-input-1) 1))

(deftest solving
  (is (= (c/solve test-input-1) 112)))
