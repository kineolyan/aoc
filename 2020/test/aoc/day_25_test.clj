(ns aoc.day-25-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-25 :as c]))

(deftest encrypt
  (testing "default"
    (is (= 5764801 (c/encrypt :subject 7 :turns 8)))
    (is (= 17807724 (c/encrypt :subject 7 :turns 11))))
  
  (testing "enc key"
    (is (= 14897079
           (c/encrypt :subject 5764801 :turns 11)
           (c/encrypt :subject 17807724 :turns 8))))
  
  (testing "hacking"
    (is (= 8 (c/find-turns :subject 7 :target 5764801)))
    (is (= 11 (c/find-turns :subject 7 :target 17807724)))))
