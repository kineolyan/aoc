(ns aoc.day-23-test
  (:require [clojure.string :as str]
            [clojure.test :refer [deftest is testing]]
            [aoc.day-23 :as c]))

(deftest parsing
  (testing "char to int"
    (is (= 1 (c/char->int \1)))
    (is (= 8 (c/char->int \8))))
  (testing "whole input"
    (is (= {3 1, 1 2, 2 3, :cup 3}
           (c/parse-input "312"))))
  
  (testing "input 2.0"
    (is (= {3 1, 1 2, 2 4, 4 5, 5 3, :cup 3}
         (c/parse-input-2 "312" 5)))))

(defn ->state
  [vs]
  (c/parse-input (str/join vs)))

(deftest play
  (testing "find destination"
    (is (= 9 (c/get-destination 1 '(8 7 3))))
    (is (= 7 (c/get-destination 2 '(9 1 8))))
    (is (= 5 (c/get-destination 6 '(9 8 1)))))
  (testing "turn"
    (is (= (->state '(2 8 9 1 5 4 6 7 3))
           (c/play-turn (->state '(3 8 9 1 2 5 4 6 7)))))
    (is (= (->state '(5 4 6 7 8 9 1 3 2))
           (c/play-turn (->state '(2 8 9 1 5 4 6 7 3)))))))

(deftest solving
  (testing "get answer"
    (is (= "23" (c/collect-answer {2 3, 3 1, 1 2, :cup 3}))))
  (testing "get answer 2.0"
    (is (= (* 27 42)
           (c/collect-answer-2 {:cup 1, 1 27, 27 42, 42 3, 3 1}))))
  (testing "part 1"
    (is (= "67384529" (c/solve "389125467"))))
  (comment 
    ; taking too long, we have to find an idea to speed process
    (testing "part 2"
      (is (= 149245887792 (c/solve-2 "389125467"))))))
