(ns aoc.day-22-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-22 :as c]))

(def test-input-1
  "Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10")

(deftest parsing
  (testing "whole input"
    (is (= {:self [9 2 6 3 1]
            :crab [5 8 4 7 10]}
           (c/parse-input test-input-1)))))

(deftest play
  (testing "resursion check"
    (is (c/recursive-deck? [1 2 3]))
    (is (c/recursive-deck? [3 2 1 0]))
    (is (not (c/recursive-deck? [3 2 1])))
    (is (not (c/recursive-deck? [9 8 7]))))
  (testing "recursion from state"
    (is (c/play-recursive? {:self [1 2 3]
                            :crab [4 5 6 7 8]}))
    (is (not (c/play-recursive? {:self [7]
                                 :crab [1 2 3]})))
    (is (not (c/play-recursive? {:self [1 2 3]
                                 :crab [7]}))))
  
  (testing "play turn"
    (is (= {:self [9 4 5 3]
            :crab [1 8]}
           (c/play-turn {:self [5 9 4]
                         :crab [3 1 8]})))
    (is (= {:self [2 1]
            :crab [8 7 6 5]}
           (c/play-turn {:self [5 2 1]
                         :crab [6 8 7]}))))
  
  (testing "whole game"
    (is (= {:self nil
            :crab [7 5 6 2 4 1 10 8 9 3]}
           (c/play-game {:self [9 2 6 3 1]
                    :crab [5 8 4 7 10]}))))
  
  (testing "get winner cards "
    (is (= [3 2 10 6 8 5 9 4 7 1]
           (c/get-winner-cards {:self nil
                                :crab [3 2 10 6 8 5 9 4 7 1]})))))

(deftest scoring
  (testing "compute"
    (is (= 306 (c/compute-score [3 2 10 6 8 5 9 4 7 1])))))

(deftest solving
  (testing "part 1"
    (is (= 291 (c/solve test-input-1)))))