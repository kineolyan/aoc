(ns aoc.day-14-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-14 :as c]))

(deftest parsing
  (testing "char->binary"
    (is (= (c/char->binary "1011") 11))
    (is (= (c/char->binary "11") 3))
    (is (= (c/char->binary "1") 1))
    (is (= (c/char->binary "10") 2))
    (is (= (c/char->binary "100") 4)))
  (testing "x-to"
    (is (= (c/x-to "XX10X" \1) (seq "11101")))
    (is (= (c/x-to "XX10X" \0) (seq "00100"))))
  (testing "mask"
    (is (= (c/parse-mask "mask = XX1X0X")
           {:base 2r001000
            :mask (- (bit-shift-left (long 1) 37) 1 2r110101)
            :vars [0 2 4 5]})))
  (testing "memory"
    (is (= (c/parse-memory "mem[8] = 11")
           {:slot 8 :value 11}))
    (is (= (c/parse-memory "mem[1] = -37")
           {:slot 1 :value -37})))
  (testing "line"
    (is (= (c/parse-line "mask = XXX10")
           {:base 2r10
            :mask (- (bit-shift-left (long 1) 37) 1 2r11100)
            :vars [2 3 4]}))
    (is (= (c/parse-line "mem[3] = 3")
           {:slot 3 :value 3}))))

(defn ->state
  [mask & kvs]
  {:mask (c/str->mask mask)
   :memory (apply hash-map kvs)})
(deftest execution
  (testing "get-addresses"
    (is (= (c/get-addresses (c/str->mask "0X1001X") 42)
           [26 27 58 59])))
  (testing "apply"
    (let [state (->state "XX1X0" 1 10)
          mask (:mask state)]
      (is (= (c/apply-instruction state (c/str->mask "0XX1"))
             (->state "0XX1" 1 10)))
      (is (= (c/apply-instruction state {:slot 2 :value 20})
             (->state "XX1X0" 1 2)))
      (is (= (c/apply-instruction state {:slot 1 :value 11})
             (->state "XX1X0" 1 2))))))

(deftest pb
  (testing "part 1"
    (let [input "mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1"]
      (is (= (c/solve input) 208)))))