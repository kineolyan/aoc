(ns aoc.day-21-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-21 :as c]))

(def test-input-1
  "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)")

(deftest parsing
  (testing "format parsed"
    (is (= {:ingredients #{"mxmxvkd" "kfcds" "sqjhc" "nhms"}
            :allergies #{"dairy" "fish"}}
           (c/format-parsed [:FOOD [:INGREDIENTS "mxmxvkd" "kfcds" "sqjhc" "nhms"] [:ALLERGIES "dairy" "fish"]]))))
  (testing "whole input"
    (is (= [{:ingredients #{"mxmxvkd" "kfcds" "sqjhc" "nhms"}
             :allergies #{"dairy" "fish"}}
            {:ingredients #{"trh" "fvjkl" "sbzzf" "mxmxvkd"}
             :allergies #{"dairy"}}
            {:ingredients #{ "sqjhc" "fvjkl"} :allergies #{"soy"}}
            {:ingredients #{"sqjhc" "mxmxvkd" "sbzzf"} :allergies #{"fish"}}]
           (c/parse-input test-input-1)))))

(deftest solving
  (testing "test solutions"
    (is (= 5 (c/solve test-input-1)))
    (is (= "mxmxvkd,sqjhc,fvjkl"
           (c/solve-2 test-input-1)))))

(comment
  (c/solve test-input-1)
  (c/input-parser test-input-1))