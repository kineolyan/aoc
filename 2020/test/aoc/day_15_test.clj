(ns aoc.day-15-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-15 :as c]))

(deftest util
  (testing "state"
    (is (= (c/create-state [1 3 2])
           {:last 2
            :memory {1 1
                     3 2}})))
  (testing "get-age"
    (is (= (c/get-age {1 3} 1 7) 3))
    (is (= (c/get-age {1 3} 2 10) 0)))
  (testing "play"
    (let [state-1 {:last 6
                   :memory {0 1
                            3 2}}]
      (is (= (c/play-one-turn state-1 4)
             {:last 0
              :memory {0 1
                       3 2
                       6 3}})))
    (let [state-2 {:last 0
                   :memory {0 1
                            3 2
                            6 3}}]
      (is (= (c/play-one-turn state-2 5)
             {:last 3
              :memory {0 4
                       3 2
                       6 3}})))))

(deftest solve
  (is (= (c/solve "0,3,6" 2020) 436))
  (is (= (c/solve "1,3,2" 2020) 1))
  (is (= (c/solve "0,5,4,1,10,14,7" 2020) 203)))