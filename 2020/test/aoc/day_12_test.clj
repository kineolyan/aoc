(ns day-12-test
  (:require [clojure.test :refer [deftest is testing]]
            [day-12 :as c]))

(deftest util
  (testing "manhattan"
    (is (= (c/manhattan 0 1) 1))
    (is (= (c/manhattan -4 5) 9)))
  (testing "rotate"
    (is (= (c/rotate-point {:lat -10 :lon 1})
           {:lat 1 :lon 10}))))

(deftest instructions
  (testing "moving"
    (is (= (c/parse-instruction "F10") [:forward 10])))
  (testing "rotate"
    (is (= (c/parse-instruction "R90") [:right 90]))
    (is (= (c/parse-instruction "R-180") [:right -180]))
    (is (= (c/parse-instruction "L1234") [:left 1234])))
  (testing "slide"
    (is (= (c/parse-instruction "N3") [:north 3]))
    (is (= (c/parse-instruction "S20") [:south 20]))
    (is (= (c/parse-instruction "E25") [:east 25]))
    (is (= (c/parse-instruction "W7") [:west 7]))))

(defn state [& {:keys [lon lat wlon wlat]}]
  {:position {:lon lon :lat lat}
   :waypoint {:lon wlon :lat wlat}})
(comment
  (c/move (state :lon 7 :lat -3 :wlon 1 :wlat -10) [:left 90]))
(deftest moves
  (testing "forward"
    (let [position (state :lon 7 :lat -3 :wlon 1 :wlat 10)]
      (is (= (c/move position [:forward 8])
             (state :lon 15 :lat 77 :wlon 1 :wlat 10)))
      (is (= (c/move position [:forward -3])
             (state :lon 4 :lat -33 :wlon 1 :wlat 10)))))

  (testing "rotate"
    (testing "left"
      (let [position (state :lon 7 :lat -3 :wlon 1 :wlat -10)]
        (is (= (c/move position [:left 90])
               (state :lon 7 :lat -3 :wlon -10 :wlat -1)))
        (is (= (c/move position [:left 180])
               (state :lon 7 :lat -3 :wlon -1 :wlat 10)))
        (is (= (c/move position [:left 270])
               (state :lon 7 :lat -3 :wlon 10 :wlat 1)))
        (is (= (c/move position [:left 360])
               (state :lon 7 :lat -3 :wlon 1 :wlat -10)))
        (is (= (c/move position [:left -90])
               (state :lon 7 :lat -3 :wlon 10 :wlat 1)))
        (is (= (c/move position [:left (+ 90 (* 4 360))])
               (state :lon 7 :lat -3 :wlon -10 :wlat -1))))))
  (testing "right"
    (let [position (state :lon 7 :lat -3 :wlon 1 :wlat -10)]
      (is (= (c/move position [:right 90])
             (state :lon 7 :lat -3 :wlon 10 :wlat 1)))
      (is (= (c/move position [:right 180])
             (state :lon 7 :lat -3 :wlon -1 :wlat 10)))
      (is (= (c/move position [:right 270])
             (state :lon 7 :lat -3 :wlon -10 :wlat -1)))
      (is (= (c/move position [:right 360])
             (state :lon 7 :lat -3 :wlon 1 :wlat -10)))
      (is (= (c/move position [:right -90])
             (state :lon 7 :lat -3 :wlon -10 :wlat -1)))
      (is (= (c/move position [:right (+ 90 (* 4 360))])
             (state :lon 7 :lat -3 :wlon 10 :wlat 1)))))

  (testing "slide"
    (let [position (state :lon 7 :lat -3 :wlon 1 :wlat -10)]
      (testing "north"
        (is (= (c/move position [:north 6])
               (state :lon 7 :lat -3 :wlon 7 :wlat -10)))
        (is (= (c/move position [:north -4])
               (state :lon 7 :lat -3 :wlon -3 :wlat -10))))
      (testing "south"
        (is (= (c/move position [:south 6])
               (state :lon 7 :lat -3 :wlon -5 :wlat -10)))
        (is (= (c/move position [:south -4])
               (state :lon 7 :lat -3 :wlon 5 :wlat -10))))
      (testing "east"
        (is (= (c/move position [:east 6])
               (state :lon 7 :lat -3 :wlon 1 :wlat -4)))
        (is (= (c/move position [:east -4])
               (state :lon 7 :lat -3 :wlon 1 :wlat -14))))
      (testing "west"
        (is (= (c/move position [:west 6])
               (state :lon 7 :lat -3 :wlon 1 :wlat -16)))
        (is (= (c/move position [:west -4])
               (state :lon 7 :lat -3 :wlon 1 :wlat -6)))))))

(deftest travel
  (let [instructions [[:forward 10]
                      [:north 3]
                      [:forward 7]
                      [:right 90]
                      [:forward 11]]]
    (is (= (c/travel (state :lon 0 :lat 0 :wlon 1 :wlat 10) instructions)
           (state :lon -72 :lat 214 :wlat 4 :wlon -10)))))

(def test-input "F10
N3
F7
R90
F11")
(deftest solve
  (is (= (c/solve (constantly test-input)) 286)))