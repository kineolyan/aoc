(ns aoc.day-16-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-16 :as c]))

(def test-input "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12")

(deftest parsing
  (testing "parse range"
    (is (= (c/parse-range "1-3") (c/->Range 1 3))))
  (testing "parse-ranges"
    (is (= (c/parse-ranges "1-3 or 5-7") [(c/->Range 1 3) (c/->Range 5 7)])))
  (testing "parse-class"
    (is (= (c/parse-class "first row: 6-11 or 33-44 or 17-18")
           {:class "first row"
            :ranges [(c/->Range 6 11) (c/->Range 33 44) (c/->Range 17 18)]})))
  (testing "parse ticket"
    (is (= (c/parse-ticket "7,1,14")
           '(7 1 14))))
  (testing "parse whole input"
    (is (= (c/parse-input test-input)
           {:conditions {"class" [(c/->Range 1 3) (c/->Range 5 7)]
                         "row" [(c/->Range 6 11) (c/->Range 33 44)]
                         "seat" [(c/->Range 13 40) (c/->Range 45 50)]}
            :ticket '(7 1 14)
            :others [[7 3 47]
                     [40 4 50]
                     [55 2 20]
                     [38 6 12]]}))))

(deftest check
  (testing "range check"
    (is (true? (c/in-range? (c/->Range 1 3) 1)))
    (is (true? (c/in-range? (c/->Range 1 3) 3)))
    (is (true? (c/in-range? (c/->Range 1 3) 2)))
    (is (false? (c/in-range? (c/->Range 1 3) 4)))
    (is (false? (c/in-range? (c/->Range 1 3) 0))))
  (testing "all ranges check"
    (let [ranges [(c/->Range 1 3) (c/->Range 5 7)]]
      (is (c/in-at-least-one-range? ranges 1))
      (is (c/in-at-least-one-range? ranges 6))
      (is (not (c/in-at-least-one-range? ranges 8)))))
  (testing "all conditions"
    (let [conditions {"class" [(c/->Range 1 3) (c/->Range 5 7)]
                      "row" [(c/->Range 6 11) (c/->Range 33 44)]
                      "seat" [(c/->Range 13 40) (c/->Range 45 50)]}]
      (is (c/valid? conditions 7))
      (is (not (c/valid? conditions 4)))))
  (testing "ticket check"
    (let [conditions {"class" [(c/->Range 1 3) (c/->Range 5 7)]
                      "row" [(c/->Range 6 11) (c/->Range 33 44)]
                      "seat" [(c/->Range 13 40) (c/->Range 45 50)]}]
      (is (empty? (c/check-ticket conditions '(7 1 14))))
      (is (= (c/check-ticket conditions '(40 4 50)) '(4)))
      (is (= (c/check-ticket conditions '(55 2 20)) '(55)))
      (is (= (c/check-ticket conditions '(38 6 12)) '(12)))))
  (testing "valid ticket"
    (let [conditions {"class" [(c/->Range 1 3) (c/->Range 5 7)]
                      "row" [(c/->Range 6 11) (c/->Range 33 44)]
                      "seat" [(c/->Range 13 40) (c/->Range 45 50)]}]
      (is (c/valid-ticket? conditions '(7 1 14)))
      (is (not (c/valid-ticket? conditions '(40 4 50))) '(4)))))

(def input-fields
  (into {}
        (map vector
             ["departure location"
              "departure station"
              "departure platform"
              "departure track"
              "departure date"
              "departure time"
              "arrival location"
              "arrival station"
              "arrival platform"
              "arrival track"
              "class"
              "duration"
              "price"
              "route"
              "row"
              "seat"
              "train"
              "type"
              "wagon"
              "zone"]
             (range))))

(def other-input "class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9")
(deftest field-detection
  (testing "departure fields"
    (is (= (c/get-departure-fields input-fields)
           {"departure location" 0
            "departure station" 1
            "departure platform" 2
            "departure track" 3
            "departure date" 4
            "departure time" 5})))
  (testing "get values"
    (is (=  (c/get-field-values {"a" 1 "c" 3 "b" 5} '(1 2 4 8 16 32 64 128))
            '(2 8 32))))
  (testing "candidate field"
    (let [ranges [(c/->Range 1 3) (c/->Range 5 7)]]
      (is (c/candidate-field? ranges '(1 2 3 5 6)))
      (is (not (c/candidate-field? ranges '(1 4 7))))))
  (testing "find candidates"
    (let [conditions {"class" [(c/->Range 1 3) (c/->Range 5 7)]
                      "row" [(c/->Range 6 11) (c/->Range 33 44)]
                      "seat" [(c/->Range 13 40) (c/->Range 45 50)]}]
      (is (= (c/find-candidate-fields conditions '(1 6))
             ["class"]))
      (is (= (c/find-candidate-fields conditions '(6))
             ["class" "row"]))
      (is (= (c/find-candidate-fields conditions '(49))
             ["seat"]))
      (is (= (c/find-candidate-fields conditions '(12))
             []))))
  (testing "find fields"
    (let [pb (c/parse-input other-input)
          tickets (c/get-valid-others pb)]
      (is (= (c/find-fields (:conditions pb) tickets)
             {"row" 0
              "class" 1
              "seat" 2})))))

(deftest solve
  (is (= (c/solve test-input) 71)))
