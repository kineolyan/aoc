(ns aoc.day-19-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-19 :as c]))

(def test-input-1
  "0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: \"a\"
5: \"b\"

ababbb
bababa
abbbab
aaabbb
aaaabbb")

(deftest parsing
  (testing "value"
    (is (= [:= \A]
           (c/parse-match-rule "\"A\""))))
  (testing "sequence"
    (is (= [:seq 4 5 1]
           (c/parse-seq-rule "4 5 1"))))
  (testing "parsing a rule"
    (is (= {0 [:seq 4 1 5]}
           (c/parse-rule {} "0: 4 1 5")))
    (is (= {0 [:eq \B]
            1 [:alt -2 -3]
            -2 [:seq 2 3]
            -3 [:seq 3 2]}
           (c/parse-rule {0 [:eq \B]} "1: 2 3 | 3 2"))))
  (testing "parsing rules"
    (is (= {:rules {0 [:= \A]}
            :messages ["abc" "cd"]}
           (c/parse-input "0: \"A\"\n\nabc\ncd"))))

  (testing "patching"
    (is (= {1 [:eq \B]
            8 [:alt -2 -3]
            11 [:alt -5 -6]
            -2 [:seq 42]
            -3 [:seq 42 8]
            -5 [:seq 42 31]
            -6 [:seq 42 11 31]}
           (c/patch-rules {1 [:eq \B]})))))

(deftest execution
  (testing "valid message"
    (let [{:keys [rules]} (c/parse-input test-input-1)]
      (is (c/valid-message? rules "ababbb")))))

(comment
  (let [{:keys [rules]} (c/parse-input test-input-1)]
    (is (c/valid-message? rules "ababbb"))))

(deftest solve
  (testing "part 1"
    (is (= 2 (c/solve test-input-1)))))

(comment
  ; with negative entries for parts of the alt branches
  {0 [:seq 4 1 5]
   1 [:alt -1 -2]
   -1 [:seq 2 3]
   -2 [:seq 3 2]
   2 [:alt -3 -4]
   -3 [:seq 4 4]
   -4 [:seq 5 5]
   3 [:alt -5 -6]
   -5 [:seq 4 5]
   -6 [:seq 5 4]
   4 [:= \a]
   5 [:= \b]}

  ; execution
  ; first execute the current rule
  ; failure -> false and no more stack
  ; success -> pop and continue
  ; alt -> create new branches
  ['(1 4 5) ; rules to execute after that
   '(2 4 5)])
