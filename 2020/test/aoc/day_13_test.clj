(ns day-13-test
  (:require [clojure.test :refer [deftest is testing]]
            [day-13 :as c]))

(def test-input "939
7,13,x,x,59,x,31,19")

(deftest input
  (testing "parse times"
    (is (= (c/parse-ids "7,13,x,x,59,x,31,19")
           [[0 7], [1 13], [4 59], [6 31], [7 19]])))

  (testing "parse input"
    (is (= (c/parse-input test-input)
           {:time 939
            :buses [[0 7], [1 13], [4 59], [6 31], [7 19]]}))))

(deftest times
  (testing "next time"
    (is (= (c/next-time-after {:time 939 :bus 7}) 945))
    (is (= (c/next-time-after {:time 939 :bus 13}) 949))
    (is (= (c/next-time-after {:time 939 :bus 59}) 944))
    ; exactly a divider of time
    (is (= (c/next-time-after {:time 940 :bus 20}) 940)))

  (testing "next bus"
    (is (= (c/find-next-bus (c/parse-input test-input))
           {:id 59 :at 944}))))

(deftest crt
  (testing "first-u"
    (is (= (c/first-u 35 3 1) 70))
    (is (= (c/first-u 21 5 1) 21))
    (is (= (c/first-u 15 7 1) 15)))
  (testing "solve-for-bus"
    (let [buses [[2 3] [3 5] [4 7]]]
      (is (= (c/solve-for-bus buses 0) (* 2 70)))
      (is (= (c/solve-for-bus buses 1) (* 3 21)))
      (is (= (c/solve-for-bus buses 2) (* 4 15)))))
  (testing "big+"
    (is (= (c/big+ [1 3 9]) (BigInteger/valueOf 13))))
  (testing "big-"
    (is (= (c/big- (BigInteger/valueOf 10) 3)
           (BigInteger/valueOf 7)))))

(deftest solve
  (is (= (c/solve test-input) 295)))

(deftest solve-times
  (is (= 1068781 (c/solve-times test-input)))
  (is (= 3417 (c/solve-times "0\n17,x,13,19")))
  (is (= 754018 (c/solve-times "0\n67,7,59,61")))
  (is (= 779210 (c/solve-times "0\n67,x,7,59,61")))
  (is (= 1261476 (c/solve-times "0\n67,7,x,59,61")))
  (is (= 1202161486 (c/solve-times "0\n1789,37,47,1889"))))
