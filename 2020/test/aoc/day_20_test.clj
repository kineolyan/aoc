(ns aoc.day-20-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-20 :as c]))

(deftest parsing
  (testing "row to number"
    (is (= 2r100 (c/str-array->number ["#" "." "."])))
    (is (= 2r101 (c/str-array->number ["." "." "#" "." "#"]))))
  
  (testing "format image"
    (let [parsed-image [:I
                        [:HEADER [:ID "3209"]]
                        [:IMAGE
                         [:ROW "." "#" "#" "#" "#" "#" "." "#" "." "."]
                         [:ROW "." "." "." "." "." "#" "#" "#" "." "."]
                         [:ROW "#" "." "." "." "." "." "." "." "." "#"]
                         [:ROW "." "." "." "." "#" "." "#" "." "." "."]
                         [:ROW "." "." "#" "." "." "." "#" "#" "#" "#"]
                         [:ROW "." "." "." "." "." "." "#" "." "." "."]
                         [:ROW "#" "." "." "." "." "." "." "." "." "#"]
                         [:ROW "#" "." "#" "#" "." "." "." "." "." "#"]
                         [:ROW "#" "." "." "#" "." "#" "." "." "." "."]
                         [:ROW "." "." "#" "#" "." "#" "#" "#" "." "#"]]]
          image (c/format-image parsed-image)]
      (is (= 3209 (:id image)))
      (is (= 2r111110100 (get-in image [:boundaries :f-top])))
      (is (= 2r10111110 (get-in image [:boundaries :b-top])))
      (is (= 2r1011101100 (get-in image [:boundaries :f-bottom])))
      (is (= 2r11011101 (get-in image [:boundaries :b-bottom])))
      (is (= 2r111000100 (get-in image [:boundaries :f-left])))
      (is (= 2r10001110 (get-in image [:boundaries :b-left])))
      (is (= 2r10101101 (get-in image [:boundaries :f-right])))
      (is (= 2r1011010100 (get-in image [:boundaries :b-right]))))))
