(ns aoc.day-18-test
  (:require [clojure.test :refer [deftest is testing]]
            [aoc.day-18 :as c]))

(def test-input-1
  "2 * 3 + (4 * 5)
5 + (8 * 3 + 9 + 3 * 4 * 3)")

(deftest parsing
  (testing "whole line"
    (is (= (c/parse-line "1 + 2 * 3 + 4 * 5 + 6")
           [1 \+ 2 \* 3 \+ 4 \* 5 \+ 6]))
    (is (= (c/parse-line "10 + (20 + 30)")
           [10 \+ :start 20 \+ 30 :end]))
    (is (= (c/parse-line "((31 + 42) * (53 + 67))")
           [:start :start 31 \+ 42 :end \* :start 53 \+ 67 :end :end])))

  (testing "whole input"
    (is (= (c/parse-input test-input-1)
           [[2 \* 3 \+ :start 4 \* 5 :end]
            [5 \+ :start 8 \* 3 \+ 9 \+ 3 \* 4 \* 3 :end]]))))

(deftest ast
  (testing "building"
    (is (= [3 + 2]
           (c/build-ast [2 \+ 3])))
    (is (= [[4 * 3] + 2]
           (c/build-ast [2 \+ :start 3 \* 4 :end]))))
  (testing "sorting ops"
    (is (= [1 + 2 + 3]
           (c/sort-ast-level [1 + 2 + 3])))
    (is (= [1 * 2]
           (c/sort-ast-level [1 * 2])))
    (is (= [1 * 2 * 3]
           (c/sort-ast-level [1 * 2 * 3])))
    (is (= [3 * [2 + 1]]
           (c/sort-ast-level [1 + 2 * 3])))
    (is (= [[4 + 1] * [3 + 2 + 1]]
           (c/sort-ast-level [1 + 2 + 3 * 1 + 4])))
    (is (= [4 * [[1 * 2] + 6]]
           (c/sort-ast-level [6 + [1 * 2] * 4]))))
  (testing "rewrite"
    (let [b-r (comp c/rewrite-ast c/build-ast)]
      (is (= [2 * [4 + 3]]
             (b-r [2 \* 4 \+ 3])))
      (is (= [[2 * [4 + 3]] + 1]
             (b-r [1 \+ :start 2 \* 4 \+ 3 :end]))))))

(comment
  (->  [2 \* 4 \+ 3] c/build-ast c/rewrite-ast))

(deftest computing
  (testing "compute"
    (testing "simple formulae"
      (is (= 5 (c/compute [2 + 3])))
      (is (= 20 (c/compute [2 + 3 * 4]))))
    (testing "with parens"
      (is (= 14 (c/compute [2 + [3 * 4]])))))

  (testing "formula"
    (is (= 51 (c/solve-line "1 + (2 * 3) + (4 * (5 + 6))")))
    (is (= 46 (c/solve-line "2 * 3 + (4 * 5)")))
    (is (= 669060 (c/solve-line "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")))
    (is (= 23340 (c/solve-line "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")))))

(comment
  (def i "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")
  (-> i (c/parse-line) (c/build-ast) c/rewrite-ast))