(ns aoc.day-18
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(def operators #{\+ \*})

(defn collection?
  [s]
  (or (seq? s) (vector? s)))

(defn parse-part
  [input]
  (cond
    (= \( (first input)) (cons :start (parse-part (rest input)))
    (= \) (last input)) (lazy-cat (parse-part (drop-last input))
                                  [:end])
    (contains? operators (first input)) (do (when (> (count input) 1)
                                              (throw (IllegalStateException. "oops")))
                                            (list (first input)))
    :else (list (Integer/parseInt (apply str input)))))

(defn parse-line
  [input]
  (let [spaced-parts (str/split input #"\s+")
        parts (map parse-part spaced-parts)]
    (flatten parts)))

(defn parse-input
  [content]
  (map parse-line (str/split-lines content)))


(defn build-ast
  ([ops] (build-ast ops '()))
  ([[op & more] stack]
   (case op
     nil stack ; end of the recursion
     \+ (build-ast more (cons + stack))
     \* (build-ast more (cons * stack))
     :start (build-ast more (cons :paren stack))
     :end (let [[paren-expr stack] (split-with (partial not= :paren) stack)]
            (build-ast more (cons paren-expr (rest stack))))
     ; default applies to number, \+ and \*, and functions
     (build-ast more (cons op stack)))))

(defmulti write-number (fn [before _ after _] [before after]))
(defmethod write-number [* *]
  [_ v _ array]
  (conj array v))
(defmethod write-number [+ +]
  [_ v _ array]
  (conj array v))
(defmethod write-number [* +]
  [_ v _ array]
  ; start grouping additions
  (-> array (conj :start) (conj v)))
(defmethod write-number [+ *]
  [_ v _ array]
  ; end addition group
  (-> array (conj v) (conj :end)))
(defmethod write-number [+ nil]
  [_ v _ array]
  (-> array (conj v) (conj :end)))
(defmethod write-number [* nil]
  [_ v _ array]
  (conj array v))

(defn sort-ast-level
  ([ast]
   (let [operators (filter fn? ast)]
     (cond
       (= (count operators) 1) ast
       (every? (partial = (first operators)) (rest operators)) ast
       :else (sort-ast-level ast operators))))
  ([ast operators]
   (loop [[node & more-nodes] (drop 2 ast)
          [next-op & more-ops :as ops] (drop 1 operators)
          last-op (first operators)
          rewrite (if (= + (first operators))
                    (into [:start] (take 2 ast))
                    (into [] (take 2 ast)))]
     (cond
       (nil? node) (do
                    ;;  (println rewrite)
                     (build-ast rewrite))
       (fn? node) (recur more-nodes
                         more-ops
                         node
                         (conj rewrite node))
       :else (recur more-nodes
                    ops
                    last-op
                    (write-number last-op node next-op rewrite))))))
(defn debug-formula
  [fs]
  (let [ss (map #(if (collection? %) "l" %) fs)]
    (apply str (interleave ss (repeat " ")))))
(defn rewrite-ast
  [ast]
  (if (collection? ast)
    (let [after (sort-ast-level (map rewrite-ast ast))]
      ;; (println (debug-formula ast))
      ;; (println (debug-formula after))
      ;; (println "----------")
      after)
    ast))

(defn compute
  ([ops] (first (compute ops '())))
  ([[op & more] stack]
   (cond
     (nil? op) stack ; end of the computation
     (fn? op) (compute more (cons op stack))
     (collection? op) (compute (cons (compute op) more) stack)
     (empty? stack) (compute more (cons op stack))
     :else (let [[f v & stack] stack]
             (compute more (cons (f v op) stack))))))

(defn solve-line
  [line]
  (->> (parse-line line)
       (build-ast)
       (rewrite-ast)
       (compute)))

(defn solve
  [content]
  (->> (str/split-lines content)
       (map solve-line)
       (apply +)))

(comment
  (solve (util/read-input 18)))