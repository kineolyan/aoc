(ns aoc.day-23
  (:require [clojure.string :as str]
            [clj-async-profiler.core :as prof]))

(def char-zero (int \0))
(defn char->int
  [c]
  (- (int c) char-zero))

(defn parse-input
  [content]
  (let [vs (map char->int content)]
    (into {:cup (first vs)} (map vector vs (rest (cycle vs))))))

(defn parse-input-2
  [content max-range]
  (let [values (parse-input content)
        max-value (apply max (vals values))
        first-value (char->int (first content))
        last-value (char->int (last content))]
    (reduce
     #(assoc %1 %2 (inc %2))
     (-> values
         (assoc max-range first-value)
         (assoc last-value (inc max-value)))
     (range (inc max-value) max-range))))

(defn walk-cups
  [state from]
  (->> (iterate (partial get state) from)
       (drop 1)))

(defn pick-cups
  [state]
  (take 3 (walk-cups state (:cup state))))

(defn get-destination
  [current pick-up]
  {:post [(not= % current) (some? %)]}
  (let [pred (complement (into #{} pick-up))]
    (->> (concat (range (dec current) 0 -1)
                 (range 9 current -1))
         (filter pred)
         (first))))

(defn move-cups
  [state pick-up destination]
  (let [next-cup (get state (last pick-up))
        post-destination (get state destination)]
    (-> state
        (assoc (:cup state) next-cup)
        (assoc destination (first pick-up))
        (assoc (last pick-up) post-destination)
        (assoc :cup next-cup))))

(defn play-turn
  [state]
  (let [current-cup (:cup state)
        pick-up (pick-cups state)
        destination (get-destination current-cup pick-up)]
    (move-cups state pick-up destination)))

(defn play
  [state turns]
  (->> state
       (iterate play-turn)
       (drop turns)
       (first)))

(defn collect-answer
  [state]
  (->> (walk-cups state 1)
       (take (- (count state) 2))
       (str/join)))

(defn collect-answer-2
  [state]
  (let [first-value (get state 1)
        second-value (get state first-value)]
    (* first-value second-value)))

(defn solve
  [content]
  (-> content
      (parse-input)
      (play 100)
      (collect-answer)))

(defn solve-2
  ([content] (solve-2 content 1000000))
  ([content n]
   (println (str "testing with " n))
   (-> content
       (parse-input-2 n)
       (play (* 10 n))
       (collect-answer-2))))

(comment
  (def my-input "364297581")
  (solve my-input)
  (time (solve-2 my-input 50000)) 
  (prof/profile 
   (solve-2 my-input 1000000))
  (prof/serve-files 8080))