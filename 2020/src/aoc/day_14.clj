(ns aoc.day-14
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(defn char->binary
  [value]
  (->> value
       (map #(if (= \0 %) 0 1))
       (reduce #(+ %2 (* 2 %1)) (long 0))))

(defn x-to
  [value c]
  (map #(if (= \X %) c %) value))

(defn collect-vars
  [value]
  (->> (reverse value)
       (map-indexed vector)
       (filter #(= \X (second %)))
       (map first)))

(defn create-base
  [xs]
  (reduce
   #(- %1 (bit-shift-left (long 1) %2))
   (dec (bit-shift-left (long 1) 37))
   xs))

(defn str->mask
  [value]
  (let [vars (collect-vars value)]
    {:base (-> value (x-to \0) char->binary)
     :mask (create-base vars)
     :vars vars}))

(defn parse-mask
  [line]
  ; remove the part "mask = "
  (str->mask (.substring line 7)))

(defn parse-memory
  [line]
  (let [[_ slot value] (re-find #"mem\[(\d+)] = (-?\d+)" line)]
    {:slot (Integer/parseInt slot)
     :value (Long/parseLong value)}))

(defn parse-line
  [line]
  (case (.charAt line 1)
    \a (parse-mask line)
    \e (parse-memory line)))

(defn build-addresses
  [[p & more] vs]
  (if p
    (let [increment (bit-shift-left (long 1) p)
          with-increment (map (partial + increment) vs)]
      (lazy-cat (build-addresses more with-increment)
                (build-addresses more vs)))
    vs))
(defn get-addresses
  [{:keys [base mask vars]} slot]
  (build-addresses vars (list (bit-and mask (bit-or base slot)))))

(defn magic-mask
  [memory mask {:keys [slot value]}]
  (let [addresses (get-addresses mask slot)]
    (reduce
     #(assoc %1 %2 value)
     memory
     addresses)))

(defmulti apply-instruction
  (fn [_ instruction]
    (if (:slot instruction) :mem :mask)))
(defmethod apply-instruction :mask
  [state mask]
  (assoc state :mask mask))
(defmethod apply-instruction :mem
  [{:keys [mask] :as state} instruction]
  (update state :memory magic-mask mask instruction))

(defn execute
  [instructions]
  (let [state {:memory {}}]
    (reduce apply-instruction state instructions)))

(defn sum-memory
  [{:keys [memory]}]
  (apply + (vals memory)))

(defn solve
  [content]
  (->> (str/split-lines content)
       (map parse-line)
       (execute)
       (sum-memory)))

(comment
  (solve (util/read-input 14))

  (->> (util/read-input 14)
       (str/split-lines)
       (filter #(str/starts-with? % "mask"))
       (map #(filter (partial = \X) %))
       (map count)))