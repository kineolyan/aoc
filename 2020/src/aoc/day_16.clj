(ns aoc.day-16
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(defrecord Range [from to])

(defn parse-range
  [input]
  (let [[from to] (map #(Integer/parseInt %) (str/split input #"-"))]
    (Range. from to)))

(defn parse-ranges
  [input]
  (->> (str/split input #" or ")
       (map parse-range)))

(defn parse-class
  [input]
  (let [[class-name ranges] (str/split input #"\s?:\s?")]
    {:class class-name
     :ranges (parse-ranges ranges)}))

(defn parse-ticket
  [input]
  (into [] (util/parse-ints input #(str/split % #","))))

(defn get-parts
  [input]
  (let [rows (str/split-lines input)
        [conditions more] (split-with (complement str/blank?) rows)
        [ticket more] (split-with (complement str/blank?) (drop 2 more)) ;; empty + "your ticket"
        others (drop 2 more)] ;; empty + "nearby tickets"
    (list conditions ticket others)))
(defn parse-input
  [input]
  (let [[conditions ticket others] (get-parts input)]
    {:conditions (->> (map parse-class conditions)
                      (map (partial (juxt :class :ranges)))
                      (into {}))
     :ticket (parse-ticket (first ticket))
     :others (map parse-ticket others)}))

(defn in-range?
  [{:keys [from to]} value]
  (<= from value to))
(defn in-at-least-one-range?
  [ranges value]
  (some #(in-range? % value) ranges))
(defn valid?
  [conditions value]
  (some #(in-at-least-one-range? % value) (vals conditions)))
(defn check-ticket
  [conditions ticket]
  (filter #(not (valid? conditions %)) ticket))

(defn collect-errors
  [{:keys [conditions others]}]
  (->> (map (partial check-ticket conditions) others)
       (flatten)))

(defn valid-ticket?
  [conditions ticket]
  (empty? (check-ticket conditions ticket)))

(defn get-valid-others
  [{:keys [conditions others]}]
  (filter (partial valid-ticket? conditions) others))

(defn solve
  [input]
  (let [pb (parse-input input)
        errors (collect-errors pb)]
    (apply + errors)))

(defn candidate-field?
  [ranges vs]
  (every? (partial in-at-least-one-range? ranges) vs))

(defn find-candidate-fields
  [conditions vs]
  (->> conditions
       (filter #(candidate-field? (second %) vs))
       (map first)))

(defn get-nth-value
  [n tickets]
  (map #(nth % n) tickets))

(defn find-fields
  [conditions tickets]
  (let [indexes ((comp range count first) tickets)
        candidates (map #(vector % (find-candidate-fields conditions (get-nth-value % tickets))) indexes)]
    (loop [[[i names :as entry] & more] candidates
           mapping {}
           found #{}
           it 0]
      (cond
        (> it 10000) (throw (RuntimeException. "oops"))
        (nil? i) mapping ;; the end
        :else (let [[first-name & more-names] (filter (complement found) names)]
                (if (seq more-names)
                  ;; too many candidates, wait one more turn
                  (recur (concat more (list entry))
                         mapping
                         found
                         (inc it))
                  (recur more
                         (assoc mapping first-name i)
                         (conj found first-name)
                         (inc it))))))))

(comment
  (def test-input "class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9")
  (let [pb (parse-input test-input)
        tickets (get-valid-others pb)]
    (find-fields (:conditions pb) tickets)))

(defn get-departure-fields
  [fields]
  (->> fields
       (filter #(str/starts-with? (first %) "departure "))
       (into {})))

(defn get-field-values
  [fields ticket]
  (map (partial nth ticket) (vals fields)))

(defn solve-2
  [input]
  (let [pb (parse-input input)
        valid-others (get-valid-others pb)
        fields (find-fields (:conditions pb) valid-others)
        wanted-fields (get-departure-fields fields)
        wanted-values (get-field-values wanted-fields (:ticket pb))]
    (apply * wanted-values)))

(comment
  (solve-2 (util/read-input 16)))
