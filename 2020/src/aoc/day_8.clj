(ns day-8
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(defrecord Mem [pos acc])

(def test-input (str/split-lines "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"))

(defn str->instruction
  [row]
  (let [[op value] (str/split row #"\s+")]
    {:op op :value (Integer/parseInt value)}))
(str->instruction "acc +1")

(defn execute-instruction
  [instructions {:keys [pos acc]}]
  (let [{:keys [op value]} (nth instructions pos)]
    (case op
      "acc" (Mem. (inc pos) (+ acc value))
      "nop" (Mem. (inc pos) acc)
      "jmp" (Mem. (+ value pos) acc))))
(defn execute-from
  [instructions start-pos]
  (let [size (count instructions)]
    (loop [{:keys [pos acc] :as mem} (Mem. start-pos 0)
           executed #{}
           cc 0]
      ;; (println mem)
      (cond
        (= pos size) [:ok acc]
        (executed pos) [:loop acc]
        (> cc size) (throw (IllegalStateException. "oops. Infinite loop"))
        :else (recur (execute-instruction instructions mem)
                     (conj executed pos)
                     (inc cc))))))
(defn execute
  [instructions]
  (execute-from instructions 0))

(defn solve
  [f]
  (->> (f)
       (mapv str->instruction)
       (execute)
       (second))) ; will always be in an infinite loop
(solve (constantly test-input))
(solve (partial util/read-input-lines 8))

(defn map-execution
  [instructions]
  (mapv (partial execute-from instructions) (range (count instructions))))
(map-execution (mapv str->instruction test-input))

(defn is-working-from
  [executions pos]
  (let [[state shift] (nth executions pos)]
    (case state
      :ok shift
      :loop nil)))

(defn patch
  [instructions]
  (let [executions (map-execution instructions)]
    (loop [{:keys [acc pos] :as mem} (Mem. 0 0)]
      (let [{:keys [op value]} (nth instructions pos)]
        (case op
          "acc" (recur (execute-instruction instructions mem))
          "jmp" (if-let [shift (is-working-from executions (inc pos))]
                  (+ acc shift)
                  (recur (execute-instruction instructions mem)))
          "nop" (if-let [shift (is-working-from executions (+ pos value))]
                  (+ acc shift)
                  (recur (execute-instruction instructions mem))))))))

(defn solve-2
  [f]
  (->> (f)
       (mapv str->instruction)
       (patch)))
(solve-2 (constantly test-input))
(solve-2 (partial util/read-input-lines 8))