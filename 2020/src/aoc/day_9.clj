(ns aoc.day-9
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(defn parse-input
  [f]
  (->> (f)
       (str/split-lines)
       (map #(Long/parseLong %))))

(def input-data "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576")
(def test-input (parse-input (constantly input-data)))

(defn combinations
  [values]
  (->> (iterate (fn [[[v & vs] _]] [vs (map (partial + v) vs)]) (list values))
       (drop 1)
       (take-while (comp some? first))
       (map second)
       (flatten)))
(combinations (range 1 6))

(defn valid?
  [n value-range]
  (some (partial = n) (combinations value-range)))
(defn invalid? [n value-range] (not (valid? n value-range)))

(comment 
  (valid? 3 (range 1 6))
  (valid? 1 (range 1 6)))

(defn find-error
  [preamble-size vs]
  (->> (reverse vs)
       (partition (inc preamble-size) 1)
       (reverse)
       (filter #(invalid? (first %) (rest %)))
       (ffirst)))

(defn solve-1
  [preamble-size f]
  (find-error preamble-size (parse-input f)))
(solve-1 5 (constantly input-data))
(solve-1 25 (partial util/read-input 9))

(defn fsecond
  [vs]
  (second (first vs)))
(defn find-magic-slice
  [values needle]
  (loop [low (map-indexed #(vector %2 %1) values)
         high low
         total 0]
    (cond
      (= total needle) {:start (fsecond low) :end (or (fsecond high) (count values))}
      (< total needle) (recur low
                              (rest high)
                              (+ total (ffirst high)))
      (> total needle) (recur (rest low)
                              high
                              (- total (ffirst low))))))
(find-magic-slice test-input 127)

(defn solve-2
  [preamble-size f]
  (let [values (parse-input f)
        error (find-error preamble-size values)
        {:keys [start end]} (find-magic-slice values error)
        slice (->> values (drop start) (take (- end start)))]
    (+ (apply max slice) (apply min slice))))
(solve-2 5 (constantly input-data))
(solve-2 25 (partial util/read-input 9))

