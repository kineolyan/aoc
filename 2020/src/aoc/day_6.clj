(ns day-6
  (:require [clojure.string :as str]
            [clojure.set :as set]))

(def test-input "abc

a
b
c

ab
ac

a
a
a
a

b")

(defn read-groups
  [rows]
  (lazy-seq
   (when (seq rows)
     (let [[entries more] (split-with (complement str/blank?) rows)]
       (cons entries (read-groups (rest more)))))))
(take 7 (read-groups (str/split-lines test-input)))

(defn row->set
  [set]
  (into #{} set))

(defn get-group-answers
  [f group]
  (apply f (map row->set group)))

(defn solve
  [f check]
  (->> (f)
       (str/split-lines)
       (read-groups)
       (map (partial get-group-answers check))
       (map count)
       (apply +)))
(solve (constantly test-input) set/union)
(solve (constantly test-input) set/intersection)

(defn read-input
  []
  (slurp "/home/oliv/tmp/input-6.txt"))
(solve read-input set/union)
(solve read-input set/intersection)