(ns day-1
  (:require [clojure.string :as str]))

(defn input
  []
  (let [content (slurp "/home/olivier/tmp/input-1.txt")
        values (->> content (str/split-lines) (map #(Integer/parseInt %)))]
    values))

(defn pairs
  [values]
  (let [vs (into [] values)
        idxs (range (count vs))]
    (for [x idxs y idxs :when (< x y)]
      [(nth vs x) (nth vs y)])))

(defn triplets
  [values]
  (let [vs (into [] values)
        idxs (range (count vs))]
    (for [x idxs y idxs z idxs :when (< x y z)]
      [(nth vs x) (nth vs y) (nth vs z)])))

(defn valid-pairs
  [pairs]
  (filter #(= 2020 (apply + %)) pairs))

(def test-inputs [1721
                  979
                  366
                  299
                  675
                  1456])

(-> test-inputs pairs valid-pairs)
(-> test-inputs triplets valid-pairs)

(defn expense
  [values]
  (let [entry (first values)]
    (apply * entry)))

(expense test-inputs)

(defn run
  [f]
  (-> (input)
      (f)
      (valid-pairs)
      (expense)))

(run pairs)
(run triplets)
