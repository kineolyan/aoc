(ns day-5
  (:require [clojure.string :as str]))

(def test-input ["FBFBBFFRLR"])

(defn char->bin
  [c]
  (case c
    \F 0
    \L 0
    \R 1
    \B 1))
(defn compute-seat
  [label]
  (->> label
       (map char->bin)
       (reduce #(+ (* 2 %1) %2))))
(compute-seat (first test-input))

(defn solve [f]
  (->> (f)
       (map compute-seat)
       (apply max)))
(solve (constantly test-input))

(defn read-input
  []
  (str/split-lines (slurp "/home/oliv/tmp/input-5.txt")))
(solve read-input)

(defn has-around-seats
  [seats seat]
  (and   (seats (inc seat)) (seats (dec seat))))
(has-around-seats (into #{} (range 12)) 135)

(defn find-seat
  [f]
  (let [seats (into #{} (map compute-seat (f)))
        max-seat (inc (apply max seats))
        missing (filter (complement seats) (range max-seat))
        with-adjs (filter (partial has-around-seats seats) missing)]
    (first with-adjs)))

(find-seat read-input)