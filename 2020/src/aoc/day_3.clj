(ns aoc.day-3
  (:require [clojure.string :as str]))

(def test-input "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#")

(defn tree?
  [row n]
  (let [i (mod n (count row))
        c (nth row i)]
    (= c \#)))

(def moves (iterate (partial + 3) 0))

(defn solve
  [f]
  (let [rows (str/split-lines (f))
        trees (map tree? rows moves)]
    (count (filter true? trees))))

(solve (constantly test-input))
