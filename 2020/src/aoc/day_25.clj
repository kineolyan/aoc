(ns aoc.day-25)

(defn transform
  [subject value]
  (-> value
      (* subject)
      (rem 20201227)))

(defn encrypt
  [& {:keys [subject turns]}]
  (->> 1
       (iterate (partial transform subject))
       (drop turns)
       (first)))

(defn find-turns
  [& {:keys [target subject]}]
  (loop [turns 0
         value 1]
    (if (= value target)
      turns
      (recur (inc turns) (transform subject value)))))

(comment
  (def my-input {:card 9789649 :door 3647239})
  (def secrets {:card (find-turns :subject 7 :target (:card my-input))
                :door (find-turns :subject 7 :target (:door my-input))})
  (def enc-key (encrypt :subject (:card my-input) :turns (:door secrets))))
