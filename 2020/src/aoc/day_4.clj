(ns aoc.day-4
  (:require [clojure.string :as str]))

(def test-input "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in")

(def required-fields
  #{"byr"
    "iyr"
    "eyr"
    "hgt"
    "hcl"
    "ecl"
    "pid"}) ;; not including cid, as it is optional

(defn valid?
  [passport]
  (every? (partial get passport) required-fields))

(valid? {"byr" 1 "hcl" 2})

(defn do-split
  [lines]
  (split-with (complement str/blank?) lines))
(defn split-entries
  [input]
  (loop [groups []
         [lines more] (do-split (str/split-lines input))]
    (if (seq lines)
      (recur (conj groups lines)
             (do-split (rest more)))
      groups)))

(defn parse-passport
  [rows]
  (->> rows
       (map #(str/split % #" "))
       (flatten)
       (map #(str/split % #":"))
       (into {})))

(parse-passport ["a:1 b:2" "c:4"])

(defn read-passports
  [input]
  (->> input split-entries (map parse-passport)))

(read-passports test-input)

(defn solve
  [f]
  (->> (f)
       (read-passports)
       (filter valid?)
       (count)))

(solve (constantly test-input))

(defn read-input [] (slurp "/Users/olivier/tmp/input-4.txt"))
(solve read-input)