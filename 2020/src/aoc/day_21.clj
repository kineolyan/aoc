(ns aoc.day-21
  (:require [clojure.set :as set]
            [clojure.string :as str]
            [instaparse.core :as insta]
            [aoc.aoc :as util]))

(def input-parser
  (insta/parser 
   "<S> = FOOD+
    NL = '\n'
    FOOD = INGREDIENTS <'(contains'> ALLERGIES <')'> <NL?>
    INGREDIENTS = INGREDIENT+
    <INGREDIENT> = NAME <' '>
    <NAME> = #'[a-z]+'
    ALLERGIES = ALLERGIE+
    <ALLERGIE> = <' '> NAME <','?>"))

(defn format-parsed
  [parsed]
  {:ingredients (->> parsed second rest (into #{}))
   :allergies (->> parsed last rest (into #{}))})

(defn parse-input
  [content]
  (mapv format-parsed (input-parser content)))

(defn index-food-allergens
  [store [position {:keys [allergies]}]]
  (reduce
   #(update %1 %2 conj position)
   store
   allergies))

(defn index-allergens
  "Creates an index returning the food positions in which they are used."
  [parsed]
  (reduce 
   index-food-allergens
   {}
   (map-indexed vector parsed)))

(defn find-candidates
  [ingredient kit index]
  (->> (get index ingredient)
       (map #(:ingredients (nth kit %)))
       (reduce set/intersection)))

(defn find-allergen-candidates
  [kit index]
  (reduce
   #(if-let [candidates (seq (find-candidates %2 kit index))]
      (conj %1 [%2 candidates])
      %1)
   '()
   (keys index)))

(defn find-allergens
  [kit index]
  (loop [found #{}
         result {}
         [[allergen candidates] & more] (find-allergen-candidates kit index)
         it 0]
    (when (> it 10000) 
      (print "Warning: unstable list of allergens: ")
      (println result))
    (if (or (nil? allergen) (> it 10000))
      result
      (let [remaining (filter (complement found) candidates)]
        (case (count remaining)
          0 ; not a candidate anymore
          (recur found
                 result
                 more
                 (inc it))
          1 ; hurray, we found something
          (recur (conj found (first remaining))
                 (assoc result allergen remaining)
                 more
                 (inc it))
          ; no luck, wait for one more turn
          (recur found
                 result
                 (lazy-cat more (list (list allergen remaining)))
                 (inc it)))))))

(defn list-unsafe
  "List all unsafe ingredients for the list of allergen candidates"
  [allergens]
  (into #{} (flatten (vals allergens))))

(defn count-safe-ingredients
  [kit unsafe]
  (->> (map :ingredients kit)
       (map (partial into '()))
       (flatten)
       (filter (complement unsafe))
       (count)))

(defn get-dangerous-list
  [allergens]
  (->> allergens
       (sort-by first)
       (map (comp first second))
       (str/join ",")))

(defn solve
  [content]
  (let [survival-kit (parse-input content)
        index (index-allergens survival-kit)
        allergens (find-allergens survival-kit index)
        unsafe-ingredients (list-unsafe allergens)]
    (println allergens)
    (count-safe-ingredients survival-kit unsafe-ingredients)))

(defn solve-2
  [content]
  (let [survival-kit (parse-input content)
        index (index-allergens survival-kit)
        allergens (find-allergens survival-kit index)]
    (get-dangerous-list allergens)))

(comment
  (solve (util/read-input 21))
  (solve-2 (util/read-input 21)))
