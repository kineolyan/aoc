(ns aoc.day-24
  (:require [clojure.string :as str]
            [aoc.aoc :as util]
            [aoc.day-17 :as d17]))

(defn input->seq
  [input]
  (let [[c & cs] input]
    (case c
      nil '()
      \e (lazy-seq (cons :e (input->seq cs)))
      \w (lazy-seq (cons :w (input->seq cs)))
      \n (let [[c' & cs] cs] 
           (case c'
            \e (lazy-seq (cons :ne (input->seq cs)))
            \w (lazy-seq (cons :nw (input->seq cs)))))
      \s (let [[c' & cs] cs]
           (case c'
             \e (lazy-seq (cons :se (input->seq cs)))
             \w (lazy-seq (cons :sw (input->seq cs))))))))

(defn hexa+
  [[x y] [ax ay]]
  (list (+ x ax) (+ y ay)))

(defn hexa-z
  [[x y]]
  (- (+ x y)))

(def directions
  {:e '(1 0)
   :ne '(1 -1)
   :se '(0 1)
   :w '(-1 0)
   :nw '(0 -1)
   :sw '(-1 1)})
(def origin '(0 0))
(defn get-target-coordinate
  [moves]
  (->> moves
       (map directions)
       (reduce hexa+ origin)))

(defn flip-tile
  [grid position]
  (if-let [_ (get grid position)]
    (dissoc grid position)
    (assoc grid position :black)))

(defn transform
  [grid transformation]
  (let [position (get-target-coordinate transformation)]
    (flip-tile grid position)))

(defn apply-transformations
  [ts]
  (reduce transform {} ts))

(defn solve
  [content]
  (let [transformmations (->> content str/split-lines (map input->seq))
        final-grid (apply-transformations transformmations)]
    (count final-grid)))

(defn evolve-grid
  [grid])

(comment
  (solve (util/read-input 24)))
