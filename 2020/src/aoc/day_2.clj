(ns day-2
  (:require [clojure.string :as str]))

(def test-input "1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc")

(defn read-input [] (slurp "/home/olivier/tmp/input-2.txt"))

(def security-pattern #"(\d+)-(\d+) ([a-z]): ([a-z]+)")
(defn parse-row
  [row]
  (let [[_ m M c input] (re-find security-pattern row)]
    {:min (Integer/parseInt m)
     :max (Integer/parseInt M)
     :char (get c 0)
     :input input}))

(defn count-char
  [input c]
  (->> input (filter (partial = c)) count))

(defn valid-v1?
  [{:keys [min max char input]}]
  (let [count (count-char input char)]
    (<= min count max)))

(defn valid-v2?
  [{:keys [min max char input]}]
  (let [f (nth input (dec min))
        l (nth input (dec max))]
    ;; (println (str char " " f " " l))
    (and 
     (or (= f char) (= l char))
         (not= f l))))

(def valid? valid-v2?)

(valid? (parse-row "1-3 a: abcde"))

(defn solve [f]
  (->> (f)
       (str/split-lines)
       (map parse-row)
       (filter valid?)
       (count)))

(solve (constantly test-input))
(solve read-input)