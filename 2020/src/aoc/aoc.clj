(ns aoc.aoc
  (:require [clojure.string :as str]))

(defn user-home
  []
  (System/getProperty "user.home"))

(defn file-path
  [day]
  (str (user-home) "/tmp/input-" day ".txt"))

(defn read-input
  [day]
  (slurp (file-path day)))

(defn read-input-lines
  [day]
  (-> (read-input day) (str/split-lines)))

(defn parse-ints
  [data splitter]
  (->> data (splitter) (map #(Integer/parseInt %))))

(defn parse-longs
  [data]
  (->> data (str/split-lines) (map #(Long/parseLong %))))
