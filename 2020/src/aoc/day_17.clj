(ns aoc.day-17
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

; some values for tests
(def ^:dynamic width nil)
(def ^:dynamic height nil)

(defrecord Point [x y z])

(defn geo-axis+
  [coordinate point-a point-b]
  (+ (coordinate point-a) (coordinate point-b)))
(defn geo+
  [point-a point-b]
  (Point.
   (geo-axis+ :x point-a point-b)
   (geo-axis+ :y point-a point-b)
   (geo-axis+ :z point-a point-b)))

(defn parse-row
  [row input-row]
  (->> input-row
       (map-indexed vector)
       (filter #(= \# (second %)))
       (map #(Point. (first %) row 0))))
(defn parse-grid
  [input]
  (->> input
       (str/split-lines)
       (map-indexed parse-row)
       (apply concat)
       (reduce #(assoc %1 %2 :here) {})))

(def directions
  (let [es (range -1 2)]
    (for [x es y es z es :when (not (= 0 x y z))]
      (Point. x y z))))

(defn get-neighbours-positions
  [point]
  (map (partial geo+ point) directions))

(defn view-grid-around
  [grid {:keys [row col]}]
  nil)

(defn still-active?
  "Evolves the state of an active cube"
  [grid point]
  (let [neighbours (filter (partial get grid) (get-neighbours-positions point))]
    (<= 2 (count neighbours) 3)))
(defn evolve-active-cube
  [grid result point]
  (if (still-active? grid point)
    result
    (dissoc result point)))
(defn evolve-active-cubes
  [grid cubes result]
  (reduce
   (partial evolve-active-cube grid)
   result
   cubes))

(defn get-boundaries
  [grid axis]
  (let [vs (map axis (keys grid))]
    (list (apply min vs) (apply max vs))))

(defn get-axis-candidate-range
  "Gets all candidate coordinates for a given axis, from the _m_in and _M_ax
   coordinates of active cubes."
  [m M]
  ; inactive cubes just before and after min/max can be activated
  (range (dec m) (+ M 2)))
(defn get-all-candidates
  [grid]
  (let [[mx Mx] (get-boundaries grid :x)
        [my My] (get-boundaries grid :y)
        [mz Mz] (get-boundaries grid :z)]
    (for [x (get-axis-candidate-range mx Mx)
          y (get-axis-candidate-range my My)
          z (get-axis-candidate-range mz Mz)]
      (Point. x y z))))
(defn get-candidates
  [grid]
  ; filter cubes already active
  (filter (complement (partial get grid))
          (get-all-candidates grid)))

(defn activate?
  "Evolves the state of an inactive cube"
  [grid point]
  (let [neighbours (filter (partial get grid) (get-neighbours-positions point))]
    (= (count neighbours) 3)))
(defn evolve-inactive-cube
  [grid result point]
  (if (activate? grid point)
    (assoc result point :here)
    result))
(defn evolve-inactive-cubes
  [grid result]
  (reduce (partial evolve-inactive-cube grid)
          result
          (get-candidates grid)))

(defn evolve-grid
  [grid]
  (->> grid
      (evolve-active-cubes grid (keys grid))
       (evolve-inactive-cubes grid)))

(defn simulate
  [grid turns]
  {:pre [(pos? turns)]}
  (->> (iterate evolve-grid grid)
       (drop turns)
       (first)))

(defn solve
  [content]
  (let [grid (parse-grid content)
        output (simulate grid 6)]
    (count output)))

(comment
  (solve (util/read-input 17)))
