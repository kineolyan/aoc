(ns aoc.day-19
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(defn parse-match-rule
  [input]
  {:pre [(= (count input) 3)]}
  (list := (.charAt input 1)))

(defn parse-seq-rule
  [input]
  (cons :seq
        (map #(Integer/parseInt %) (str/split input #" "))))

(defn parse-alt-rules
  [input id-start]
  (let [parts (str/split input #" \| ")
        ids (iterate dec (-> id-start - dec))]
    (map #(vector %1 (parse-seq-rule %2)) ids parts)))

(defn parse-rule
  [store row]
  (let [[id rules] (str/split row #": ")
        id (Integer/parseInt id)]
    (cond
      (str/includes? rules "|")
      (let [parsed-rules (parse-alt-rules rules (count store))
            ids (map first parsed-rules)]
        (into (assoc store id (cons :alt ids))
              parsed-rules))
      (str/includes? rules "\"")
      (assoc store id (parse-match-rule rules))
      :else
      (assoc store id (parse-seq-rule rules)))))

(defn parse-rules
  [rows]
  (reduce parse-rule {} rows))

(defn parse-input
  [content]
  (let [[rules blank-and-msgs] (split-with (complement str/blank?) (str/split-lines content))]
    {:rules (parse-rules rules)
     :messages (rest blank-and-msgs)}))

(defn execute-step
  [rules v [r & stack]]
  (if
   (nil? r) '() ; empty stack but still chars to parse
   (let [rule (get rules r)]
     (case (first rule)
       := (if (= (second rule) v)
            [stack]
            '())
       :seq (execute-step rules v (lazy-cat (rest rule) stack))
       :alt (->> (rest rule)
                 (map #(cons % stack))
                 (map (partial execute-step rules v))
                 (apply concat))))))

(defn execute
  [rules stacks v]
  ; we must execute the first of each stack
  ; stacks can multiply because of alt branch
  ; we can have an early return if one stack is exhausted but we still have char
  (if (empty? stacks)
    (reduced :fail)
    (->> stacks (map #(execute-step rules v %)) (apply concat))))

(defn init-stack
  [rules]
  (let [rule (get rules 0)]
    (case (first rule)
      :seq (list (rest rule))
      :alt (map list (rest rule))
      :eq (throw (IllegalStateException. "too easy. will not happen")))))

(defn valid-message?
  [rules message]
  (let [result (reduce
                (partial execute rules)
                (init-stack rules)
                message)]
    ; valid if one stack is empty and there are still stack
    ; or we may have fail early
    (and (not= result :fail)
         (some empty? result))))

(defn patch-rules
  [rules]
  (-> rules
      (parse-rule "8: 42 | 42 8")
      (parse-rule "11: 42 31 | 42 11 31")))

(defn solve-pb
  [{:keys [rules messages]}]
  (->> messages
       (filter (partial valid-message? rules))
       count))
(defn solve
  [content]
  (let [pb (parse-input content)]
    (solve-pb pb)))

(defn solve-2
  [content]
  (let [pb (parse-input content)
        pb (update pb :rules patch-rules)]
    (solve-pb pb)))

(comment
  (solve (util/read-input 19))
  (solve-2 (util/read-input 19)))