(ns day-13
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(defn parse-ids
  [input]
  (->> (str/split input #"\s*,\s*")
       (map #(if (= "x" %) nil (Long/parseLong %)))
       (map-indexed vector)
       (filter (comp some? second))))

(defn parse-input
  [content]
  (let [[row-time row-ids] (str/split-lines content)]
    {:time (Integer/parseInt row-time)
     :buses (parse-ids row-ids)}))

(defn next-time-after
  ([{:keys [time bus]}] (next-time-after time bus))
  ([time bus]
   {:pre [(< 1 bus)]}
   (let [time-slices (quot (dec time) bus)]
     (* (long bus) (inc time-slices)))))

(defn find-next-bus
  [{:keys [time buses]}]
  (->> (map second buses)
       (map #(hash-map :id % :at (next-time-after {:time time :bus %})))
       (sort-by :at)
       (first)))

(defn solve
  [content]
  (let [state (parse-input content)
        next-bus (find-next-bus state)
        time-to-wait (- (:at next-bus) (:time state))]
    (* time-to-wait (:id next-bus))))

;; (defn euclidean
;;   [a b]
;;   (loop [r a u 1 v 0
;;          r' b u' 0 v' 1]
;;     (if (zero? r')
;;       (list u v)
;;       (let [q (mod r r')]
;;         (recur r' u' v'
;;                (- r (* q r')) (- u (* q u')) (- v (* q v')))))))

(defn first-u
  "Computes the first value u so that (= r (mod n m))"
  [n m r]
  {:pre [(< r m) (pos? r)]}
  (->> (iterate (partial + n) n)
       (filter #(= r (mod % m)))
       (first)))

(defn solve-for-bus
  [buses i]
  (let [[a bus] (nth buses i)
        others (for [[j b] buses :when (not= a j)] b)
        p (apply * others)
        u (first-u p bus 1)]
    (* (mod a bus) u)))

(defn big+
  [values]
  (->> values
       (map #(BigInteger/valueOf %))
       (reduce #(.add %1 %2))))
(defn big-
  [A b]
  (.subtract A (BigInteger/valueOf b)))
(defn big-mod
  [A b]
  (.mod A (BigInteger/valueOf b)))

(defn find-magic-time
  [buses]
  (let [C (count buses)
        P (apply * (map second buses))
        changed-buses (for [[i bus] buses] [(- C i) bus])
        parts (map (partial solve-for-bus changed-buses) (range C))
        total (big+ parts)
        mod-total (big-mod total P)]
    (big- mod-total C)))

(comment
  (find-magic-time [[0 17] [2 13] [3 19]]) ;; 3417
  (find-magic-time [[0 67] [1 7] [2 59] [3 61]]) ;; 754018
  )

(defn solve-times
  [content]
  (let [{:keys [buses]} (parse-input content)]
    (find-magic-time buses)))

(comment
  (solve (util/read-input 13))

  (solve-times (util/read-input 13)))