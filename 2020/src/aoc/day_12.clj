(ns day-12
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(defn manhattan
  [x y]
  (+ (Math/abs x) (Math/abs y)))

(defn char->instruction
  [c]
  (case c
    \F :forward
    \R :right
    \L :left
    \N :north
    \S :south
    \E :east
    \W :west))

(defn parse-instruction
  [row]
  (list (char->instruction (.charAt row 0))
        (Integer/parseInt (.substring row 1))))

(defmulti move (fn [_position [action & _]]
                 (condp contains? action
                   #{:forward :left :right} action
                   #{:north :south :east :west} :move-waypoint)))

(defmethod move :forward
  [position [_ value]]
  (-> position
      (update-in [:position :lon] + (* value (get-in position [:waypoint :lon])))
      (update-in [:position :lat] + (* value (get-in position [:waypoint :lat])))))

(defn rotate-point
  [{:keys [lon lat]}]
  {:lat lon :lon (- lat)})
(defn rotate
  [point value]
  {:pre [(zero? (mod value 90))]}
  (->> (iterate rotate-point point)
       (drop (/ (mod value 360) 90))
       (first)))

(defmethod move :left
  [state [_ value]]
  (update state :waypoint rotate (- value)))
(defmethod move :right
  [position [_ value]]
  (update position :waypoint rotate value))

(defmulti move-waypoint (fn [_ instruction] (first instruction)))
(defmethod move-waypoint :north
  [position [_ value]]
  (update position :lon + value))
(defmethod move-waypoint :south
  [position [_ value]]
  (update position :lon - value))
(defmethod move-waypoint :east
  [position [_ value]]
  (update position :lat + value))
(defmethod move-waypoint :west
  [position [_ value]]
  (update position :lat - value))

(defmethod move :move-waypoint
  [state instruction]
  (update state :waypoint move-waypoint instruction))

(defn travel
  [state instructions]
  (reduce move state instructions))

(defn initial-state
  []
  {:position {:lat 0 :lon 0}
   :waypoint {:lat 10 :lon 1}})
(defn solve
  [f]
  (let [content (f)
        instructions (map parse-instruction (str/split-lines content))
        end-state (travel (initial-state) instructions)]
    (manhattan (get-in end-state [:position :lat])
               (get-in end-state [:position :lon]))))

(comment
  (solve (partial util/read-input 12)))