(ns aoc.day-11
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

; some values for tests
(def ^:dynamic width nil)
(def ^:dynamic height nil)

(def test-input "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL")

(defrecord Point [row col])

(defn ->spot
  [c]
  (case c
    \. nil
    \L :empty
    \# :used))
(defn parse-row
  [row input-row]
  (->> input-row
       (map-indexed #(vector (Point. row %1) (->spot %2)))
       (filter (comp some? second))))
(defn parse-grid
  [input]
  (->> input
       (str/split-lines)
       (map-indexed parse-row)
       (apply concat)
       (into {})))

(def directions
  (for [x (range -1 2) y (range -1 2) :when (not (and (zero? x) (zero? y)))]
    (Point. x y)))
(defn get-next-point
  [direction point]
  (Point. (+ (:row direction) (:row point))
          (+ (:col direction) (:col point))))
(defn in-grid?
  [{:keys [col row]}]
  (and (<= 0 col width)
       (<= 0 row height)))
(defn positions-in-direction
  [point direction]
  (let [s (iterate (partial get-next-point direction) point)]
    (take-while in-grid? (rest s))))
(defn get-next-seat-in-direction
  [grid start direction]
  (->> (positions-in-direction start direction)
       (map (partial get grid))
       (filter some?)
       (first)))
(comment
  (get-next-point (Point. 1 -1) (Point. 5 8))
  (positions-in-direction (Point. 5 4) (Point. -1 0))
  (binding [width 10 height 10]
    (get-next-seat-in-direction
     (parse-grid test-input)
     (Point. 6 7)
     (Point. -1 0)))
  (binding [width 10 height 10]
    (let [g (parse-grid ".##.##.
#.#.#.#
##...##
...L...
##...##
#.#.#.#
.##.##.
")]
      (map #(get-next-seat-in-direction g (Point. 4 4) %) directions))))

(defn get-neighbours-positions
  [{:keys [row col]}]
  (map (fn [{r :row c :col}] (Point. (+ row r) (+ col c))) directions))
(defn get-neighbours
  [grid point]
  (->> (get-neighbours-positions point)
       (map (partial get grid))
       (filter some?)))

(defn view-grid-around
  [grid {:keys [row col]}]
  (partition
   3
   (for [x (range -1 2) y (range -1 2)]
     (get grid (Point. (+ row x) (+ col y))))))

(defn filter-seats
  [t seats]
  (filter (partial = t) seats))
(defn empty-seats
  [seats]
  (filter-seats :empty seats))
(defn used-seats
  [seats]
  (filter-seats :used seats))

(def surrounding 5)
(defn evolve-seat
  [grid point]
  (let [seat (get grid point)
        around (get-neighbours grid point)
        used-count (count (used-seats around))]
    (cond
      (and (= seat :empty) (zero? used-count)) :used
      (and (= seat :used) (>= used-count surrounding)) :empty
      :else seat)))
(defn evolve-grid
  [grid]
  (reduce
   (fn [acc point]
     (assoc acc point (evolve-seat grid point)))
   grid
   (keys grid)))
(evolve-grid (parse-grid test-input))

(defn find-stable-point
  [steps]
  (->> (map vector steps (rest steps))
       (drop-while (partial apply not=))
       (ffirst)))

(defn count-used-seats
  [grid]
  (-> grid vals used-seats count))
(comment
  (count-used-seats (parse-grid test-input)))

(defn spot->str
  [seat]
  (case seat
    nil \.
    :used \#
    :empty \L))
(defn print-grid
  [grid]
  (doall
   (for [row (range (inc height))]
     (do
       (doall (for [col (range (inc width))]
                (->> (Point. row col) (get grid) spot->str print)))
       (println "")))))
(comment
  (binding [width 10
            height 10]
    (print-grid (parse-grid test-input))))

(defn solve
  [f]
  (let [grid (parse-grid (f))]
    (binding [width (apply max (map :col (keys grid)))
              height (apply max (map :row (keys grid)))]
      (println (str width " x " height))
      (->> grid
           (iterate evolve-grid)
           (take 1000) ; limit not to blow the REPL
           (find-stable-point)
           ((fn [g]
              (print-grid g)
              g))
           (count-used-seats)))))

(comment
  (binding [width 10 height 10]
    (doseq [g (take 10 (iterate evolve-grid (parse-grid test-input)))]
      (print-grid g)
      (println "")))

  (solve (constantly test-input))
  (solve (partial util/read-input 11)))


(comment
  ; testing stuff
  (def small-grid (parse-grid "#.LL.L
#LLLL#
L.L.L."))
  (count-used-seats small-grid)

  (evolve-seat small-grid (Point. 1 2))
  (view-grid-around small-grid (Point. 1 2)))