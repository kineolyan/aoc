(ns aoc.day-20
  (:require [instaparse.core :as insta]
            [loom.graph :as loom]
            [aoc.aoc :as util]))

(def input-parser
  (insta/parser
   "<S> = I+
     I = HEADER <NL> IMAGE <NL?>
    <NL> = '\n'
     HEADER = <'Tile '> ID <':'>
    ID = #\"\\d+\"
    <PIXEL> = '.'|'#'
    ROW = PIXEL+ <NL>
     IMAGE = ROW+"))

(defn get-id
  [header]
  (-> header second second (#(Integer/parseInt %))))

(defn str->bit
  [v]
  (case v
    "#" 1
    "." 0))
(defn str-array->number
  [image-line]
  (reduce
   #(+ (* 2 %1) %2)
   0
   (map str->bit image-line)))

(defn get-image-boundaries
  [image]
  (let [top (-> image first rest)
        bottom (-> image last rest)
        left (map second image)
        right (map last image)]
    {:f-top (str-array->number top)
     :b-top (str-array->number (reverse top))
     :f-bottom (str-array->number (reverse bottom))
     :b-bottom (str-array->number bottom)
     :f-left (str-array->number (reverse left))
     :b-left (str-array->number left)
     :f-right (str-array->number right)
     :b-right (str-array->number (reverse right))}))

(defn format-image
  [parsed]
  (let [[_tag header image] parsed]
    {:id (get-id header)
     :boundaries (get-image-boundaries (rest image))}))

(defn parse-input
  [content]
  (let [parsed (input-parser content)
        images (map format-image parsed)]
    images))

(defn find-side-connection
  [key pattern image-b]
  (->> (:boundaries image-b)
       (filter (fn [[_ v]] (= v pattern)))
       (map (fn [[k _]] [key [(:id image-b) k]]))))

(defn find-connections
  [image-a image-b]
  (->> (:boundaries image-a)
       (map (fn [[k v]] (find-side-connection [(:id image-a) k] v image-b)))
       (apply concat)))

(defn connect-to-images
  [image others graph]
  (reduce
   #(let [connections (find-connections image %2)]
      (reduce loom/add-edges %1 connections))
   graph
   others))
(defn connect-images
  [images]
  (loop [[image & more] images
         graph (loom/graph)]
    (if (seq more)
      (recur more (connect-to-images image more graph))
      graph)))

(def forward-sides [:f-top :f-bottom :f-left :f-right])
(def backwark-sides [:b-top :b-bottom :b-left :b-right])
(defn get-side-connections
  [graph id sides]
  (->> (map #(vector id %) sides)
       (filter #(loom/successors graph %))))

(defn corner?
  ([graph id]
   (or (corner? graph id forward-sides)
       (corner? graph id backwark-sides)))
  ([graph id sides]
   (= 2 (count (get-side-connections graph id sides)))))

(defn find-corners
  [images graph]
  (let [ids (map :id images)]
    (filter (partial corner? graph) ids)))

(defn solve
  [content]
  (let [images (parse-input content)
        graph (connect-images images)
        corners (find-corners images graph)]
    (pr-str "Corners" corners)
    (if (= (count corners) 4)
      (apply * corners)
      (throw (IllegalStateException. (pr-str "Corners = " corners))))))

(comment 
  (-> (util/read-input 20) (input-parser) first format-image)
  (solve (util/read-input 20))
  (solve (slurp "/home/olivier/tmp/test-input-20.txt")))