(ns aoc.day-15
  (:require [clojure.string :as str]))

(defn parse-input
  [input]
  (map #(Integer/parseInt %) (str/split input #",")))

(defn create-state
  [vs]
  {:last (last vs)
   :memory (into {} (map-indexed #(vector %2 (inc %1)) (drop-last vs)))})

(defn get-age
  [memory value turn]
  (if-let [before (get memory value)]
    (- turn 1 before) ; we are asking this on next turn
    0))

(defn play-one-turn
  [{last :last mem :memory} turn]
  (let [answer (get-age mem last turn)]
    ;; (println (str "r " turn " -> " answer))
    {:last answer
     :memory (assoc mem last (dec turn))})) ; said on previous turn

(defn solve
  [content n]
  (let [vs (parse-input content)
        state (create-state vs)
        result (reduce play-one-turn state (range (inc (count vs)) (inc n)))]
    (:last result)))

(comment
  (solve "0,5,4,1,10,14,7" 2020))
