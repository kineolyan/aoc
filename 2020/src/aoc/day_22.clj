(ns aoc.day-22
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(defn rows->numbers
  [rows]
  (map #(Integer/parseInt %) rows))

(defn parse-input
  [content]
  (let [[player-1 player-2] (->> content (str/split-lines) (split-with (complement str/blank?)))]
    {:self (rows->numbers (rest player-1))
     :crab (rows->numbers (drop 2 player-2))}))

(defn get-result
  [state]
  (if (= :infinite state)
    {:winner :self :loser :crab}
    (let [{self-deck :self crab-deck :crab} state]
      (cond
        (and (empty? self-deck) (seq crab-deck)) {:winner :crab :loser :self}
        (and (empty? crab-deck) (seq self-deck)) {:winner :self :loser :crab}
        :else (throw (IllegalStateException. (str state)))))))

(defn play-with-cards
  [{[self-card & _] :self [crab-card & _] :crab}]
  (condp apply [self-card crab-card]
    < {:winner :crab :loser :self}
    > {:winner :self :loser :crab}
    = (throw (IllegalStateException. "Not supporting draws."))))

(defn recursive-deck?
  [deck]
  (<= (first deck) (dec (count deck))))

(defn play-recursive?
  [state]
  (every? recursive-deck? (vals state)))

(defn play-with-recursion
  [state]
  (let [recursion-start {:self (-> state :self rest)
                         :crab (-> state :crab rest)}
        _ (println (str "recursive game from " recursion-start))
        recursion-result (play-game recursion-start)]
    (get-result recursion-result)))

(defn get-winner-loser
  [state]
  (if (play-recursive? state)
    (play-with-recursion state)
    (play-with-cards state)))

(defn update-state
  [state {:keys [winner loser]}]
  {:pre [(keyword? winner) (keyword? loser)]}
  (let [[winning-card & win-rest] (winner state)
        [losing-card & loss-rest] (loser state)]
    {winner (lazy-cat win-rest [winning-card losing-card])
     loser loss-rest}))

(defn play-turn
  [state]
  (let [result (get-winner-loser state)]
    (update-state state result)))

(defn loser?
  [state player]
  (empty? (player state)))
(defn play-game
  [state]
  (loop [state state
         played #{state}
         it 0]
    (when (> it 100000) (throw (ex-info "Infinite loop" {})))
    (let [next-state (play-turn state)]
      (cond
        (contains? played next-state) :infinite
        (or (loser? next-state :self) (loser? next-state :crab)) next-state
        :else  (recur next-state
                      (conj played next-state)
                      (inc it))))))

(defn compute-score
  [cards]
  (reduce
   +
   (map * (range 1 (inc (count cards))) (reverse cards))))

(defn get-winner-cards
  [state]
  (->> state (get-result) :winner (get state)))

(defn solve
  [content]
  (->> (parse-input content)
       (play-game)
       (get-winner-cards)
       (compute-score)))

(comment
  (solve (util/read-input 22)))