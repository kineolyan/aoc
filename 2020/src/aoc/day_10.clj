(ns day-10
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(def test-input "16
10
15
5
1
11
7
19
6
12
4")
(def test-data (util/parse-longs test-input))

(defn compute-inner-jumps
  [values]
  (map - (rest values) values))

(defn compute-jumps
  [values]
  (let [sorted values
        jumps (compute-inner-jumps sorted)]
    ;; (when (< 3 (first sorted))
      ;; (throw (IllegalStateException. (str "First value not compatible with the problem: " (take 5 sorted) "..."))))
    (concat jumps [(first sorted) 3]))) ; add the last jump always of 3 to the end

(defn compute-stats
  [jumps]
  (frequencies jumps))

(-> test-data compute-jumps compute-stats)

(defn compute-v1-value
  [freqs]
  (* (get freqs 3) (get freqs 1)))

(defn solve-1
  [f]
  (->> (f)
       (util/parse-longs)
       (sort)
       (compute-jumps)
       (compute-stats)
       (compute-v1-value)))
(def test-input-2 "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3")
(comment
  (= (solve-1 (constantly test-input)) 35)
  (= (solve-1 (constantly test-input-2)) 220)
  (= (solve-1 (partial util/read-input 10)) 2368))

(defn valid-chain?
  [values]
  (every? (partial >= 3) (compute-inner-jumps values)))
(defn invalid-chain?
  [values]
  (not (valid-chain? values)))

(comment
  (valid-chain? (sort test-data))
  (valid-chain? (range 3 7)))

(def combine
  (memoize
   (fn [[x & [y & rest :as more] :as values]]
    ;  (println (str "testing " values))
     (cond
       (invalid-chain? values) 0
       (empty? rest) 1 ; end of the recursion
       :else (+ (combine more) ; standard chain
                (combine (cons x rest))))))) ; without y

(defn complete-inputs
  [values]
  (sort (concat values [0 (+ 3 (apply max values))])))
(defn solve-2
  [f]
  (->> (f)
       (util/parse-longs)
       (complete-inputs)
       (sort)
       (combine)))

(comment
  (solve-2 (constantly test-input))
  (solve-2 (constantly test-input-2))
  (solve-2 (partial util/read-input 10)))