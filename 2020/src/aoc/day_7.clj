(ns aoc.day-7
  (:require [clojure.string :as str]
            [aoc.aoc :as util]))

(def test-input (str/split-lines "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."))

(defn parse-color-list
  [value]
  (->> value
       (re-seq #"(\d+) ([a-z ]+?) bags?")
       (map (juxt last #(Integer/parseInt (second %))))
       (into {})))
(parse-color-list "1 dark olive bag, 2 vibrant plum bags")
(parse-color-list "no other bags")

(defn parse-rule
  [rule]
  (let [[main inner] (str/split rule #" bags contain ")]
    {:main main
     :inner (parse-color-list inner)}))
(parse-rule "light red bags contain 1 bright white bag, 2 muted yellow bags.")

(defn add-to-graph
  [graph {:keys [main inner]}]
  (reduce
   #(update-in %1 [%2] conj main)
   graph
   (keys inner)))
(defn build-rule-graph
  [rules]
  (reduce
   add-to-graph
   {}
   rules))
(build-rule-graph (map parse-rule test-input))

(defn find-containers
  [color graph]
  (loop [items (list color)
         results #{}]
    (if-let [[item & more] (seq items)]
      (recur (concat more (filter (complement results) (get graph item)))
             (conj results item))
      results)))

(def my-color "shiny gold")
(defn solve
  [f]
  (->> (f)
       (map parse-rule)
       (build-rule-graph)
       (find-containers my-color)
       (count)
       (dec)))
(solve (constantly test-input))

(solve (partial util/read-input-lines 7))

(def test-input-2 (str/split-lines "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."))

(defn rules->map
  [rules]
  (->> rules
       (map (juxt :main :inner))
       (into {})))
(get (rules->map (map parse-rule test-input)) "shiny gold")

(def count-bags
  (memoize (fn [color graph]
             (if-let [inner (seq (get graph color))]
               (->> inner
                    (map (fn [[c n]] (* n (inc (count-bags c graph)))))
                    (apply +))
               0))))

(defn solve-2
  [f]
  (->> (f)
       (map parse-rule)
       (rules->map)
       (count-bags my-color)))
(solve-2 (constantly test-input))
(solve-2 (constantly test-input-2))
(solve-2 (partial util/read-input-lines 7))