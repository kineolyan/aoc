
type BusTable = Vec<(u8, u128)>;

fn next_time_after(time: &u128, bus: &u128) -> u128 {
	let time_slices = (time - 1) / bus;
	bus * (time_slices + 1)
}

fn solve(buses: BusTable) -> u128 {
	let reset_count = buses.len() - 1;
	let mut c = buses.len();
	let mut bus_it = buses.iter().cycle();
	let mut t = 1u128;
	loop {
		if c == 0 {
			return t;
		}
		let (position, bus) = bus_it.next().expect("No more element");
		let target_t = t + *position as u128;
		let next_t = next_time_after(&target_t, bus);
		if target_t == next_t {
			c -= 1;
		} else {
			t = next_t - *position as u128;
			c = reset_count;
		}
	}
}

fn main() {
	let buses: BusTable = vec![
		(0, 29),
		(23, 37),
		(29, 433),
		(42, 13),
		(43, 17),
		(48, 19),
		(52, 23),
		(60, 977),
		(101, 41)
	];
	let result = solve(buses);
	println!("{:?}", result)
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_1() {
		let buses: BusTable = vec![(0, 17), (2, 13), (3, 19)];
		let result = solve(buses);
		assert_eq!(result, 3417);
	}

	#[test]
	fn test_2() {
		let buses: BusTable = vec![(0, 67), (1, 7), (2, 59), (3, 61)];
		let result = solve(buses);
		assert_eq!(result, 754018);
	}

	#[test]
	fn test_3() {
		let buses: BusTable = vec![(0, 67), (2, 7), (3, 59), (4, 61)];
		let result = solve(buses);
		assert_eq!(result, 779210);
	}

	#[test]
	fn test_4() {
		let buses: BusTable = vec![(0, 67), (1, 7), (3, 59), (4, 61)];
		let result = solve(buses);
		assert_eq!(result, 1261476);
	}

	#[test]
	fn test_5() {
		let buses: BusTable = vec![(0, 1789), (1, 37), (2, 47), (3, 1889)];
		let result = solve(buses);
		assert_eq!(result, 1202161486);
	}
}