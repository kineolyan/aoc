(ns parse-util)

(defn unwrap
  "Returns the passed arguments as a seq."
  [& args]
  args)

(defn ->hash-map-fn
  [& ks]
  (fn [& args]
    (zipmap ks args)))
