(ns y2k24.day2
  (:require [instaparse.core :as insta]))

(def sample-input
  "7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9
")

(def parser
  (insta/parser
   "<S> = (Line <nl>?)*
    nl = #'\\n'
    sp = #'\\s+'
    Line = (N <sp>)+ N
    N = #'\\d+'"))

(defn parse-input
  [sample-input]
  (def sample-tree (parser sample-input))
  (when (:reason sample-tree)
    (throw (ex-info "Parsing error" sample-tree)))
  (def sample-data
    (insta/transform
     {:Line (fn [& args] args)
      :N Long/parseLong}
     sample-tree))
  sample-data)

(defn sorted?
  [xs]
  (->> (map - xs (rest xs))
       (map pos?)
       (apply =)))

(defn gradual?
  [xs]
  (->> (map - xs (rest xs))
       (map abs)
       (map #(<= 1 % 3))
       (apply =)))

(parse-input sample-input)
(map (juxt identity sorted? gradual?) sample-data)

(defn solve-1
  [data]
  (count (filter #(and (sorted? %) (gradual? %)) data)))

(solve-1 sample-data)

(comment
  (let [input (slurp "clj/src/y2k24/day2.txt")
        data (parse-input input)]
    (solve-1 data)))
