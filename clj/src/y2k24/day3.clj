(ns y2k24.day3
  (:require [instaparse.core :as insta]
            [clojure.core.match :as m]
            [parse-util :as pu]))

(def sample-input
  "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))")

(def parser
  (insta/parser
   "<S> = <(C*)> (VAL <(C*)>)+
    <VAL> = MUL | DO | DONT
    DONT = <C>* <'don\\'t()'>
    DO = <'do()'>
    MUL = <'mul('> N <','> N <')'>
    N = #'-?\\d+'
    C = #'.'"))

(defn parse-input
  [sample-input]
  (def sample-tree (parser sample-input))
  (when (:reason sample-tree)
    (throw (ex-info "Parsing error" sample-tree)))
  (def sample-data
    (insta/transform
     {:MUL pu/unwrap
      :N Long/parseLong}
     sample-tree))
  sample-data)

(parse-input sample-input)

(def pattern #"(?:mul\((\d+),(\d+)\)|(do)\(\)|(don't)\(\))")

(defn parse-reg
  [sample-input]
  (for [[_ x y d dn] (re-seq pattern sample-input)]
    (cond
      dn :dont
      d :do
      :else [:mul (Long/parseLong x) (Long/parseLong y)])))

(defn solve-1
  [data]
  (->> (map (partial apply *) data)
       (reduce +)))

(defn solve-2
  [data]
  (first
   (reduce
    (fn [[sum agg?] datum]
      (m/match [agg? sum datum]
        [_ _ :dont] [sum false]
        [_ _ :do] [sum true]
        [false _ _] [sum agg?]
        [true _ [:mul x y]] [(+ sum (* x y)) agg?]))
    [0 true]
    data)))

(comment
  (.length (slurp "clj/src/y2k24/day3.txt"))
  (let [input (slurp "clj/src/y2k24/day3.txt")
        data (time (parse-reg input))]
    (time (solve-2 data))))
; 1 => 165225049
; 2 => 108830766
