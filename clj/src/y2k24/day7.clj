(ns y2k24.day7
  (:require [instaparse.core :as insta]
            [clojure.core.match :as m]
            [parse-util :as pu]))

(def sample-input
  "190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20
")

(def parser
  (insta/parser
   "<S> = (EQ <NL>)+
    EQ = TOTAL <':'> (<SP>+ N)+
    <TOTAL> = N
    N = #'\\d+'
    NL = #'\\n'
    SP = #'[\\t ]+'"))

(defn parse-input
  [sample-input]
  (def sample-tree (parser sample-input))
  (when (:reason sample-tree)
    (throw (ex-info "Parsing error" sample-tree)))
  (def sample-data
    (insta/transform
     {:EQ (fn [x & more] [x := more])
      :N Long/parseLong}
     sample-tree))
  sample-data)

(parse-input sample-input)

(comment
  (-> (slurp "clj/src/y2k24/day7.txt")
      (parse-input)))
