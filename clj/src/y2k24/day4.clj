(ns y2k24.day4
  (:require [instaparse.core :as insta]
            [clojure.core.match :as m]
            [parse-util :as pu]))

(def sample-input
  "MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
")

(def parser
  (insta/parser
   "<S> = <(C*)> (VAL <(C*)>)+
    <VAL> = MUL | DO | DONT
    DONT = <C>* <'don\\'t()'>
    DO = <'do()'>
    MUL = <'mul('> N <','> N <')'>
    N = #'-?\\d+'
    C = #'.'"))

(defn parse-input
  [sample-input]
  (def sample-tree (parser sample-input))
  (when (:reason sample-tree)
    (throw (ex-info "Parsing error" sample-tree)))
  (def sample-data
    (insta/transform
     {:MUL pu/unwrap
      :N Long/parseLong}
     sample-tree))
  sample-data)

(parse-input sample-input)
