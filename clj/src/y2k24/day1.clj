(ns y2k24.day1
  (:require [instaparse.core :as insta]))

(def sample-input
  "3   4
4   3
2   5
1   3
3   9
3   3")

(def parser
  (insta/parser
   "S = (Line <#'\\n'>?)*
     Line = N <#'\\s+'> N
     N = #'\\d+'"))

(defn parse-input
  [input]
  (def sample-tree (parser input))
  (when (:reason sample-tree)
    (throw (ex-info "Parsing error" sample-tree)))
  (def sample-data
    (insta/transform
     {:S (fn [& args] args)
      :Line (fn [x y] [x y])
      :N Long/parseLong}
     sample-tree))
  sample-data)

(parse-input sample-input)

(defn solve-1
  [data]
  (let [xs (map first data)
        ys (map second data)
        tdata (map vector (sort xs) (sort ys))]
    (->> (map (partial apply -) tdata)
         (map Math/abs)
         (reduce +))))

(defn solve-2
  [data]
  (let [xs (map first data)
        ys (map second data)
        fs (frequencies ys)]
    (->> (map #(* % (get fs % 0)) xs)
         (reduce +))))

(comment
  (let [aoc-input (slurp "clj/src/y2k24/day1.txt")
        aoc-data (parse-input aoc-input)]
    (solve-1 aoc-data)
    (solve-2 aoc-data))) ; 24643097
