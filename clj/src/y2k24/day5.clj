(ns y2k24.day5
  (:require [instaparse.core :as insta]
            [clojure.core.match :as m]
            [parse-util :as pu]))

(def sample-input
  "47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47
")

(def parser
  (insta/parser
   "S = ORDERS <NL> UPDATES
    ORDERS = (ORDER <NL>)+
    ORDER = N <'|'> N
    UPDATES = (UPDATE <NL>)+
    UPDATE = N (<','> N)*
    N = #'\\d+'
    NL = #'\\n'
    SP = #'[\\t ]+'"))

(defn parse-input
  [sample-input]
  (def sample-tree (parser sample-input))
  (when (:reason sample-tree)
    (throw (ex-info "Parsing error" sample-tree)))
  (def sample-data
    (insta/transform
     {:S (pu/->hash-map-fn :orders :updates)
      :ORDERS pu/unwrap
      :UPDATES pu/unwrap
      :UPDATE pu/unwrap
      :ORDER #(vector %1 :> %2)
      :N Long/parseLong}
     sample-tree))
  sample-data)

(parse-input sample-input)

(comment
  (-> (slurp "clj/src/y2k24/day5.txt")
      (parse-input)))
