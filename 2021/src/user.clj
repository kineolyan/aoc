(ns user
  (:require [hyperfiddle.rcf :as rcf]))

; Enable per run not to execute tests from all loaded namespaces
(defn rcf-go
  []
  (rcf/enable!))

(rcf/enable!)

(println "User space loaded")
