(ns aoc.day14
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  (str/split-lines
  "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"))

(defn parse-polymer
  [input]
  (let [pairs (map vector input (rest input))]
    {:pairs (frequencies pairs)
     :tail (last pairs)}))

(defn parse-formula
  [input]
  (let [[[a b] [r]] (str/split input #" -> ")]
    {:pattern [a b] 
     :result r}))

(defn parse-input
  [input]
  (let [[template _ & formulae] input]
    {:polymer (parse-polymer template)
     :rules (->> formulae
                 (map parse-formula)
                 (map #(vector (% :pattern) (% :result)))
                 (into {}))}))

(def test-data
  (parse-input test-input))

(defn mutate-pair
  [[a b :as pair] rules]
  (if-let [r (get rules pair)]
    [[a r] [r b]]
    [pair]))

(defn mutate
  ([{:keys [polymer rules] :as state}]
   (update state :polymer mutate rules))
  ([{:keys [pairs tail]} rules]
   (let [new-tail (second (mutate-pair tail rules))
         new-pairs (mapcat
                     (fn [[pair c]]
                       (map #(vector % c) (mutate-pair pair rules)))
                     pairs)]
     {:pairs (reduce
               #(update %1 (first %2) (fnil + 0) (second %2))
               {}
               new-pairs)
      :tail new-tail})))

(rcf/tests
  (:polymer (mutate test-data)) := (parse-polymer "NCNBCHB"))

(defn run
  [n data]
  (reduce
    (fn [state _] (mutate state))
    data
    (range n)))

(rcf/tests
  "run"
  (:polymer (run 1 test-data)) := (parse-polymer "NCNBCHB")
  (:polymer (run 2 test-data)) := (parse-polymer "NBCCNBBBCBHCB")
  (:polymer (run 3 test-data)) := (parse-polymer "NBBBCNCCNBBNBNBBCHBHHBCHB")
  (:polymer (run 4 test-data)) := (parse-polymer "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"))

(defn atom-frequencies
  [{:keys [pairs tail]}]
  (let [fs (reduce #(update %1 (ffirst %2) (fnil + 0) (second %2)) {} pairs)]
    (update fs (second tail) (fnil inc 0))))

(rcf/tests
  "freqs"
  (atom-frequencies (parse-polymer "NCNBCHB")) := {\N 2 \C 2 \B 2 \H 1})

(defn solve1
  [data]
  (let [result (run 10 data)
        freqs (atom-frequencies (result :polymer))
        m (apply min (vals freqs))
        M (apply max (vals freqs))]
    (- M m)))

(defn solve2
  [data]
  (let [result (run 40 data)
        freqs (atom-frequencies (result :polymer))
        m (apply min (vals freqs))
        M (apply max (vals freqs))]
    (- M m)))

(rcf/tests
  "solve"
  (solve1 test-data) := 1588
  (solve2 test-data) := 2188189693529
  )

;; We can build a state using pairs but we have to record which char/pair is the last one

(comment
  (def my-data (parse-input (c/my-input "day14.txt")))
  (count (my-data :polymer))
  (count (my-data :rules))
  ; We have a list of 10 chars with rules
  (-> my-data :rules vals set)
  (-> my-data :polymer set)
  ; all combinations are present
  (let [cs (-> my-data :polymer set)]
    (for [a cs
          b cs
          :when (nil? (get (my-data :rules) [a b]))]
      [a b]))
  (solve1 my-data) ; 2321
  (solve2 my-data)
  )
