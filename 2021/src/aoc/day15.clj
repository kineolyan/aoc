(ns aoc.day15
  (:require [clojure.string :as str]
            [clojure.core.reducers :as r]
            [hyperfiddle.rcf :as rcf]
            [clj-async-profiler.core :as prof]
            [aoc.common :as c]))

(def test-input
  (str/split-lines
  "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581"))

(def test-data (c/parse-grid test-input))

(defn update-route
  [entry n]
  (if (< n (or entry Long/MAX_VALUE))
    n 
    entry))

(rcf/tests
  (update-route nil 1) := 1
  (update-route 1 2) := 1
  (update-route 7 2) := 2
  (update-route 7 3) := 3)

(defn get-cost
  [routes point]
  (get routes point Long/MAX_VALUE))

(defn update-routes
  "Updates the routes with a new path from src to dst with a cost of n."
  [routes dst n]
  (update routes dst update-route n))

(defn get-neighbours
  [grid coord]
  (c/get-neighbour-coords c/cardinal-shifts c/map-point grid coord))

(defn add-candidate
  [m p d]
  (update m d (fnil conj #{}) p))

(defn delete-candidate
  [m p d]
  (let [ps (get m d)
        nps (disj ps p)]
    (if (seq nps)
      (assoc m d nps)
      (dissoc m d))))

(rcf/tests
  "add candidates"
  (add-candidate {} :a 1) := {1 #{:a}}
  (add-candidate {1 #{:a} 2 #{:b}} :c 1) := {1 #{:a :c} 2 #{:b}}
  "delete candidate"
  (delete-candidate {1 #{:a :b} 2 #{:c}} :a 1) := {1 #{:b} 2 #{:c}}
  (delete-candidate {1 #{:a} 2 #{:b}} :a 1) := {2 #{:b}})

(defn pop-candidate
  [m]
  (if-let [[d ps] (first m)]
    (let [p (first ps)]
      [(first ps) (delete-candidate m p d)])
    [nil m]))

(rcf/tests
  "pop candidate"
  (pop-candidate (sorted-map 2 #{:a} 1 #{:b :c})) := [:c {2 #{:a} 1 #{:b}}]
  (pop-candidate (sorted-map 2 #{:a} 3 #{:b})) := [:a {3 #{:b}}]
  (pop-candidate (sorted-map)) := [nil {}])

(defn pick-next
  [routes candidates limit]
  (let [[p candidates] (pop-candidate candidates)]
    (if 
      (or (nil? p) (>= (get routes p) limit)) 
      [nil candidates]
      [p candidates])))

(defn get-top-down-cost
  [grid]
  (let [row (first grid)
        col (map last grid)]
    (- (reduce + (concat row col))
       (first col))))

(defn get-down-bottom-cost
  [grid]
  (let [row (last grid)
        col (map first grid)]
    (- (reduce + (concat row col))
       (first row))))

(comment
  (get-top-down-cost test-data)
  (get-down-bottom-cost test-data))

(defn build-routes
  ([grid]
   (let [start {:x 0 :y 0}
         {:keys [w h]} (c/get-dims grid)
         end {:x (dec w) :y (dec h)}]
     (build-routes 
       grid
       end
       (hash-map start 0 
                 end (min (get-top-down-cost grid)
                          (get-down-bottom-cost grid)))
       (add-candidate (sorted-map) start 0))))
  ([grid dst routes candidates]
   (loop [routes routes
          seen #{}
          candidates candidates]
     ; TODO: remove dbg
     (def last* [:next (pick-next routes candidates (get-cost routes dst))])
     (let [[p candidates] (pick-next routes candidates (get-cost routes dst))]
       (if (some? p)
         (let [p-cost (get routes p)
               points-and-costs (for [n (get-neighbours grid p)
                                      :when (not (contains? seen n))
                                      :let [c (+ p-cost (c/get-point grid n))
                                            v (get-cost routes n)]
                                      :when (< c v)]
                                  {:point n :cost c :before v})
               ; remove first the old positions that are going to change
               clean-candidates (reduce #(delete-candidate %1 (:point %2) (:before %2))
                                        candidates
                                        points-and-costs)
               ; update the routes
               new-routes (reduce 
                            #(update-routes %1 (:point %2) (:cost %2)) 
                            routes 
                            points-and-costs)
               updated-candidates (reduce #(add-candidate %1 (:point %2) (:cost %2))
                                          clean-candidates
                                          points-and-costs)]
           (recur new-routes
                  (conj seen p)
                  updated-candidates))
         routes)))))

(comment
  (def r (build-routes test-data))
  (count *1)
  (get r {:x 9 :y 9})
  (c/get-point test-data {:x 0 :y 0}))

(defn solve1
  [grid]
  (let [routes (build-routes grid)
        {:keys [w h]} (c/get-dims grid)
        end (get routes {:x (dec w) :y (dec h)})]
    end))

(defn increase-level
  [v]
  (let [n (inc v)]
    (if (= n 10)
      1
      n)))

(defn increase-grid
  [grid]
  (mapv
    #(mapv increase-level %)
    grid))

(rcf/tests
  (increase-grid [[1 2] [9 8]])
  :=
  [[2 3] [1 9]])

(defn bigger-grid
  [grid]
  (let [replicates (vec (take 10 (iterate increase-grid grid)))
        gds (mapv #(->> replicates (drop %) (take 5)) (range 5))]
    (vec
      (mapcat
        (fn [row-of-grids] 
          (apply map
            (fn [& grids] (vec (flatten grids)))
            row-of-grids))
        gds))))

(comment
  (bigger-grid [[2 3] [1 9]]))

(rcf/tests
  (solve1 test-data) := 40
  (solve1 (bigger-grid test-data)) := 315
  )

(comment
  (def my-data (c/parse-grid (c/my-input "day15.txt")))
  (c/get-dims my-data)
  (solve1 my-data) ; 487

  (prof/serve-files 8080)
  (let [g (bigger-grid test-data)]
    (prof/profile (dotimes [_ 25] (solve1 g))))
  (prof/profile (dotimes [_ 5] (solve1 my-data)))

  (def big-data (bigger-grid my-data))
  (c/get-dims big-data)
  (take 3 big-data)

  (solve1 big-data) ; 2821
  (prof/profile (dotimes [_ 5] (solve1 big-data)))
  )
