(ns aoc.day23
  (:require [clojure.string :as str]
            [aoc.common :as c]
            [hyperfiddle.rcf :as rcf]))

;; test input
;; #############
;; #...........#
;; ###B#C#B#D###
;;   #A#D#C#A#
;;   #########

(def hallway-keywords (mapv #(keyword (str "h" %)) (range 1 12)))
(def hallway-distances
  (into 
    {}
    (for [i (range 1 12)
          j (range 1 12)
          :when (< i j)
          :let [hi (nth hallway-keywords (dec i))
                hj (nth hallway-keywords (dec j))]]
      [(hash-set hi hj) (- j i)])))

(defn build-room-distances
  [distances room exit f]
  (reduce #(assoc %1 #{room %2} (f (get hallway-distances #{exit %2}))) 
          (assoc distances #{room exit} 1) 
          (filter #(not= % exit) hallway-keywords)))

; We could restrict to the distances between rooms and hallway because pods never move into the 
; corridor
(def all-distances
  (as-> hallway-distances m
    (build-room-distances m :a1 :h3 inc)
    (build-room-distances m :a2 :h3 #(+ 2 %))
    (build-room-distances m :b1 :h5 inc)
    (build-room-distances m :b2 :h5 #(+ 2 %))
    (build-room-distances m :c1 :h7 inc)
    (build-room-distances m :c2 :h7 #(+ 2 %))
    (build-room-distances m :d1 :h9 inc)
    (build-room-distances m :d2 :h9 #(+ 2 %)))
    ; (assoc m #{:a1 :a2} 1 #{:b1 :b2} 1)
    )

(defn get-distance
  [a b]
  (get all-distances #{a b}))

(rcf/tests
  (get-distance :a1 :h3) := 1
  (get-distance :b2 :h3) := 4)

;; my input
;; #############
;; #...........#
;; ###D#A#B#C###
;;   #B#A#D#C#
;;   #########

(def energies {\A 1 \B 10 \C 100 \D 1000})
