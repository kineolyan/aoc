(ns aoc.day7
  (:require [clojure.string :as str]
            [clojure.java.math :as math]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  [16,1,2,0,4,2,7,1,2,14])

(defn direct-move
  [x y]
  (math/abs (- x y)))

(defn exp-move
  [x y]
  (let [diff (math/abs (- x y))]
    (/ (* diff (inc diff)) 2)))

(rcf/tests
  "exp-move"
  (exp-move 5 1) := 10
  (exp-move 2 5) := 6)


(defn compute-area
  [n xs f]
  (->> xs
       (map #(f % n))
       (reduce +)))

(rcf/tests
  "direct area"
  (compute-area 2 test-input direct-move) := 37
  (compute-area 1 test-input direct-move) := 41
  (compute-area 10 test-input direct-move) := 71
  "exponential area"
  (compute-area 2 test-input exp-move) := 206
  (compute-area 5 test-input exp-move) := 168)

(defn solve
  [xs f]
  (let [m (apply min xs)
        M (apply max xs)
        candidates (c/signed-range m M :inclusive true)
        areas (map #(compute-area % xs f) candidates)]
    (apply min areas)))

(rcf/tests
  "solve"
  (solve test-input direct-move) := 37
  (solve test-input exp-move) := 168)

(comment
  (def my-content (c/my-input "day7.txt"))
  (def my-input (c/parse-array parse-long (first my-content)))
  (apply max my-input)
  (apply min my-input)
  (count my-input)
  (solve my-input direct-move) ; 341558
  (solve my-input exp-move) ; 93214037
  )
