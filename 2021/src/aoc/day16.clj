(ns aoc.day16
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(defn bit->number
  [b]
  (case b
    \1 1
    \0 0))

(defn bits->number
  [bits]
  (reduce
    #(+ %2 (* 2 %1))
    0
    (map bit->number bits)))

(rcf/tests
  (bits->number "100") := 4
  (bits->number "000") := 0)

(def hexa-digit-in-binary
  {\0 "0000"
   \1 "0001"
   \2 "0010"
   \3 "0011"
   \4 "0100"
   \5 "0101"
   \6 "0110"
   \7 "0111"
   \8 "1000"
   \9 "1001"
   \A "1010"
   \B "1011"
   \C "1100"
   \D "1101"
   \E "1110"
   \F "1111"})

(defn hexa->binary
  [word]
  (mapcat
    hexa-digit-in-binary
    word))

(rcf/tests
  (hexa->binary "D2FE28") := (seq "110100101111111000101000"))

(declare parse-packet)

(defn read-packages
  [word]
  (loop [packages []
         word word]
    (let [[part word] (split-at 5 word)]
      (if (= \0 (first part))
        (let [packages (conj packages (rest part))]
          [packages word])
        (recur (conj packages (rest part))
               word)))))

(defn parse-literal
  [word]
  (let [[packages word] (read-packages word)
        value (bits->number (flatten packages))]
    [{:value value}
     word]))

(rcf/tests
  (parse-literal "101111111000101000") 
  := 
  [{:value 2021}
   (seq "000")]
  (parse-literal (subs "1111110000101111" 6))
  :=
  [{:value 1}
   (seq "01111")])

(defn parse-repeat-operator
  [word]
  (let [[c word] (split-at 11 word)
        c (bits->number c)
        [packets word] (reduce 
                         (fn [[res word] _]
                           (let [[packet word] (parse-packet word)]
                             [(conj res packet) word]))
                         [[] word]
                         (range c))]
  [{:packets packets} word]))

(comment
  (def word "01010000001100100000100011000001100000")
  (def c 3))

(defn parse-length-operator
  [word]
  (let [[c word] (split-at 15 word)
        c (bits->number c)
        [bits word] (split-at c word)
        packets (loop [ps []
                      word bits]
                  (if (seq word)
                    (let [[packet word] (parse-packet word)] 
                      (recur (conj ps packet)
                             word))
                    ps))]
  [{:packets packets} word]))

(defn parse-operator
  [word]
  (case (first word)
    \1 (parse-repeat-operator (rest word))
    \0 (parse-length-operator (rest word))))

(defn parse-packet
  [word]
  (let [[version word] (split-at 3 word)
        version (bits->number version)
        [id word] (split-at 3 word)
        id (bits->number id)
        [result word] (if (= id 4) 
                        (parse-literal word)
                        (parse-operator word))]
    [(merge result {:version version
                    :id id})
     word]))

(defn parse-hexa-packet
  [word]
  (parse-packet (hexa->binary word)))

(rcf/tests
  (parse-hexa-packet "D2FE28")
  :=
  [{:version 6
    :id 4
    :value 2021}
   (seq "000")]
  (parse-hexa-packet "EE00D40C823060")
  :=
  [{:version 7
    :id 3
    :packets [{:version 2
               :id 4
               :value 1}
              {:version 4
               :id 4
               :value 2}
              {:version 1
               :id 4
               :value 3}]}
   (seq "00000")])

(defn visit-packets
  [f packet]
  (f packet)
  (doseq [p (packet :packets)]
    (visit-packets f p)))

(defn solve1
  [word]
  (let [[packet _rest] (parse-hexa-packet word)
        total (atom 0)]
    (visit-packets #(swap! total + (get % :version 0)) packet)
    @total))

(rcf/tests
  (solve1 "8A004A801A8002F478") := 16
  (solve1 "620080001611562C8802118E34") := 12 
  (solve1 "C0015000016115A2E0802F182340") := 23
  (solve1  "A0016C880162017C3686B18A3D4780") := 31
  )

(defmulti evaluate :id)

(defmethod evaluate 4
  [packet]
  (packet :value))

(defn reduce-packs
  [f packet]
  (->> packet :packets (map evaluate) (reduce f)))

(defmethod evaluate 0
  [packet]
  (reduce-packs + packet))

(defmethod evaluate 1
  [packet]
  (reduce-packs * packet))

(defmethod evaluate 2
  [packet]
  (reduce-packs min packet))

(defmethod evaluate 3
  [packet]
  (reduce-packs max packet))

(defn comparison
  [f packet]
  (let [[p1 p2] (packet :packets)]
    (if (f (evaluate p1) (evaluate p2))
      1
      0)))

(defmethod evaluate 5
  [packet]
  (comparison > packet))

(defmethod evaluate 6
  [packet]
  (comparison < packet))

(defmethod evaluate 7
  [packet]
  (comparison = packet))

(defn solve2
  [word]
  (let [[packet _] (parse-hexa-packet word)]
    (evaluate packet)))

(rcf/tests
  (solve2 "C200B40A82") := 3
  (solve2 "04005AC33890") := 54
  (solve2 "880086C3E88112") := 7
  (solve2  "CE00C43D881120") := 9
  (solve2  "D8005AC2A8F0") := 1
  (solve2  "F600BC2D8F") := 0
  (solve2  "9C005AC2F8F0") := 0
  (solve2  "9C0141080250320F1802104A08") := 1
  )

(comment
  (def my-data (first (c/my-input "day16.txt")))
  (solve1 my-data) ; 995
  (solve2 my-data) ; 96257984154
  )

