(ns aoc.day21
  (:require [aoc.common :as c]
            [hyperfiddle.rcf :as rcf]
            [clojure.core.reducers :as r]))

(def test-data {:p1 4 :p2 8})
(def my-data {:p1 9 :p2 3})

(defn roll-dice-3
  ([] {:face 1})
  ([dice]
  (let [value (dice :face)]
    (cond
      (<= value 98) [(* 3 (inc value))
                     (update dice :face #(if (= 98 %) 1 (+ 3 %)))]
      (= value 99) [(+ 99 100 1)
                    (assoc dice :face 2)]
      (= value 100) [(+ 100 1 2)
                     (assoc dice :face 3)]))))

(rcf/tests
  (roll-dice-3) := {:face 1}
  (roll-dice-3 {:face 1}) := [6 {:face 4}]
  (roll-dice-3 {:face 50}) := [(+ 50 51 52) {:face 53}]
  (roll-dice-3 {:face 98}) := [(+ 98 99 100) {:face 1}]
  (roll-dice-3 {:face 99}) := [? {:face 2}]
  (roll-dice-3 {:face 100}) := [? {:face 3}])

(def board-size 10)
(defn update-player
  [position x]
  (let [new-position (+ position x)]
    (inc
      (mod (dec new-position) board-size))))

(rcf/tests
  (update-player 9 100) := 9
  (update-player 3 4) := 7
  (update-player 1 299) := 10)

(defn run-turn
  [state]
  (let [player (state :current)
        [value die] ((state :roll) (get-in state [player :die]))]
    (as-> state $
      (update-in $ [player :position] update-player value)
      (update-in $ [player :score] + (get-in $ [player :position]))
      (update $ :turn inc)
      (update-in $ [player :die] (constantly die))
      (assoc $ :current (if (= player :p1) :p2 :p1)))))

(rcf/tests
  (run-turn {:turn 20
             :die {:face 1}
             :roll roll-dice-3
             :current :p1
             :p1 {:position 4 :score 10}
             :p2 {:position 8 :score 52}})
  :=
  {:turn 21
   :die {:face 4}
   :roll roll-dice-3
   :current :p2
   :p1 {:position 10 :score 20}
   :p2 {:position 8 :score 52}})

(defn play
  [initial-state]
  (take ; to avoid infinite sequence
    1000 
    (iterate run-turn initial-state)))

(defn create-init-state
  [{:keys [p1 p2]}]
  (let [single-dice (atom (roll-dice-3))
        roll-fn #(let [[v new-state] (roll-dice-3 (deref %))]
                   (reset! single-dice new-state)
                   [v single-dice])]
  {:turn 0
   :roll roll-fn
   :current :p1
   :p1 {:position p1 :score 0 :die single-dice}
   :p2 {:position p2 :score 0 :die single-dice}}))

(def test-init-state (create-init-state test-data))

(defn solve1
  [initial-state target-score]
  (let [final-state (first (drop-while #(and (< (get-in % [:p1 :score]) target-score)
                                             (< (get-in % [:p2 :score]) target-score))
                                       (play initial-state)))]
    (* (get-in final-state [(final-state :current) :score])
       (* 3 (final-state :turn)))))

(rcf/tests
  (solve1 test-init-state 1000) := 739785)

(defrecord abc [a b c])
(class (->abc 1 2 3))
(class (assoc (->abc 1 2 3) :a 4))

(defn win?
  [initial-state target-score player]
  (let [final-state (first (drop-while #(and (< (get-in % [:p1 :score]) target-score)
                                             (< (get-in % [:p2 :score]) target-score))
                                       (play initial-state)))]
    (not= player (final-state :current))))

(rcf/tests
  (win? test-init-state 1000 :p1) := true)

(def quantum-throws
  (frequencies (for [x (range 1 4)
                     y (range 1 4)
                     z (range 1 4)]
                 (+ x y z))))

(defn create-state2
  [data]
  {:current :p1
   :p1 {:position (data :p1) :score 0}
   :p2 {:position (data :p2) :score 0}})

(def test-state2 (create-state2 test-data))

(defn run-turn2
  [state value]
  (let [player (state :current)]
    (as-> state $
      (update-in $ [player :position] update-player value)
      (update-in $ [player :score] + (get-in $ [player :position]))
      (assoc $ :current (if (= player :p1) :p2 :p1)))))

(comment
  (run-turn2 
    (run-turn2 test-state2 6)
    4))

(defn winner
  [target-score state]
  (cond
    (<= target-score (-> state :p1 :score)) :p1
    (<= target-score (-> state :p2 :score)) :p2
    :else nil))

(defn merge-stats
  [stats result]
  (let [get-stat #(* (:count %1) (get-in %1 [:stats %2]))]
  (-> stats
      (update :p1 (fnil + 0) (get-stat result :p1))
      (update :p2 (fnil + 0) (get-stat result :p2)))))

(rcf/tests
  (merge-stats {:p1 3N} {:count 10N :stats {:p1 1N :p2 2N}}) := {:p1 13N :p2 20N})

(def compute-stats
  (memoize 
    (fn [state]
      (let [runs (map #(hash-map :state (run-turn2 state (first %)) :count (second %)) quantum-throws)
            {wins :p1 losses :p2 others nil} (group-by #(winner 21 (:state %)) runs)
            win-loss-results {:p1 (reduce + 0N (map :count wins)) 
                              :p2 (reduce + 0N (map :count losses))}
            other-results (map #(assoc % :stats (compute-stats (:state %))) others)]
        (reduce
          merge-stats
          win-loss-results
          other-results)))))

(rcf/tests
  (compute-stats test-state2) := {:p1 444356092776315N :p2 341960390180808N})

(defn solve2
  [state]
  (let [stats (compute-stats state)]
    (apply max (vals stats))))

(comment
  (take 5 (drop 330 (play test-init-state)))
  (take 10 (play test-init-state))
  ; solving
  (solve1 (create-init-state my-data) 1000) ; 1073709
  (solve2 test-state2)
  (solve2 (create-state2 my-data))
  )
