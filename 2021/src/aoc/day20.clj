(ns aoc.day20
  (:require [aoc.common :as c]
            [clojure.string :as str]
            [clojure.core.reducers :as r]
            [hyperfiddle.rcf :as rcf]))

(def test-input
  (str/split-lines
    "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###"))

(defn char->bit
  [c]
  (case c 
    \. 0
    \# 1))

(defn get-raw-pixel
  [image coord]
  (char->bit (c/get-point image coord)))

; 2**64 -> 1 bit for sign for each, 2 spare => use 2**30 at most
(def factor (Long/parseLong "10000" 16))
(def half (int (/ factor 2)))
(defn point->n
  [{:keys [x y]}]
  (+ (* (+ half y) factor)
     (+ half x)))

(defn grid->map
  [grid]
  (let [{:keys [w h]} (c/get-dims grid)]
    (persistent!
      (reduce
        #(assoc! %1 (point->n %2) (get-raw-pixel grid %2))
        (transient {})
        (for [x (range w)
              y (range h)]
          {:x x :y y})))))

(defn parse-input
  [lines]
  (let [[dic _ & image] lines]
    {:converter (mapv char->bit dic)
     :default-pixel 0
     :dims (let [{:keys [h w]} (c/get-dims image)] {:fx 0 :tx w :fy 0 :ty h})
     :image (grid->map image)}))

(def test-data (parse-input test-input))

(defn get-pixel
  [image coord]
  (get image coord))

(defn set-pixel
  [image coord v]
  (update image coord (constantly v)))

(rcf/tests
  "get-pixel"
  ; in the box
  (get-pixel (test-data :image) (point->n {:x 3 :y 0})) := 1
  (get-pixel (test-data :image) (point->n {:x 0 :y 3})) := 0
  ; out of the box
  (get-pixel (test-data :image) (point->n {:x -1 :y -1})) := nil)

(defn region-coords
  [point]
  (for [dy [(- factor) 0 factor]
        dx [-1 0 1]]
    (+ point dy dx)))

(defn binary->number
  [bits]
  (reduce
    #(+ %2 (* 2 %1))
    0
    bits))

(rcf/tests
  (binary->number [0 0 0 1]) := 1
  (binary->number [1 1 1 0]) := 14)

(defn convert-pixel
  [data value]
  (get (data :converter) value))

(defn enhance-pixel
  [data coord]
  {:post [(some? %)]}
  (let [default (data :default-pixel)
        coords (region-coords coord)
        word (map #(or (get-pixel (data :image) %) default) coords)
        value (binary->number word)]
    (convert-pixel data value)))

(rcf/tests
  (enhance-pixel test-data (point->n {:x 2 :y 2})) := 1
  (enhance-pixel test-data (point->n {:x 1 :y 2})) := 1
  (enhance-pixel test-data (point->n {:x 2 :y 1})) := 1
  (enhance-pixel test-data (point->n {:x 0 :y 0})) := 0)

(defn update-default-pixel
  [data]
  (case (data :default-pixel)
    0 (convert-pixel data 0)
    1 (convert-pixel data 511)))

(defn pixel-seq
  [data]
  (let [{:keys [fx tx fy ty]} (data :dims)]
    (for [y (range (dec fy) (inc ty))
          x (range (dec fx) (inc tx))] 
      (point->n {:x x :y y}))))

(defn compute-pixels
  ([_data] {})
  ([data m point]
    (assoc m point (enhance-pixel data point))))

(defn merge-pixels
  ([] {})
  ([& ms] (apply merge ms)))

(defn enhance-image
  [data]
  (let [pixel-coords (pixel-seq data)
        new-image (r/fold merge-pixels (partial compute-pixels data) pixel-coords)]
    (assoc data 
           :image new-image
           :dims (-> (data :dims)
                     (update :fx dec)
                     (update :tx inc)
                     (update :fy dec)
                     (update :ty inc))
           :default-pixel (update-default-pixel data))))

(defn image->string
  [{:keys [image dims]}]
  (let [{:keys [tx ty fx fy]} dims]
    (doseq [y (range fy tx)]
      (doseq [x (range fx tx)]
        (print (case (get image (point->n {:x x :y y})) 0 \. 1 \#)))
      (println))))

(rcf/tests
  (enhance-image test-data))
(comment
  (image->string *1)
  (enhance-image *2))

(defn solve
  [data times]
  (let [result (reduce (fn [d _] (enhance-image d)) data (range times))]
    (->> (result :image)
         (vals)
         (filter pos?)
         (count))))

(rcf/tests
  (solve test-data 2) := 35
  (solve test-data 50) := 3351)

(comment
  (def my-data (parse-input (c/my-input "day20.txt")))
  (c/get-dims (my-data :image))
  (solve my-data 2) ; 5563

  ; too brute force :)
  (c/profile 10 #(solve test-data 50))
  (c/profile 20 #(solve my-data 20))
  (solve my-data 50)
  )

