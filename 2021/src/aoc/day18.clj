(ns aoc.day18
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  (str/split-lines
    "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"))

(defn parse
  [inputs]
  (map read-string inputs))

(def test-data (parse test-input))

(def other-data
  (parse
    (str/split-lines
      "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]")))

;;; Tree operations

(defn get-in-tree
  [tree stack]
  (let [k (->> stack reverse vec)]
    (get-in tree k)))

(defn update-in-tree
  [tree stack f & args]
  (let [k (->> stack reverse vec)]
    (apply update-in (concat [tree k f] args))))

(rcf/tests
  "get-in-tree"
  (def tree [[[[5 2]
               5]
              [8 
               [3 7]]] 
             [[5 
               [7 5]] 
              [4 4]]])
  (def stack [0])
  (defn ->stack [& s] (into '() s))
  (->stack 1 0) := '(0 1)
  (get-in-tree tree (->stack 0)) := [[[5 2] 5] [8 [3 7]]]
  (get-in-tree tree (->stack 1)) := [[5 [7 5]] [4 4]]
  (get-in-tree tree (->stack 1 0)) := [5 [7 5]]
  (get-in-tree tree (->stack 1 0 0)) := 5
  (get-in-tree tree (->stack 1 0 1)) := [7 5]
  (get-in-tree tree (->stack 1 0 1 0)) := 7
  "update-in-tree"
  (update-in-tree [1 [2 4]] (->stack 1 0) inc) := [1 [3 4]])

(defn node?
  [x]
  (vector? x))

(defn left?
  [x]
  {:pre [(number? x)]}
  (zero? x))

(defn right?
  [x]
  {:pre [(number? x)]}
  (pos? x))

(defn sink-in-tree
  [tree stack side]
  {:pre [(or (left? side) (right? side))]}
  (when-let [v (get-in-tree tree stack)]
    (if-not (node? v)
      stack
      (sink-in-tree tree (cons side stack) side))))

(rcf/tests
  "sink"
  (sink-in-tree tree '() 0) := '(0 0 0 0)
  (sink-in-tree tree '(1 0) 0) := '(0 1 0)
  (sink-in-tree tree '(1 1 1 1 1) 0) := nil)

(defn next-in-tree
  "Returns the stack referring to the next element in the tree, or nil if the iteration is complete."
  [tree [x & xs]]
  (if (left? x) 
    (sink-in-tree tree (cons 1 xs) 0)
    (when-let [xs (->> xs (drop-while right?) seq)]
      (sink-in-tree tree (cons 1 (rest xs)) 0))))

(rcf/tests
  "next-in-tree"
  (def stack '(0))
  (next-in-tree tree '(0 0 0 0)) := '(1 0 0 0)
  (next-in-tree tree '(1 0 0 0)) := '(1 0 0)
  (next-in-tree tree '(1 0 0)) := '(0 1 0)
  (next-in-tree tree '(0 1 0)) := '(0 1 1 0)
  (next-in-tree tree '(1 1 1 0)) := '(0 0 1)
  (next-in-tree tree '(1 1 1)) := nil)

(defn prev-in-tree
  "Returns the stack referring to the next element in the tree, or nil if the iteration is complete."
  [tree [x & xs]]
  (if (right? x) 
    (sink-in-tree tree (cons 0 xs) 1)
    (when-let [xs (->> xs (drop-while left?) seq)]
      (sink-in-tree tree (cons 0 (rest xs)) 1))))

(rcf/tests
  "prev-in-tree"
  ; define wrt next-in-tree
  (prev-in-tree tree '(0 0 0 0)) := nil
  (prev-in-tree tree '(1 0 0 0)) := '(0 0 0 0)
  (prev-in-tree tree '(1 0 0)) := '(1 0 0 0)
  (prev-in-tree tree '(0 1 0)) := '(1 0 0)
  (prev-in-tree tree '(0 1 1 0)) := '(0 1 0)
  (prev-in-tree tree '(1 1 1 0)) := '(0 1 1 0)
  (prev-in-tree tree '(1 1 1)) := '(0 1 1))

(defn tree->seq
  [tree]
  (take-while
    some?
    (iterate #(next-in-tree tree %) (sink-in-tree tree '() 0))))


;;; snail-fish math

(defn snail-explode
  [number stack]
  (let [[l r] (get-in-tree number stack)
        p (prev-in-tree number (cons 0 stack))
        n (next-in-tree number (cons 1 stack))
        number (if p (update-in-tree number p + l) number)
        number (if n (update-in-tree number n + r) number)]
    (update-in-tree number stack (constantly 0))))

(rcf/tests
  (snail-explode [[[[[9,8],1],2],3],4] 
                 (->stack 0 0 0 0)) 
  := 
  [[[[0,9],2],3],4]
  (snail-explode [7,[6,[5,[4,[3,2]]]]]
                 (->stack 1 1 1 1))
  :=
  [7,[6,[5,[7,0]]]]
  (snail-explode [[6,[5,[4,[3,2]]]],1]
                 (->stack 0 1 1 1))
  :=
  [[6,[5,[7,0]]],3]
  (snail-explode [[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]
                 (->stack 0 1 1 1))
  :=
  [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]
  (snail-explode [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]
                 (->stack 1 1 1 1))
  :=
  [[3,[2,[8,0]]],[9,[5,[7,0]]]])

(defn snail-split
  [number stack]
  (let [v (get-in-tree number stack)
        l (int (/ v 2))
        r (- v l)]
    (update-in-tree number stack (constantly [l r]))))

(defn snail-reduce-by-explode
  [number stack]
  (if (> (count stack) 4)
    (reduced (snail-explode number (rest stack)))
    number))

(defn snail-reduce-by-split
  [number stack]
  (let [v (get-in-tree number stack)]
    (if (>= v 10)
      (reduced (snail-split number stack))
      number)))

(defn snail-reduce
  [number]
  (let [ps (tree->seq number)
        exploded (reduce snail-reduce-by-explode number ps)]
    (if (not= number exploded)
      exploded
      (reduce snail-reduce-by-split number ps))))

(rcf/tests
  (snail-reduce [[[[[4 3] 4] 4] [7 [[8 4] 9]]] [1 1]])
  :=
  [[[[0 7] 4] [7 [[8 4] 9]]] [1 1]]
  (snail-reduce [[[[0 7] 4] [7 [[8 4] 9]]] [1 1]])
  :=
  [[[[0 7] 4] [15 [0 13]]] [1 1]]
  (snail-reduce [[[[0 7] 4] [15 [0 13]]] [1 1]])
  :=
  [[[[0 7] 4] [[7 8] [0 13]]] [1 1]]
  (snail-reduce [[[[0 7] 4] [[7 8] [0 13]]] [1 1]])
  :=
  [[[[0 7] 4] [[7 8] [0 [6 7]]]] [1 1]]
  (snail-reduce [[[[0 7] 4] [[7 8] [0 [6 7]]]] [1 1]])
  :=
  [[[[0 7] 4] [[7 8] [6 0]]] [8 1]])

(defn snail-add
  [a b]
  (loop [x [a b]]
    (let [y (snail-reduce x)]
      (if (= x y)
        x
        (recur y)))))

(comment
  (snail-reduce
    [
     [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
     [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]])
  (snail-reduce *1))

(rcf/tests
  (snail-add [[[[4 3] 4] 4] [7 [[8 4] 9]]] [1 1])
  := 
  [[[[0 7] 4] [[7 8] [6 0]]] [8 1]]

  ; testing on the large example
  (snail-add
    [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
    [7,[[[3,7],[4,3]],[[6,3],[8,8]]]])
  := [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
  (snail-add
    [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
    [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]])
  := [[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]
  (snail-add
    [[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]
    [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]])
  := [[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]
  (snail-add
    [[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]
    [7,[5,[[3,8],[1,4]]]])
  := [[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]
  (snail-add
    [[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]
    [[2,[2,2]],[8,[8,1]]])
  := [[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]
  (snail-add
    [[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]
    [2,9])
  := [[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]
  (snail-add
    [[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]
    [1,[[[9,3],9],[[9,0],[0,7]]]])
  := [[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]
  (snail-add
    [[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]
    [[[5,[7,4]],7],1])
  := [[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]
  (snail-add
    [[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]
    [[[[4,2],2],6],[8,7]])
  := [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]
  )

;;; magnitude

(defn magnitude
  [number]
  #_{:post [(number? %)]}
  (if (node? number)
    (+ (* 3 (magnitude (first number))) 
       (* 2 (magnitude (second number))))
    number))

(rcf/tests
  (magnitude 1) := 1
  (magnitude [1 10]) := 23
  (magnitude [[1 2] [[3 4] 5]]) := 143
  (magnitude [[[[0 7] 4] [[7 8] [6 0]]] [8 1]]) := 1384
  (magnitude [[[[1 1] [2 2]] [3 3]] [4 4]]) := 445
  (magnitude [[[[3 0] [5 3]] [4 4]] [5 5]]) := 791
  (magnitude [[[[5 0] [7 4]] [5 5]] [6 6]]) := 1137
  (magnitude [[[[8 7] [7 7]] [[8 6] [7 7]]] [[[0 7] [6 6]] [8 7]]]) := 3488
  (magnitude [[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]) := 4140)

(comment
  (get-in [[[3 4] 1] [2]] [0 0 1])
  (update-in [[[3 4] 1] [2]] [0 0 1] (constantly 10)))

(defn solve1
  [inputs]
  (magnitude (reduce snail-add inputs)))

(rcf/tests
  (reduce snail-add [[1,1] [2,2] [3,3] [4,4]])
  := [[[[1,1],[2,2]],[3,3]],[4,4]]
  (reduce snail-add [[1,1] [2,2] [3,3] [4,4] [5,5]])
  :=
  [[[[3,0],[5,3]],[4,4]],[5,5]]
  (reduce snail-add [[1,1] [2,2] [3,3] [4,4] [5,5] [6,6]])
  :=
  [[[[5,0],[7,4]],[5,5]],[6,6]]


  (reduce snail-add other-data)
  :=
  [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]
  (solve1 other-data) := 3488

  (reduce snail-add test-data)
  :=
  [[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]
  (solve1 test-data) := 4140)

(defn solve2
  [inputs]
  (reduce
    max
    (for [a inputs
          b inputs
          :when (not= a b)]
      (magnitude (snail-add a b)))))

(rcf/tests
  (solve2 test-data) := 3993)

(comment
  (def my-data (parse (c/my-input "day18.txt")))
  (solve1 my-data) ; 3524
  (solve2 my-data) ; 4656
  )
