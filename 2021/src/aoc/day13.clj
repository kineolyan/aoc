(ns aoc.day13
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  (str/split-lines
  "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5"))

(defn parse-dot
  [input]
  (let [[x y] (str/split input #",")]
    {:x (parse-long x)
     :y (parse-long y)}))

(defn parse-instruction
  [input]
  (let [[_ axis coord] (re-find #"fold along (x|y)=(\d+)" input)]
    {:axis (if (= "x" axis) :x :y)
     :value (parse-long coord)}))

(defn parse-input
  [input]
  (let [[coords instructions] (split-with (complement str/blank?) input)]
    {:paper (map parse-dot coords)
     :instructions (map parse-instruction (next instructions))}))

(def test-data
  (parse-input test-input))

(defn update-coord
  [v value]
  (- (* 2 value) v))

(defn dot-after-fold
  [dot axis value]
  (update dot axis update-coord value))

(rcf/tests
  (dot-after-fold {:x 6 :y 0} :x 5) := {:x 4 :y 0}
  (dot-after-fold {:x 1 :y 10} :y 7) := {:x 1 :y 4})

(defn fold
  [dots {:keys [axis value]}]
  (let [pred #(< (get % axis) value)
        untouched (filter pred dots)
        folded (filter (complement pred) dots)
        re-coords (map #(dot-after-fold % axis value) folded)]
    (set (concat untouched re-coords))))

(defn fold-input
  [data]
  (reduce
    #(fold %1 %2)
    (data :paper)
    (data :instructions)))

(defn draw-paper
  [dots]
  (let [rows (group-by :y dots)
        ys (sort (keys rows))]
    (doseq [y ys]
      (let [dots (set (map :x (get rows y)))
            M (apply max dots)]
        (doseq [x (range (inc M))]
          (if (contains? dots x)
            (print "#")
            (print " ")))
        (println)))))

(defn solve
  [data]
  (let [result (fold-input data)]
    (draw-paper result)))

(rcf/tests
  "first fold"
  (count (fold (test-data :paper) (first (test-data :instructions)))) := 17)

(comment
  (def my-data (parse-input (c/my-input "day13.txt")))
  (count (my-data :instructions))
  (solve my-data)
  (let [{:keys [paper instructions]} my-data]
    (count (fold paper (first instructions))))
  ;part 2
  (solve test-data)
  (solve my-data)
  )
