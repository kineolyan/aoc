(ns aoc.day1
  (:require [aoc.common :as c]
            [clojure.string :as str]))

(defn parse-input
  [f]
  (let [data (slurp f)
        lines (str/split-lines data)]
    (map parse-long lines)))

(defn solve
  [depths]
  (->> (map #(> %2 %1) depths (rest depths)) 
     (filter true?)
     (count)))

(comment
  (def test-input [199
                   200
                   208
                   210
                   200
                   207
                   240
                   269
                   260
                   263])
  (take 10 (parse-input "/home/olivier/tmp/aoc/d1.txt"))
  (solve test-input)
  (solve (parse-input "/home/olivier/tmp/aoc/d1.txt")))
