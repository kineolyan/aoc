(ns aoc.day4
  (:require [aoc.common :as c]
            [clojure.string :as str]))

(def test-input
  (str/split-lines
  "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"))

(defn rows->grid
  [rows]
  (mapv #(as-> % e 
           (str/trim e)
           (str/split e #"\s+")
           (mapv parse-long e))
        rows))

(comment
  (def grid (->> test-input (take 7) (drop 2) rows->grid)))

(defn grid-row
  [grid i]
  (nth grid i))

(defn grid-col
  [grid i]
  (mapv #(nth % i) grid))

(comment
  (grid-row grid 1)
  (grid-col grid 4))

(defn grid-seq
  "Returns the values in the grid with their coordinates, as a sequence of `(<val> [<row> <col>])`"
  [grid]
  (map vector 
       (flatten grid) 
       (for [x (range 5) y (range 5)] [x y])))

(comment
  (grid-seq grid))

(defn grid-position
  "returns the coordinates of the number if present in the grid, or nil."
  [grid n]
  (->> (grid-seq grid)
       (filter (fn [[v _coord]] (= v n)))
       first
       second))

(comment
  (grid-position grid 22)
  (grid-position grid 18)
  (grid-position grid -1))

(defn parse-input
  [lines]
  (let [[numbers _ & grids] lines]
    {:draws (as-> numbers e 
              (str/split e #",")
              (map parse-long e))
     :grids (map rows->grid (partition 5 6 grids))
     :picked #{}}))

(comment
  (parse-input test-input))

(defn score-grid
  [grid picked]
  (let [numbers (flatten grid)
        unmarked (filter (complement picked) numbers)]
    (reduce + unmarked)))

(comment 
  (score-grid grid #{22 8 2 20})
  (- (reduce + (flatten grid)) (+ 22 8 2 20)))

(defn win?
  [grid picked n]
  (when-let [[x y] (grid-position grid n)]
    (or (every? picked (grid-row grid x))
        (every? picked (grid-col grid y)))))

(comment
  (win? grid (set (first grid)) (ffirst grid))
  (win? grid (set (second grid)) (ffirst grid)))

(defn play-turn
  [grids picked n]
  (filter #(win? % picked n) grids))

(defn run-to-win
  [{:keys [draws grids picked] :as _state}]
  (let [n (first draws)
        picked (conj picked n)
        winners (play-turn grids picked n)
        winner (c/only-first winners)
        score (when winner (* n (score-grid winner picked)))]
    {:draws (rest draws)
     :grids grids
     :picked picked
     :score score}))

(defn play
  [strategy input]
  (loop [state input]
    (let [{:keys [score] :as state} (strategy state)]
      (cond 
        (some? score) score
        (empty? (state :draws)) (throw (IllegalStateException. "Cannot find solution"))
        :else (recur state)))))

(defn run-to-lose
  [{:keys [draws grids picked] :as _state}]
  (let [n (first draws)
        picked (conj picked n)
        winners (play-turn grids picked n)
        remaining-grids (filterv (complement (set winners)) grids)
        score (when (and (seq winners) (empty? remaining-grids)) 
                (* n (score-grid (c/only-first winners) picked)))]
    ;(tap> (str (if (seq winners) "ok" "ko") " -> remaining grids = " (count remaining-grids)))
    {:draws (rest draws)
     :grids remaining-grids
     :picked picked
     :score score}))

(comment
  (def test-data (parse-input test-input))
  (play-turn (test-data :grids) (set (first grid)) (ffirst grid))
  (play-turn (test-data :grids) #{} (ffirst grid))

  (defn mine [] (parse-input (c/my-input "day4.txt")))
  ; part 1
  (play run-to-win test-data) ; 4512
  (play run-to-win (mine)) ; 65325
  ; part 2
  (play run-to-lose test-data) ; 1924
  (play run-to-lose (mine)))

