(ns aoc.day10
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  (str/split-lines
  "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"))

(def openings (set "([{<"))


(def matches (apply hash-map (seq "{}()[]<>")))

(defn find-error
  [stack c]
  (if (contains? openings c)
    (cons c stack)
    (if (= (matches (first stack)) c)
      (rest stack)
      (reduced [:error c]))))

(rcf/tests
  (find-error (seq "[{") \]) := (seq "{")
  (find-error (seq "[{") \<) := (seq "<[{")
  (unreduced (find-error (seq "[{") \>)) := [:error \>])

(defn find-first-error
  [input]
  (let [result (reduce find-error '() input)]
    (when (= (first result) :error)
      (second result))))

(rcf/tests
  (find-first-error "{([(<{}[<>[]}>{[]{[(<()>") := \})

(def ratings (into {} (map vector (seq ">)]}") [25137 3 57 1197])))

(defn solve1
  [inputs]
  (let [errors (map find-first-error inputs)
        scores (map #(get ratings % 0) errors)]
    (reduce + scores)))

(def closing-ratings
  (into {} (map vector (seq "<([{") [4 1 2 3])))

(defn rate-completion
  [cs]
  (reduce 
    #(+ (* 5 %1) (closing-ratings %2))
    0
    cs))

(defn middle-value
  [xs]
  {:post [(odd? (count xs))]}
  (let [n (count xs)
        mid (long (/ n 2))]
    (nth (sort xs) mid)))

(rcf/tests
  (middle-value [7 1 6 2 5 3 4]) := 4)

(defn solve2
  [inputs]
  (->> inputs
       (map #(reduce find-error '() %))
       (filter #(not= :error (first %)))
       (map rate-completion)
       (middle-value)))

(rcf/tests
  (solve1 test-input) := 26397
  (solve2 test-input) := 288957)

(comment
  (def my-input (c/my-input "day10.txt"))
  (count my-input)
  (-> my-input first count)
  (solve1 my-input) ; 323691
  (solve2 my-input)
  )

