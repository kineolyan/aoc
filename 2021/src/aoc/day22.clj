(ns aoc.day22
  (:require [aoc.common :as c]
            [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]))

(def test-input
  (str/split-lines
    "on x=-20..26,y=-36..17,z=-47..7
on x=-20..33,y=-21..23,z=-26..28
on x=-22..28,y=-29..23,z=-38..16
on x=-46..7,y=-6..46,z=-50..-1
on x=-49..1,y=-3..46,z=-24..28
on x=2..47,y=-22..22,z=-23..27
on x=-27..23,y=-28..26,z=-21..29
on x=-39..5,y=-6..47,z=-3..44
on x=-30..21,y=-8..43,z=-13..34
on x=-22..26,y=-27..20,z=-29..19
off x=-48..-32,y=26..41,z=-47..-37
on x=-12..35,y=6..50,z=-50..-2
off x=-48..-32,y=-32..-16,z=-15..-5
on x=-18..26,y=-33..15,z=-7..46
off x=-40..-22,y=-38..-28,z=23..41
on x=-16..35,y=-41..10,z=-47..6
off x=-32..-23,y=11..30,z=-14..3
on x=-49..-5,y=-3..45,z=-29..18
off x=18..30,y=-20..-8,z=-3..13
on x=-41..9,y=-7..43,z=-33..15
on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
on x=967..23432,y=45373..81175,z=27513..53682"))

(defn parse-range
  [input]
  (let [[start end] (str/split input #"\.\.")]
    {:from (parse-long start)
     :to (parse-long end)}))

(rcf/tests
  (parse-range "-20..26") := {:from -20 :to 26})

(defn parse-line
  [line]
  (let [[_ state xrange yrange zrange] (re-find #"(on|off) x=([\-\d.]+),y=([\-\d.]+),z=([\-\d.]+)"
                                                line)]
    {:on (= state "on")
     :x (parse-range xrange)
     :y (parse-range yrange)
     :z (parse-range zrange)}))

(def test-data (map parse-line test-input))

(defn range-to-matcher
  [{:keys [from to]}]
  (fn [v] (<= from v to)))

(defn datum-to-matcher
  [datum]
  (let [x-matcher (range-to-matcher (datum :x))
        y-matcher (range-to-matcher (datum :y))
        z-matcher (range-to-matcher (datum :z))]
    {:on (datum :on)
     :x x-matcher
     :y y-matcher
     :z z-matcher
     :all (fn [point] (and (x-matcher (:x point))
                           (y-matcher (:y point))
                           (z-matcher (:z point))))}))


(rcf/tests
  ((range-to-matcher {:from 1 :to 10}) 5) := true
  (def some-matcher (datum-to-matcher {:on true
                                       :x {:from 1 :to 4}
                                       :y {:from -3 :to 0}
                                       :z {:from -1 :to 1}}))
  ((some-matcher :x) 1) := true
  ((some-matcher :x) 4) := true
  ((some-matcher :x) 2) := true
  ((some-matcher :x) 0) := false
  ((some-matcher :x) 5) := false
  ((some-matcher :y) -2) := true
  ((some-matcher :y) 1) := false
  ((some-matcher :z) 1) := true
  ((some-matcher :z) 2) := false
  ((some-matcher :all) {:x 1 :y 0 :z 1}) := true)

(def test-matchers (reverse (map datum-to-matcher test-data)))

(defrecord point [x y z])

(defn restrict-range
  [matchers axis]
  (filter 
    (fn [x] (some #((axis %) x) matchers))
    (range -50 51)))

(defn on?
  [matchers point]
  (let [on-zone (filter #((:all %) point) matchers)]
    (-> on-zone first :on boolean)))

(rcf/tests
  (on? test-matchers (->point 967 45400 27520)) := true
  (on? test-matchers (->point 19 -10 0)) := false)

(defn solve
  [matchers]
  (let [x-range (restrict-range matchers :x)
        y-range (restrict-range matchers :y)
        z-range (restrict-range matchers :z)
        candidates (for [x x-range y y-range z z-range]
                     (->point x y z))]
    (reduce
      #(if (on? matchers %2) (inc %1) %1)
      0
      candidates)))

(rcf/tests
  (solve test-matchers) := 590784)

(comment
  (def my-input (c/my-input "day22.txt"))
  (def my-matchers (->> my-input
                        (map parse-line)
                        (map datum-to-matcher)
                        reverse))
  (solve my-matchers))
