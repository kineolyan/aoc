(ns aoc.day23-2
  (:require [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  {:a [:b :d :d :a]
   :b [:c :c :b :d]
   :c [:b :b :a :c]
   :d [:d :a :c :a]})

(def rooms [:a :b :c :d])

(defn build-amphipod
  [type]
  {:type type
   :moved? false})

(defn build-state
  [input]
  (->> input
       (mapcat (fn [[room amphipods]] (map #(vector [room (inc %2)] (build-amphipod %1)) amphipods (range))))
       (into {})))

(def state (build-state test-input))

(def ^:dynamic *depth* 4)
(defn positions-in-room
  [room]
  (map #(vector room %) (range 1 (inc *depth*))))

(binding [*depth* 1]
  (positions-in-room :a))

(defn pods-in-room
  "Returns all pods present in the room (ignoring empty spaces)
  Pods are returned in order, according to their positions."
  [state room]
  (->> 
    (map #(get state %) (positions-in-room room))
    (filter some?)))

(defn first-pod-position-in-room
  [state room]
  (->> (positions-in-room room)
       (filter #(contains? state %))
       (first)))

(defn first-free-position-in-room
  [state room]
  {:pre [(some #{room} rooms)]}
  (->> (positions-in-room room)
       (take-while #(not (contains? state %)))
       (last)))

(rcf/tests
  (map :type (pods-in-room state :b)) := [:c :c :b :d]
  (map :type (pods-in-room (dissoc state [:b 3]) :b)) := [:c :c :d]
  (first-pod-position-in-room state :d) := [:d 1]
  (first-free-position-in-room state :c) := nil
  (first-free-position-in-room (dissoc state [:a 1]) :a) := [:a 1]
  (first-free-position-in-room (dissoc state [:a 1] [:a 2]) :a) := [:a 2]
  (first-free-position-in-room (dissoc state [:a 1] [:a 3]) :a) := [:a 1])

(defn victory-in-room?
  [state room]
  (let [pods (pods-in-room state room)]
    (and (= *depth* (count pods))
         (every? #(= room (:type %)) pods))))

(defn victory?
  [state]
  (every? #(victory-in-room? state %) rooms))

(rcf/tests
  (victory-in-room? state :a) := false
  (victory? state) := false
  (victory? (->>
              (mapcat positions-in-room rooms)
              (map #(vector % {:type (first %)}))
              (into {}))) 
  := true
  (victory? {[:h 1] {:type :a
                     :moved? true}
             [:h 2] {:type :b
                     :moved? true}
             [:h 8] {:type :c
                     :moved? true}
             [:h 6] {:type :d
                     :moved? true}})
  := false)

(def possible-hall-spots
  (map #(vector :h %) [1 2 4 6 8 10 11]))

(defn free-hall-spots
  [state]
  (filter #(nil? (get state %)) possible-hall-spots))

(defn free?
  [state position]
  (not (contains? state position)))

(rcf/tests
  (free? state [:h 1]) := true
  (free? state [:a 1]) := false)

(defn in-hallway?
  [position]
  (= :h (first position)))

(defn in-room?
  [position]
  (not (in-hallway? position)))

(def room-entries
  {:a [:h 3]
   :b [:h 5]
   :c [:h 7]
   :d [:h 9]})

(defn path-from-hallway
  [src dst]
  {:pre [(in-hallway? src)
         (not (in-hallway? dst))]}
  (let [room (first dst) 
        entry (get room-entries (first dst))
        hallway-positions (map #(vector :h %) 
                               (c/signed-range (second src) (second entry) :inclusive true))
        room-positions (map #(vector room %)
                            (c/signed-range 1 (second dst) :inclusive true))]
    (concat hallway-positions room-positions)))

(defn path-to-hallway
  [src dst]
  {:pre [(not (in-hallway? src))
         (in-hallway? dst)]}
  (reverse (path-from-hallway dst src)))

(rcf/tests
  (path-from-hallway [:h 1] [:b 2]) := [[:h 1] [:h 2] [:h 3] [:h 4] [:h 5]
                                        [:b 1] [:b 2]]
  (path-from-hallway [:h 8] [:c 1]) := [[:h 8] [:h 7]
                                        [:c 1]]
  (path-to-hallway [:d 1] [:h 11]) := [[:d 1] 
                                       [:h 9] [:h 10] [:h 11]])

(defn valid-path?
  [state path]
  (every? #(free? state %) (rest path)))

(def costs
  {:a 1
   :b 10
   :c 100
   :d 1000})

(defn path-cost
  [state path]
  (let [pod (get state (first path))]
  (* (get costs (:type pod))
     (dec (count path)))))

(defn cost-from-hallway
  [state src dst]
  (let [path (path-from-hallway src dst)]
    (when (valid-path? state path)
      (path-cost state path))))

(defn cost-to-hallway
  [state src dst]
  {:pre [(not (in-hallway? src))
         (in-hallway? dst)]}
  (let [path (path-to-hallway src dst)]
    (when (valid-path? state path)
      (path-cost state path))))

(defn progress-move?
  "Tests if the state after the move is progressing towards a solution.
  In a solution, all pods in the room have the correct type."
  [state move]
  (let [target-room (first (:dst move))]
    (->> (pods-in-room state target-room)
         (every? #(= target-room (:type %))))))

(rcf/tests
  (progress-move? {[:a 2] {:type :a}
                   [:a 1] {:type :a}} 
                  {:dst [:a 1]}) 
  := true
  (progress-move? {[:a 2] {:type :a}
                   [:a 1] {:type :b}}
                  {:dst [:a 1]})
  := false)

(defn move-from-hallway
  [state]
  (let [pods-in-hallway (filter in-hallway? (keys state))
        destinations (map (fn [position] 
                            (first-free-position-in-room state (:type (get state position)))) 
                          pods-in-hallway)
        moves (filter (comp some? :dst) (map #(hash-map :src %1 :dst %2) pods-in-hallway destinations))
        without-fail moves ;#_(filter #(progress-move? state %) moves)
        with-costs (map #(assoc % :cost (cost-from-hallway state (:src %) (:dst %))) without-fail)
        valid-moves (filter (comp some? :cost) with-costs)]
    valid-moves))

(defn move-from-room
  [state]
  (let [pods-in-rooms (filter some? (map #(first-pod-position-in-room state %) rooms))
        idle-pods (filter #(not (:moved? (get state %))) pods-in-rooms)
        hall-spots (free-hall-spots state)]
    (for [pod-position idle-pods
          hall-position hall-spots
          :let [c (cost-to-hallway state pod-position hall-position)]
          :when (some? c)]
      {:src pod-position
       :dst hall-position
       :cost c})))

(comment
  (move-from-hallway state)
  (move-from-room state))

(defn move-pod
  [state move]
  (let [pod (get state (:src move))
        pod (assoc pod :moved? true)]
    (-> state
        (assoc (:dst move) pod)
        (dissoc (:src move)))))

(defn find-moves
  "Returns all possible moves from a given state.
  Each move contains the new :state after move and the :total cost.
  This does not return invalid moves."
  ([{:keys [state total]}] (find-moves state total))
  ([state total]
   (let [from-hallway-moves (move-from-hallway state)
         from-room-moves (move-from-room state)]
     (map #(hash-map :state (move-pod state %)
                     :total (+ total (:cost %)))
          (concat from-hallway-moves from-room-moves)))))

(defonce stats (atom nil))
(reset! stats {:pruned 0 :found [] :count 0})
;; Not working, taking too much time to visit all possible paths and prune
(defn run-turn
  [{:keys [state total] :as payload} limit depth]
  (cond
    (> depth 33) (throw (IllegalStateException. "In too deep"))
    (>= total limit) (do (swap! stats update :pruned inc)
                        nil)
    (victory? state) (do (swap! stats update :found conj total)
                         total)
    :else (let [possible-moves (find-moves payload)]
            (swap! stats update :count + (count possible-moves))
            (loop [moves possible-moves
                   limit limit
                   best nil]
              (if-not (seq moves)
                best
                (if-let [solution (run-turn (first moves) (or best limit) (inc depth))]
                  (recur (rest moves) solution solution)
                  (recur (rest moves) limit best)))))))

(defn solve
  [state limit]
  (run-turn {:state state :total 0} limit 0))

;; Using Dijkstra at least

(def inf Long/MAX_VALUE)
(defn dijkstra
  [initial-state]
  (loop [best-score inf
         seen #{}
         costs {}
         candidates (sorted-map 0 [initial-state])]
    (def last* [best-score costs seen candidates])
    (let [[cost [candidate & others]] (first candidates)
          next-candidates (if (seq others)
                            (assoc candidates cost others)
                            (dissoc candidates cost))]
      (cond 
        (nil? candidate) best-score
        (victory? candidate) (recur (min best-score cost)
                                    (conj seen candidate)
                                    (update costs candidate min cost)
                                    (if (< cost best-score)
                                      (->> (keys next-candidates)
                                           (filter #(< cost %))
                                           (reduce dissoc next-candidates))
                                      next-candidates))
        :else (let [moves (find-moves candidate cost)
                    ; remove already seen states
                    moves (filter #(not (seen (:state %))) moves)
                    ; drop those beyond found costs
                    better-moves (filter #(<= (:total %) best-score) moves)
                    ; drop moves greater than before
                    better-moves (filter #(< (:total %) (get costs (:state %) inf)) 
                                         better-moves)
                    updated-costs (reduce #(assoc %1 (:state %2) (:total %2)) 
                                          costs 
                                          better-moves)
                    ; add the good moves to the candidates
                    next-candidates (reduce #(update %1 (:total %2) (fnil conj #{}) (:state %2))
                                            next-candidates
                                            better-moves)]
                (recur best-score
                       (conj seen candidate)
                       updated-costs
                       next-candidates))))))

(defn compare-candidates
  [a b]
  ; a candidate is made of [rating cost state]
  ; only compare rating and cost
  (compare (vec (drop-last a)) (vec (drop-last b))))

(defn pod-heuristic
  [state position pod]
  (cond 
    (= (first position) (:type pod)) 0
    (in-hallway? position) (let [dst [(:type pod) 1]
                                 path (path-from-hallway position dst)]
                             (path-cost state path))
    (in-room? position) (let [exit (get room-entries (first position))
                              exit-path (path-to-hallway position exit)
                              dst [(:type pod) 1]
                              entry-path (path-from-hallway exit dst)]
                          (path-cost state (concat exit-path (rest entry-path))))))

(defn heuristic
  [state]
  (reduce + (map #(pod-heuristic state (first %) (second %)) state)))

(rcf/tests
  (pos? (heuristic (build-state test-input))) := true)

(defn a-star
  [initial-state]
  (let [-queue (java.util.PriorityQueue. 100 compare-candidates)
        _ (.add -queue [(heuristic initial-state) 0 initial-state])]
    (loop [best-score inf
           seen #{}
           costs {}]
      (let [[_ cost candidate] (.poll -queue)]
        ;(def alast* [best-score costs [cost candidate] seen -queue])
        (cond 
          (nil? candidate) best-score ; safe-guard
          (victory? candidate) cost ; We actually reach the end
          (< best-score cost) best-score ; We cannot do better at this point
          :else (let [moves (find-moves candidate cost)
                      ; remove already seen states
                      moves (filter #(not (seen (:state %))) moves)
                      ; drop those beyond found costs
                      better-moves (filter #(<= (:total %) best-score) moves)
                      ; drop moves greater than before
                      better-moves (filter #(< (:total %) (get costs (:state %) inf)) 
                                           better-moves)
                      updated-costs (reduce #(assoc %1 (:state %2) (:total %2)) 
                                            costs 
                                            better-moves)]
                  ; add the good moves to the candidates
                  (doseq [{:keys [state total]} better-moves]
                    (.add -queue [(+ cost (heuristic state)) total state]))
                  (recur best-score
                         (conj seen candidate)
                         updated-costs)))))))

(def input-l1
  {:a [:d]
   :b [:a]
   :c [:b]
   :d [:c]})
(def input-l2
  {:a [:b :a]
   :b [:c :d]
   :c [:b :c]
   :d [:d :a]})

;; my input
;; #############
;; #...........#
;; ###D#A#B#C###
;;   #B#A#D#C#
;;   #########
(def my-input-l2
  {:a [:d :b]
   :b [:a :a]
   :c [:b :d]
   :d [:c :c]})
(def my-input-l4
  {:a [:d :d :d :b]
   :b [:a :c :b :a]
   :c [:b :b :a :d]
   :d [:c :a :c :c]})

(comment
  (solve state 47500)
  (deref stats)
  (binding [*depth* 1] 
    (dijkstra (build-state input-l1))) ; 8446
  (let [[_best costs _seen _candidates] last*]
    (->> costs
         (keys)
         (filter victory?)
         (take 3)))
  (time 
    (binding [*depth* 2]
      (dijkstra (build-state input-l2)))) ; 12521
  (c/profile
    5
    #(binding [*depth* 1] 
       (let [state (build-state input-l1)]
         (time (solve state Long/MAX_VALUE)))))
  
  
  (time
    (binding [*depth* 2]
      (dijkstra (build-state my-input-l2)))) ; 15516
  (time 
    (binding [*depth* 4]
      (dijkstra (build-state my-input-l4)))) ; 47272 in 125s

  (binding [*depth* 1] 
    (a-star (build-state input-l1))) ; 8446
  ;; a-star with heuristic is incorrect
  (second (nth alast* 2))
  (time
    (binding [*depth* 2] 
      (a-star (build-state input-l2)))) ; 12521
  (time
    (binding [*depth* 2] 
      (a-star (build-state my-input-l2)))) ; 15516
  (time
    (binding [*depth* 4] 
      (a-star (build-state my-input-l4)))) ; 47272
  )

