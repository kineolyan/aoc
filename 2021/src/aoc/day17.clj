(ns aoc.day17
  (:require [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

; target area: x=20..30, y=-10..-5
(def test-target
  {:x {:from 20 :to 30}
   :y {:from -10 :to -5}})

; target area: x=287..309, y=-76..-48
(def my-target
  {:x {:from 287 :to 309}
   :y {:from -76 :to -48}})

(defn update-state
  [state]
  (let [{:keys [x y]} (state :position)
        {vx :x vy :y} (state :speed)]
    {:position {:x (+ x vx)
                :y (+ y vy)}
     :speed {:x (if (zero? vx) 0 (dec vx))
             :y (dec vy)}}))

(defn create-trajectory
  [initial-speed]
  (take
    1000 ; safe guard
    (iterate 
      update-state
      {:position {:x 0 :y 0}
       :speed initial-speed})))

(rcf/tests
  (take 10 (create-trajectory {:x 3 :y 2}))
  :=
  [{:position {:x 0, :y 0}, :speed {:x 3, :y 2}}
 {:position {:x 3, :y 2}, :speed {:x 2, :y 1}}
 {:position {:x 5, :y 3}, :speed {:x 1, :y 0}}
 {:position {:x 6, :y 3}, :speed {:x 0, :y -1}}
 {:position {:x 6, :y 2}, :speed {:x 0, :y -2}}
 {:position {:x 6, :y 0}, :speed {:x 0, :y -3}}
 {:position {:x 6, :y -3}, :speed {:x 0, :y -4}}
 {:position {:x 6, :y -7}, :speed {:x 0, :y -5}}
 {:position {:x 6, :y -12}, :speed {:x 0, :y -6}}
 {:position {:x 6, :y -18}, :speed {:x 0, :y -7}}])

(defn in-target?
  [target initial-speed]
  (let [{ax :from bx :to} (target :x)
        trajectory (create-trajectory initial-speed)
        trajectory (map :position trajectory)
        trajectory (take-while #(<= (get % :x) bx) trajectory)
        trajectory (drop-while #(< (get % :x) ax) trajectory)
        {ay :from by :to} (target :y)]
    (boolean (some #(<= ay (get % :y) by) trajectory)))) 

(rcf/tests
  (in-target? test-target {:x 7 :y 2}) := true
  (in-target? test-target {:x 6 :y 3}) := true
  (in-target? test-target {:x 9 :y 0}) := true
  (in-target? test-target {:x 17 :y -4}) := false
  (in-target? test-target {:x 29 :y -2}) := false)

(defn fires
  [target]
  (let [{{ax :from bx :to} :x {ay :from by :to} :y} target
        xs (c/signed-range 1 (inc bx))
        ys (range (dec (- ay)) (dec ay) -1)]
    (for [x xs y ys
                 :let [speed {:x x :y y}]
                 :when (in-target? target speed)]
             speed)))

(comment
  (fires test-target)
  (->> (create-trajectory {:x 29 :y -2})
       (map :position)
       (take 10))
  (in-target? test-target {:x 29 :y -2}))

(defn solve
  [target]
  (count (fires target)))

(rcf/tests
  (solve test-target) := 112)

(comment
  (solve my-target) ; 1117
  )

