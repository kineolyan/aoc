(ns aoc.day8
  (:require [clojure.string :as str]
            [clojure.set :as s]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")

(defn parse-input
  [line]
  (let [words (str/split line #" (:?\| )?")
        words (map set words)]
    {:digits (take 10 words)
     :output (drop 10 words)}))

(def test-data (parse-input test-input))
(rcf/tests
  "parse-input"
  test-data := {:digits (map set ["acedgfb" "cdfbe" "gcdfa" "fbcad" "dab" "cefabd" "cdfgeb" "eafb" "cagedb" "ab"])
                :output (map set ["cdfeb" "fcadb" "cdfeb" "cdbaf"])})

(def digits
  {#{:a :b :c :e :f :g} 0
   #{:c :f} 1
   #{:a :c :d :e :g} 2
   #{:a :c :d :f :g} 3
   #{:b :c :d :f} 4
   #{:a :b :d :f :g} 5
   #{:a :b :d :e :f :g} 6
   #{:a :c :f} 7
   #{:a :b :c :d :e :f :g} 8
   #{:a :b :c :d :f :g} 9})

(comment
  ; digits 1 4 7 8 are truly unique in terms of length
  (->> digits
       (group-by (comp count first))
       (map second)
       (filter #(= 1 (count %)))
       (map first)
       (map second)
       (sort)))

(def uniq-lengths #{2 4 7 3})
(defn uniq?
  [word]
  (contains? uniq-lengths (count word)))

(rcf/tests
  "uniq?"
  (uniq? "af") := true
  (uniq? "gc") := true
  (uniq? "abcefg") := false)

(def test-long-input
  "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce")

(def test-long-data
  (map parse-input (str/split-lines test-long-input)))

(defn solve
  [inputs]
  (->> (map :output inputs)
     (flatten)
     (filter uniq?)
     (count)))

(rcf/tests
  "solve"
  (solve test-long-data) := 26)

(defn find-by-count
  [n digits]
  (first (filter #(= n (count %)) digits)))

(defn find-1
  [digits]
  (find-by-count 2 digits))

(defn find-4
  [digits]
  (find-by-count 4 digits))

(defn find-7
  [digits]
  (find-by-count 3 digits))

(defn find-8
  [digits]
  (find-by-count 7 digits))

(defn find-a
  [{one 1 seven 7}]
  {:post (some? %)}
  (let [d (s/difference seven one)]
    (c/only-first d)))

(defn card-frequencies
  [n digits]
  (let [xs (filter #(= n (count %)) digits)
        fs (->> xs (mapcat seq) (frequencies))
        cs (group-by fs (keys fs))]
    (into {} (map #(vector (first %) (set (second %))) cs))))

(comment 
  (card-frequencies 5 (test-data :digits)))

(defn find-b
  [e3 e4]
  {:post [(some? %)]}
  (c/only-first (s/intersection e3 e4)))

(defn find-d
  [e3 e4]
  {:post [(some? %)]}
  (c/only-first (s/difference e3 e4)))

(defn find-e
  [e3 e4]
  {:post [(some? %)]}
  (c/only-first (s/difference e4 e3)))


(defn not-yet-found
  [xs m]
  {:post [(some? %)]}
  (c/only-first 
    (s/difference xs (set (keys m)))))

(defn match-pins
  [digits]
  (let [one (find-1 digits)
        four (find-4 digits)
        seven (find-7 digits)
        eight (find-8 digits)
        result {(find-a {1 one 7 seven}) :a}
        ; some equations to find the values
        e3 (s/difference four one)
        {e4 1 e5 2 e6 3} (card-frequencies 5 digits)
        {e12 2 e13 3} (card-frequencies 6 digits)
        result (assoc result 
                      (find-b e3 e4) :b
                      (find-d e3 e4) :d
                      (find-e e3 e4) :e)
        result (assoc result (not-yet-found e12 result) :c)
        result (assoc result (not-yet-found e5 result) :f)]
    (assoc result (not-yet-found eight result) :g)))

(rcf/tests
  "find numbers"
  (find-1 (test-data :digits)) := #{\a \b}
  (find-4 (test-data :digits)) := #{\a \b \e \f}
  (find-7 (test-data :digits)) := #{\a \b \d}
  (find-8 (test-data :digits)) := (set "abcdefg")
  (find-a {1 (set "ab") 7 (set "abd")}) := \d
  "match-digits"
  (match-pins (test-data :digits)) 
  := 
  {\d :a
   \e :b
   \f :d
   \g :e
   \a :c
   \b :f
   \c :g})

(def test-pins (match-pins test-data))

(defn output->number
  [pins word]
  (->
    (map pins word)
    (set)
    (digits)))

(rcf/tests
  (map #(output->number test-pins %) (test-data :output))
  := 
  [5 3 5 3])

(defn number
  [digits]
  (reduce #(+ %2 (* 10 %1)) 0 digits))

(defn solve-input
  [{:keys [digits output]}]
  (let [pins (match-pins digits)
        digits (map #(output->number pins %) output)]
    (number digits)))

(defn solve2
  [inputs]
  (reduce
    +
    (map solve-input inputs)))

(rcf/tests
  (solve-input test-data) := 5353
  (solve2 test-long-data) := 61229)

(comment
  (def my-input (c/my-input "day8.txt"))
  (def my-data (map parse-input my-input))
  (take 3 my-data)
  (solve my-data) ; 321
  (solve2 my-data) ; 1028926
  )
