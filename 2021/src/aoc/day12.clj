(ns aoc.day12
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def simple-input
    "start-A
start-b
A-c
A-b
b-d
A-end
b-end")

(defn upper?
  [s]
  (some #(Character/isUpperCase %) s))

(rcf/tests
  (boolean (upper? "abc")) := false
  (boolean (upper? "TEST")) := true)

(defn parse-line
  [line]
  (str/split line #"-"))

(defn add-to-graph
  [graph from to]
  (update graph from (fnil conj []) to))

(rcf/tests
  (add-to-graph {} :a :b) := {:a [:b]}
  (add-to-graph {:a [:b]} :a :c) := {:a [:b :c]})

(defn build-graph
  [lines]
  (reduce
    (fn [g [a b]]
      (-> g
          (add-to-graph a b)
          (add-to-graph b a)))
    {}
    (map parse-line lines)))

(defn input->graph
  [input]
  (->> input
       (str/split-lines)
       (build-graph)))

(def simple-graph (input->graph simple-input))

(defn mark-seen
  [xs x]
  (if (upper? x)
    xs
    (conj xs x)))

(rcf/tests
  (mark-seen #{"start" "e"} "AB") := #{"start" "e"}
  (mark-seen #{"start" "e"} "cd") := #{"start" "e" "cd"})

(defn find-paths
  ([graph] (find-paths graph "start" #{"start"} ["start"]))
  ([graph node seen path]
   (if (= node "end")
     (list path)
     (let [adjacents (get graph node)
           unseen (filter (complement seen) adjacents)]
       (mapcat #(find-paths graph % (mark-seen seen %) (conj path %)) unseen)))))

(defn count-paths
  [graph]
  (count (find-paths graph)))

(rcf/tests
  (count-paths simple-graph) := 10
  (def bigger-graph
    (input->graph "dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc"))
  (count-paths bigger-graph) := 19
  (def large-graph
    (input->graph "fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW"))
  (count-paths large-graph) := 226)

(defn find-paths-bis
  ([graph] 
   (find-paths-bis graph 
                   "start" 
                   {:nodes #{"start"}
                    :double nil} 
                   ["start"]))
  ([graph node seen path]
   (if (= node "end")
     (list path)
     (let [adjacents (get graph node)
           unseen (filter (complement (seen :nodes)) adjacents)
           small (filter #(and (not (upper? %)) (not= % "end")) unseen)]
       (concat 
         ; continue as part 1
         (mapcat #(find-paths-bis graph 
                                           % 
                                           (update seen :nodes mark-seen %) 
                                           (conj path %)) 
                 unseen)
         ; chose one small cave to be seen twice
         (when-not (and (seq small) (seen :double))
           (mapcat #(find-paths-bis graph 
                                             % 
                                             (assoc seen :double %) 
                                             (conj path %)) 
                   small)))))))


(map #(str/join "," %) (find-paths-bis simple-graph))

(defn solve2
  [graph]
  (-> graph find-paths-bis set count))

(rcf/tests
  (solve2 simple-graph) := 36)

(comment
  (def my-input (str/split-lines "pn-TY
rp-ka
az-aw
al-IV
pn-co
end-rp
aw-TY
rp-pn
al-rp
end-al
IV-co
end-TM
co-TY
TY-ka
aw-pn
aw-IV
pn-IV
IV-ka
TM-rp
aw-PD
start-IV
start-co
start-pn"))
  (def my-graph (build-graph my-input))
  (->> (keys my-graph)
      (filter (complement upper?))
      (count))
  (->> (keys my-graph)
      (filter upper?)
      (count))
  ; 9 small caves, for 4 + 9 caves, enough for recursion
  (count-paths my-graph)
  (solve2 my-graph))
