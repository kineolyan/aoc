(ns aoc.day5
  (:require [clojure.string :as str]
            [clojure.java.math :as math]
            [aoc.common :as c]))

(def test-input
  (str/split-lines
    "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"))

(defn parse-line
  [line]
  (if-let [match (re-find #"(\d+),(\d+) -> (\d+),(\d+)" line)]
    (let [[x1 y1 x2 y2] (map parse-long (drop 1 match))]
      {:start {:x x1 :y y1}
       :end {:x x2 :y y2}})
    (throw (IllegalStateException. (str "Cannot parse " line)))))

(comment 
  (parse-line (first test-input)))

(defn parse-input
  [lines]
  (map parse-line lines))

(def t (parse-input test-input))

(defn same-coord
  [point k]
  (= (get-in point [:start k]) (get-in point [:end k])))

(comment
  [(same-coord (first t) :x)
   (same-coord (first t) :y)])

(defn filter-input
  [lines]
  (filter 
    #(or (same-coord % :x) (same-coord % :y))
    lines))

(def vt (filter-input t))

(defn diagonal?
  [cloud]
  (= (math/abs (- (get-in cloud [:start :x]) (get-in cloud [:end :x])))
     (math/abs (- (get-in cloud [:start :y]) (get-in cloud [:end :y])))))

(comment 
  (diagonal? {:start {:x 5 :y 9}
              :end {:x 9 :y 5}})
  (diagonal? {:start {:x 3 :y 4}
              :end {:x 0 :y 7}})
  (diagonal? {:start {:x 1 :y 1}
              :end {:x 1 :y 4}}))

(defn filter-input2
  [clouds]
  (filter 
    #(or (same-coord % :x) 
         (same-coord % :y)
         (diagonal? %))
    clouds))

(def vt2 (filter-input2 t))


(defn point->seq
  [cloud k]
  (let [c1 (get-in cloud [:start k])
        c2 (get-in cloud [:end k])
        start (min c1 c2)
        end (inc (max c1 c2))]
    (range start end)))

(defn linear-cloud-seq
  [{:keys [xs ys]}]
  (for [x xs y ys]
    [x y]))

(defn diagonal-cloud-seq
  [cloud]
  (let [xs (c/signed-range (-> cloud :start :x) 
                           (-> cloud :end :x)
                           :inclusive true)
        ys (c/signed-range (-> cloud :start :y)
                           (-> cloud :end :y)
                           :inclusive true)]
    (map vector xs ys)))

(comment
  (diagonal-cloud-seq {:start {:x 3 :y 3}
                       :end {:x 5 :y 5}})
  (diagonal-cloud-seq {:start {:x 5 :y 0}
                       :end {:x 1 :y 4}}))

(defn cloud->seq
  [cloud]
   (cond
     (same-coord cloud :x) (linear-cloud-seq {:xs [(get-in cloud [:start :x])]
                                              :ys (point->seq cloud :y)})
     (same-coord cloud :y) (linear-cloud-seq {:xs (point->seq cloud :x)
                                             :ys [(get-in cloud [:start :y])]})
     (diagonal? cloud) (diagonal-cloud-seq cloud)
     :else (throw (IllegalArgumentException. (str "Bad cloud " cloud)))))

(comment 
  (cloud->seq (first vt)))

(defn map-clouds
  [clouds]
  (reduce
    #(update %1 %2 (fnil inc 0))
    {}
    (mapcat cloud->seq clouds)))

(def test-map (map-clouds vt))

(defn count-cross
  [m]
  (count (filter #(> (second %) 1) m)))

(defn solve
  [input]
  (let [all-clouds (parse-input input)
        clouds (filter-input all-clouds)
        m (map-clouds clouds)]
    (count-cross m)))

(defn solve2
  [input]
  (let [all-clouds (parse-input input)
        clouds (filter-input2 all-clouds)
        m (map-clouds clouds)]
    (count-cross m)))

(comment
  (def mine (c/my-input "day5.txt"))
  ; part 1
  (solve test-input) ; 5
  (solve mine) ; 4655 
  ; part 2
  (solve2 test-input) ; 12
  (solve2 mine) ; 20500
  )
