(ns aoc.day11
  (:require [clojure.string :as str]
            [clojure.set :as s]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  (str/split-lines
    "5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526"))

(defn parse-line
  [line]
  (mapv (comp parse-long str) line))

(def test-grid (mapv parse-line test-input))

(defn get-neighbour-coords
  [grid coord]
  (c/get-neighbour-coords c/grid-shifts c/map-point grid coord))

(defn increase-levels
  [grid]
  (mapv #(mapv inc %) grid))

(comment
  (increase-levels test-grid))

(defn find-coords
  [f grid]
  (let [{:keys [w h]} (c/get-dims grid)]
    (for [x (range w)
          y (range h)
          :let [p {:x x :y y}]
          :when (f p)]
      p)))

(defn inc-octopus
  [v]
  (if (zero? v) 0 (inc v)))

(defn trigger-flashes
  [grid]
  (let [charged (find-coords #(< 9 (c/get-point grid %)) grid)
        ready (filter #(pos? (c/get-point grid %)) charged)]
    (if (seq ready)
      (let [grid-after-flash (reduce #(c/update-point %1 %2 (constantly 0)) grid ready)
            ; TODO do we need to trigger twice an octopus if many nb flashed
            neighbours (mapcat #(get-neighbour-coords grid %) ready)
            neighbours-not-flashed (filter #(pos? (c/get-point grid-after-flash %)) neighbours)
            inc-grid (reduce #(c/update-point %1 %2 inc) grid-after-flash neighbours-not-flashed)]
        (recur inc-grid))
      grid)))

(comment
  (trigger-flashes (increase-levels test-grid))
  (trigger-flashes (increase-levels *1)))

(defn one-turn
  [grid]
  (let [inc-grid (increase-levels grid)]
    (trigger-flashes inc-grid)))

(defn count-flashes
  [grid]
  (->> (flatten grid)
       (filter zero?)
       (count)))

(rcf/tests
  (count-flashes [[0 1]
                  [2 0]])
  :=
  2)

(defn solve1
  [grid]
  (second
    (reduce
      (fn [[g n] _]
        (let [next-grid (one-turn g)]
          [next-grid (+ n (count-flashes next-grid))]))
      [grid 0]
      (range 100))))

(defn solve2
  [grid]
  (loop [grid (one-turn grid)
         n 1]
    (if (every? zero? (flatten grid))
      n
      (recur (one-turn grid) (inc n)))))

(rcf/tests
  (solve1 test-grid) := 1656
  (solve2 test-grid) := 195)

(comment
  (def my-input (str/split-lines "3265255276
1537412665
7335746422
6426325658
3854434364
8717377486
4522286326
6337772845
8824387665
6351586484"))
  (def my-grid (mapv parse-line my-input))
  (solve1 my-grid)
  (solve2 my-grid))

