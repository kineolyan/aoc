(ns aoc.day6
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  [3,4,3,1,2])

(defn build-state
  [input]
  (frequencies input))

(def test-init-state (build-state test-input))
;; {3 2, 4 1, 1 1, 2 1}

(defn flip-days
  [state]
  {:pre [(every? pos? (keys state))]}
  (->> state
       (map (fn [[k v]] (vector (dec k) v)))
       (into {})))

(rcf/tests
  "flip"
  (flip-days test-init-state)
  :=
  {2 2, 3 1, 0 1, 1 1})

(defn tick
  [state]
  (let [creators (get state 0)
        next-state (flip-days (dissoc state 0))]
    (if creators
      (-> next-state
          (assoc 8 creators)
          (update 6 (fnil + 0) creators))
      next-state)))

(rcf/tests
  "tick"
  (tick (build-state [2 3 2 0 1]))
  :=
  (build-state [1 2 1 6 0 8])
  (tick (build-state [1 2 7 0]))
  :=
  (build-state [0 1 6 6 8])
  (tick (build-state [1 2]))
  :=
  (build-state [0 1]))

(defn count-fishes
  [state]
  (reduce + (vals state)))

(rcf/tests
  "count"
  (count-fishes test-init-state) := 5
  (count-fishes (->> test-init-state 
                     (iterate tick)
                     (drop 2)
                     (first)))
  := 6)

(defn run
  [init-state n]
  (reduce
    (fn [r _] (tick r))
    init-state
    (range n)))

(rcf/tests
  "running"
  (count-fishes (run test-init-state 18)) := 26)

(defn solve
  [input n]
  (-> (build-state input)
      (run n)
      (count-fishes)))

(comment
  (->> test-init-state
       (iterate tick)
       (take 2))
  
  (build-state 
    [6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8]))

(defn parse-input
  [lines]
  (as-> (first lines) v
    (str/split v #",")
    (map parse-long v)))


(rcf/tests
  "test inputs"
  (solve test-input 80) := 5934
  (solve test-input 256) := 26984457539)
(comment
  (def mine (c/my-input "day6.txt"))
  ; part 1
  (solve (parse-input mine) 80) ; 352872
  ; part 2 
  (solve (parse-input mine) 256))
