(ns aoc.common
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [clj-async-profiler.core :as prof]))

(def home (System/getProperty "user.home"))

(def -std-loc (str home "/tmp/aoc"))

(defn my-input
  [f & {:keys [lines] :or {lines true}}]
  (let [f (str -std-loc "/" f)
        content (slurp f)]
    (if lines
      (str/split-lines content)
      content)))

(comment
  (take 5 (my-input "day3.txt"))
  (take 20 (my-input "day3.txt" :lines false)))

(defn only-first
  "Returns the single element of the collection or throws if there are more than one."
  [xs]
  {:pre [(seq xs)]}
  (let [[x y] (take 2 xs)]
    (if (nil? y)
      x
      (throw (IllegalStateException. (str "More than one result: " (doall xs)))))))
  
(defn signed-range
  [start end & {:keys [inclusive] :or {inclusive false}}]
  (if (< start end)
    (range start (if inclusive (inc end) end))
    (range start (if inclusive (dec end) end) -1)))

(comment
  (signed-range 0 5)
  (signed-range 0 5 :inclusive true)
  (signed-range 5 0)
  (signed-range 5 0 :inclusive true))

(rcf/tests
  "ascending"
  (signed-range 0 3) := [0 1 2]
  (signed-range 2 4 :inclusive true) := [2 3 4]
  (signed-range -1 2) := [-1 0 1]
  "descending"
  (signed-range 0 -2) := [0 -1]
  (signed-range 1 -3 :inclusive true) := [1 0 -1 -2 -3])

(defn parse-array
  "Parse a single line representing a comma-separated array of values"
  [f line]
  (as-> line v
    (str/split v #",")
    (map f v)))

(rcf/tests
  "array of long"
  (parse-array parse-long "1,2,4") := [1 2 4])

;;; Grid functions

(defn parse-grid
  "Parses a seq of strings representing a grid, one digit being one cell in the grid"
  [lines]
  (mapv #(mapv (comp parse-long str) %) lines))

(defn get-point
  ([grid x y]
   (-> grid (nth y) (nth x)))
  ([grid coord]
   (get-point grid (coord :x) (coord :y))))

(defn update-point
  ([grid {:keys [x y]} f]
   (mapv 
     (fn [gy row]
       (if (= y gy)
         (mapv 
           (fn [gx v]
             (if (= x gx)
               (f v)
               v))
           (range)
           row)
         row))
     (range)
     grid)))

(rcf/tests
  (update-point [[1 2] [3 4]] {:x 1 :y 0} #(* 10 %))
  :=
  [[1 20] [3 4]])

(defn get-dims
  [grid]
  {:h (count grid) :w (-> grid first count)})

(def cardinal-shifts
  [[0 -1]
   [0 1]
   [-1 0]
   [1 0]])
(def grid-shifts
  (for [x (range -1 2)
        y (range -1 2)
        :when (not (and (zero? x) (zero? y)))]
    [x y]))

(defn pair-point
  [x y]
  [x y])

(defn map-point
  [x y]
  {:x x :y y})

(defn get-neighbour-coords
  [shifts make-point grid {:keys [x y]}]
  (let [{:keys [w h]} (get-dims grid)]
    (for [[dx dy] shifts
          :let [nx (+ x dx)
                ny (+ y dy)]
          :when (<= 0 nx)
          :when (<= 0 ny)
          :when (< nx w)
          :when (< ny h)]
      (make-point nx ny))))

(rcf/tests
  "neighbours"
  (def test-grid (vec (repeatedly 10 #(vec (range 10)))))
  (get-neighbour-coords cardinal-shifts pair-point test-grid {:x 0 :y 0}) := [[0 1] [1 0]]
  (get-neighbour-coords cardinal-shifts pair-point test-grid {:x 1 :y 3}) := [[1 2] [1 4] [0 3] [2 3]]
  (get-neighbour-coords cardinal-shifts pair-point test-grid {:x 9 :y 2}) := [[9 1] [9 3] [8 2]])

(defonce prof-srv-running (atom false))

(defn profile
  "Profile some code"
  ([f] (profile 1 f))
  ([n f] 
   (when (compare-and-set! prof-srv-running false true)
     (prof/serve-files 8080))
   (prof/profile (dotimes [_ n] (f)))))

(defn invert-map
  [m]
  (let [ks (keys m)
        vs (map #(get m %) ks)]
  (zipmap vs ks)))

(rcf/tests
  (invert-map {:a 1 :b 2}) := {1 :a 2 :b})

(defn todo
  []
  (throw (UnsupportedOperationException. "TODO")))

