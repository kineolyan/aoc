(ns aoc.day24
  (:require [clojure.string :as str]
            [clojure.core.match :as m]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  (str/split-lines
    "inp z
inp x
mul z 3
eql z x"))

(def vars {"x" :x "y" :y "z" :z "w" :w})
(defn ->val
  [x]
  (if-let [v (get vars x)]
    v
    (parse-long x)))

(defn parse-line
  [line]
  (let [[op & args]  (str/split line #"\s")]
    (vec (concat 
           [(keyword op)] 
           (map ->val args)))))

(rcf/tests
  (parse-line "add z w") := [:add :z :w]
  (parse-line "inp x") := [:inp :x]
  (parse-line "mul y 42") := [:mul :y 42])

(def test-data
  (mapv parse-line test-input))

(defn run-step
  [state {:keys [D M N]}]
  (let [w (first (:inputs state))
        z (:z state)
        _ (tap> [(- 14 (count (:inputs state))) (mod z 26)])
        a (+ (mod z 26) N)
        z (int (/ z D))]
    
      {:inputs (-> state :inputs rest)
       :z (if (= a w) 
            z 
            (+ (* 26 z) M w))}))

  (defn debug-alu
    ([program inputs]
     (let [init-state {:inputs inputs :z 0}]
       (map :z (reductions #(run-step %1 %2) init-state program))))
    ([program inputs n]
     (take (inc n) (debug-alu program inputs))))

  (defn run-alu
    [program inputs]
    (last (debug-alu program inputs)))

  (defn valid?
    [program number]
    (let [result (run-alu program number)]
      (zero? (:z result))))

  (defn cart [colls]
    "Compute the cartesian product of list of lists"
    (if (empty? colls)
      '(())
      (for [more (cart (rest colls))
            x (first colls)]
        (cons x more))))

  (def my-input (c/my-input "day24.txt"))
  (def my-data (map parse-line my-input))

  (comment
    (count my-input)
    (partition (/ 252 14) my-data)
    ;; for every group, we do the same ops
    (apply = (map #(map first %) (partition (/ 252 14) my-data)))
    ;; for every group, we do the same ops on the same inputs
    (apply = (map #(map (partial take 2) %) (partition (/ 252 14) my-data)))
    ;; are only values changing? yes, there are only diffs in num values
    (apply = (partition 
               (/ 252 14) 
               (map #(if (number? (last %)) (assoc % 2 :number) %)
                    my-data))))

  (def groups (partition (/ 252 14) my-data))

  (comment
    ; #1 all do [:mul :x 0]
    (apply = (map #(nth % 1) groups))
    ; #3 all do [:mod :x 26]
    (apply = (map #(nth % 3) groups))
    ; #4 either [:div :z (1|26)]
    (apply = (map #(nth % 4) groups))
    ; #5 variable [:add :x n]
    (apply = (map #(nth % 5) groups))
    ; #7 all [:eql :x 0]
    (apply = (map #(nth % 7) groups))
    ; #8 and #9 all [:mul :y 0] then [:add :y 25]
    (apply = (map #(nth % 8) groups))
    (apply = (map #(nth % 9) groups))
    ; #11 all [:add :y 1]
    (apply = (map #(nth % 11) groups))
    ; #13 all [:mul :y 0]
    (apply = (map #(nth % 13) groups))
    ; #15 variable [:add :y m]
    (apply = (map #(nth % 15) groups))

    ;; no need to simplify
    #_(def my-data (->> my-data 
                      (simplify-program)
                      (optimize)
                      (doall)))

  )

  (defn extract-params
    [data]
    (let [D (last (nth data 4))
          N (last (nth data 5))
          M (last (nth data 15))]
      {:D D :N N :M M}))

  (def smaller-program (map extract-params groups))

  (defn print-program
    [program]
    (with-out-str
      (doseq [{:keys [D N M]} program] 
        (println "w = read()")
        (printf "A = (z %% 26) + %d%n" N)
        (printf "z = z / %d%n" D)
        (printf "if A <> w: z = 26 * z + (%d + w)%n" M)
        (println "-------------------"))))

  (comment
    (println (print-program smaller-program))
    (doseq [v [[9 9 9 9 9 9 9 9 9 9 9 9 9 9 ]
               [ 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
               '(1 2 3 4 5 6 7 8 9 9 9 9 9 9)
               '(9 8 7 6 5 4 3 2 1 1 1 1 1 1)]]
      (tap> (debug-alu smaller-program v)))
    (debug-alu 
      smaller-program 
      [9 9 7 9 9 9 6, 7 ]
      9)
    (run-alu smaller-program [5 2 9 2 6
                              9 9 5 9 7
                              1 9 9 9]) ; 0 as expected
    (debug-alu smaller-program [1 1 8 1 1
                                9 5 1 3 1
                                1 4 8 5]))

;; How to solve it
;; Reverse engineer the program :)
;; Whenever there is A = z % 26 + N, N is greater than 9 so A is always different from w
;; And in this case, we always divide z by 1
;; On the contrary, A = z % 26 + -N and z is always divided by 26
;; We must also note that we update z as 26 * z + (M + w)
;; and we conversely do (z % 26).
;; So we must compare M(i) + w(i) with w(j) + N(j)
;; They all nicely pile so we have the following equations
;; 15 + w( 1) = 11 + w(14)
;;  8 + w( 2) =  1 + w(13)
;;  2 + w( 3) =  9 + w( 4)
;; 13 + w( 5) = 10 + w(12)
;;  4 + w( 6) = 12 + w(11)
;;  1 + w( 7) =  5 + w( 8)
;;  5 + w( 9) =  7 + w(10)
;; We can simplify and solve it, using the max w values

(str 5 2 9 2 6
     9 9 5 9 7
     1 9 9 9 )

(str 1 1 8 1 1
     9 5 1 3 1
     1 4 8 5)



