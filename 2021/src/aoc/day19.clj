(ns aoc.day19
  (:require [clojure.string :as str]
            [clojure.set :as s]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input (c/my-input "test19.txt"))

;;; parsing

(defn group-scanner-inputs
  [inputs]
  (loop [inputs inputs
         groups []]
    (let [[group more] (split-with (complement str/blank?) inputs)]
      (if (seq group)
        (recur (rest more) (conj groups group))
        groups))))

(defn parse-coordinates
  [line]
  (zipmap
    [:x :y :z]
    (map parse-long (str/split line #","))))

(defn parse-group
  [inputs]
  (let [[_ id] (re-find #"--- scanner (\d+) ---" (first inputs))
        id (parse-long id)]
    (hash-map id (mapv parse-coordinates (rest inputs)))))

(defn parse
  [inputs]
  (->> inputs
       group-scanner-inputs
       (map parse-group)
       (reduce merge {})))

(def test-data (parse test-input))

;;; Find the correct transformation

(def coordinates [:x :y :z])

(defn point-diff
  "Computes the difference `pb - pa`"
  ([data grp ia ib]
   (point-diff
     (get-in data [grp ib])
     (get-in data [grp ia])))
  ([pa pb]
   (reduce
    #(assoc %1 %2 (- (%2 pb) (%2 pa)))
    {}
    coordinates)))

(defn diff-candidate?
  [{dx :x dy :y dz :z}]
  (and (not= dx dy)
       (not= dy dz)
       (not= dz dx)))

(defn rotate
  [matrix point]
  (let [p (map point coordinates)]
    (zipmap 
      coordinates
      (map
        #(reduce + (map * % p))
        matrix))))

(rcf/tests
  (rotate
    [[0 1 0]
     [0 0 1]
     [-1 0 0]]
    {:x 2 :y 3 :z 4})
  :=
  {:x 3 :y 4 :z -2})

(def dpa [1 -2 3])
(def dpb [2 1 -3])
(def base-row [0 0 0])
(defn index-of
  [xs x]
  (ffirst
    (filter #(= x (second %))
            (map vector (range) xs))))

(defn create-matrix-row
  [dpb v]
  (let [x (index-of dpb v)
        x' (index-of dpb (- v))]
    (if x 
      (update-in base-row [x] (constantly 1))
      (update-in base-row [x'] (constantly -1)))))

(defn create-matrix
  [dpa dpb]
  (mapv #(create-matrix-row dpb %) dpa))

(rcf/tests
  (create-matrix dpa dpb) := [[0 1 0] [-1 0 0] [0 0 -1]])

(defn point-sum
  [pa pb]
  (reduce
    #(assoc %1 %2 (+ (%2 pa) (%2 pb)))
    {}
    coordinates))

(rcf/tests
  (point-sum {:x 1 :y 2 :z 3} {:x 10 :y 20 :z 30}) := {:x 11 :y 22 :z 33})

(defn transform-point
  [{:keys [rotation translation]} point]
  (point-sum translation 
             (rotate rotation point)))

(defn valid-transformation-for?
  "Tests if the transformation is valid for a pair of points."
  [transformation src dst]
  (= (transform-point transformation dst) src))

(defn find-transformation
  [pa1 pa2 pb1 pb2]
  (let [dpa (point-diff pa1 pa2)
        dpb (point-diff pb1 pb2)
        matrix (create-matrix (map (partial get dpa) coordinates) 
                              (map (partial get dpb) coordinates))
        translation (point-diff (rotate matrix pb1) pa1)]
    [matrix translation]))

(rcf/tests
  (def mt (find-transformation
            (get-in test-data [1 1])
            (get-in test-data [1 0])
            (get-in test-data [0 4])
            (get-in test-data [0 9])))
  "points after transformation"
  (point-sum (second mt) (rotate (first mt) (get-in test-data [0 4])))
  :=
  (get-in test-data [1 1])
  (point-sum (second mt) (rotate (first mt) (get-in test-data [0 9])))
  :=
  (get-in test-data [1 0]))

(defn find-zone-transformation
  [data [[a b] mapping]]
  (let [raw-results (for [i1 (keys mapping)
                          i2 (keys mapping)
                          :when (not= i1 i2)
                          :let [pa1 (get-in data [a i1])
                                pa2 (get-in data [a i2])]
                          :when (diff-candidate? (point-diff pa1 pa2))]
                      (find-transformation
                        pa1 
                        pa2 
                        (get-in data [b (get mapping i1)])
                        (get-in data [b (get mapping i2)])))
        transformations (map #(hash-map :rotation (first %) :translation (second %)) 
                             raw-results)
        valid-txs (filter
                    (fn [tx] 
                      (every? #(valid-transformation-for? tx (first %) (second %)) mapping))
                    transformations)]))

;;; zone matching

(defn distance
  [pa pb]
  (->> coordinates
       (map #(- (pa %) (pb %)))
       (map #(* % %))
       (reduce +)))

(rcf/tests
  (distance {:x 1 :y 0 :z 0} {:x 2 :y -10 :z 0}) := 101)

(defn group-distances
  "Computes all distances between every point of a given group.
  The results is returned as a map of #{pa pb} -> distance"
  ([points]
   (group-distances points (range (count points))))
  ([points indexes]
   (reduce
     #(update %1 (second %2) (fnil conj #{}) (first %2))
     {}
     (for [a indexes b indexes :when (> a b)]
       [(hash-set a b) (distance (nth points a) (nth points b))]))))

(defn index-by-distance
  [data]
  (into {} (map #(vector (first %) (group-distances (second %))) data)))

(defn all-points
  [group distances]
  (reduce
    s/union
    (mapcat #(get group %) distances)))

(defn inner
  [ga gb]
  (let [i (s/intersection (set (keys ga)) (set (keys gb)))
        a-points (all-points ga i)
        b-points (all-points gb i)]
    [a-points b-points]))

(defn find-candidates
  [groups]
  (for [x (keys groups) y (keys groups) :when (> x y)]
    (zipmap [x y] (inner (get groups x) (get groups y)))))

(defn overlapping-candidates
  [xs]
  (filter #(>= (-> % first second count) 12) xs))

(defn find-pairs
  "Finds candidate pairs of groups that shares at least 12 points in common.
  The matching is made solely on the distance between points."
  [groups]
  (let [group-by-distances (index-by-distance groups)
        candidates (find-candidates group-by-distances)]
    (overlapping-candidates candidates)))

(rcf/tests
  (->> (find-pairs test-data) (map keys) set) := #{[1 0] [3 1] [4 1] [4 2]})

(defn pair-by-distances
  [data pair ix iy]
  (let [->ds #(group-distances (get data %) (get pair %))
        xds (->ds ix)
        yds (->ds iy)
        ->set #(reduce s/union (get %1 %2))]
    (->> (keys xds)
         (map #(vector % (->set xds %) (->set yds %)))
         (filter #(and (seq (second %))
                       (seq (last %)))))))

;; this is giving series of entries like: for D #{x1 x2} #{y1 y2}
;; after that, we have to find if x1 = y1 or x1 = y2
(defn pair-points-by-distance
  [dss]
  (loop [es dss
         candidates {}]
    (if-let [[_ pas pbs] (first es)]
      (recur
        (rest es)
        (reduce
          (fn [res pa] (update res pa #(if % (s/intersection % pbs) pbs)))
          candidates
          pas))
      (into {} (filter (comp pos? count second) candidates)))))

(defn match-points-of-pairs
  [data a b dss]
    (let [candidates (pair-points-by-distance dss)
          paired-mapping (into {} (filter #(= 1 (count (second %))) candidates))
          unknown (filter #(< 1 (count (second %))) candidates)
          transformation (find-zone-transformation data [[a b] paired-mapping])]
      (cond
        ; no valid transfo for all paired points
        (nil? transformation) nil
        ; not enough candidates
        (> 12 (+ (count paired-mapping) (count unknown)))
        nil
        :else
        :todo
        )))

(defn match-all-cds
  [data cds]
  (->> cds
       (map
         (fn [cd] 
           (let [[a b] (sort (keys cd))
                 pairs (pair-by-distances data cd a b)
                 matches (match-points-of-pairs data a b pairs)]
             [[a b] matches]))
         )
       (filter (comp some? second))))

(defn find-all-transformations
  [data matches]
  (concat
    (map #(find-zone-transformation data %) matches)
    (map #(find-zone-transformation data %)
         (map #(vector (reverse (first %)) (c/invert-map (second %))) 
              matches))))

;;; Visit the various zones, counting the points in each 
(defn find-zone-connections
  [pairs]
  (reduce
    (fn [res [a b]] 
      (-> res 
          (update a (fnil conj #{}) b)
          (update b (fnil conj #{}) a)))
    {}
    pairs))

(defn get-transformation
  [transformations a b]
  (first
    (filter #(= [a b] (:zones %)) transformations)))

(defn count-unseen
  [data transformations zone adjacent-zones]
  (count
    (reduce
      (fn [points adjacent-zone]
        (let [tx (get-transformation transformations adjacent-zone zone)]
          (tap> [zone adjacent-zone tx])
          (filter
            (fn [point] 
              (let [transformed-point (transform-point tx point)]
                (not-any? 
                  #(= transformed-point %) 
                  (get data adjacent-zone))))
            points)))
      (get data zone)
      adjacent-zones)))

(defn complete1
  [data transformations]
  (let [connections (find-zone-connections (map :zones transformations))]
    (loop [c 0
           seen #{}
           stack [0]]
      (if-let [zone (first stack)]
        (let [adjacent-zones (get connections zone)
              seen-zones (filter seen adjacent-zones)
              unseen-zones (filter (complement seen) adjacent-zones)]
          (recur (+ c (count-unseen data transformations zone seen-zones))
                 (conj seen zone)
                 (concat (rest stack) unseen-zones)))
        c))))

(defn solve1
  [data]
  (let [cds (find-pairs data)
        matches (match-all-cds data cds)
        transformations (find-all-transformations data matches)]
    (complete1 data transformations)))

(comment
  (def my-data (parse (c/my-input "day19.txt")))
  ; nb of points
  (-> my-data first second count)
  (def my-cds (find-pairs my-data))
  (def result-matches (match-all-cds my-data my-cds))
  (map second result-matches)
  ; This is a special case where one point has no match
  ; and another has two candidates by distances
  (match-points-of-pairs
    my-data
    38
    1
    (pair-by-distances my-data 
                      (first (filter #(= #{38 1} (set (keys %))) my-cds)) 
                      38 1))

  ; now we have all transformations from one zone to another
  ; as [min max], matrix + translation from max to min
  (def transformations (find-all-transformations my-data result-matches))
  
  (complete1 my-data transformations)

  (rcf/tests
    (solve1 test-data) := 79)
  )

