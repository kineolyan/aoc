(ns aoc.day9
  (:require [clojure.string :as str]
            [hyperfiddle.rcf :as rcf]
            [aoc.common :as c]))

(def test-input
  (str/split-lines "2199943210
3987894921
9856789892
8767896789
9899965678"))

(defn parse-input
  [lines]
  (mapv #(mapv (comp parse-long str) %) lines))

(def test-grid (parse-input test-input))

(defn get-neighbour-coords
  [grid coord]
  (c/get-neighbourd-coords c/cardinal-shifts c/pair-point grid coord))

(rcf/tests
  "neighbours"
  (get-neighbour-coords test-grid {:x 0 :y 0}) := [[0 1] [1 0]]
  (get-neighbour-coords test-grid {:x 1 :y 3}) := [[1 2] [1 4] [0 3] [2 3]]
  (get-neighbour-coords test-grid {:x 9 :y 2}) := [[9 1] [9 3] [8 2]])


(defn get-neighbour-values
  [grid coord]
  (map 
    (fn [[x y]] (c/get-point grid x y))
    (get-neighbour-coords grid coord)))

(defn local-min?
  [grid x y]
  (let [v (c/get-point grid x y)
        around (get-neighbour-values grid {:x x :y y})]
    (every? #(< v %) around)))

(rcf/tests
  (local-min? test-grid 1 0) := true
  (local-min? test-grid 0 0) := false)

(defn solve1
  [grid]
  (let [{:keys [w h]} (c/get-dims grid)
        coords (for [x (range w) y (range h)] {:x x :y y})
        mins (filter #(local-min? grid (:x %) (:y %)) coords)
        vs (map #(c/get-point grid (:x %) (:y %)) mins)]
    (reduce + (map inc vs))))

(rcf/tests
  (c/get-dims test-grid) := {:h 5 :w 10}
  (solve1 test-grid)) := 15

(defn build-pool-size
  [grid coord]
  (loop [size 0
         ps [coord]
         seen #{}]
    (if-let [p (first ps)]
      (if (seen p) 
        (recur size (rest ps) seen)
        (let [v (c/get-point grid p)
              around (for [[x y] (get-neighbour-coords grid p)
                           :let [np {:x x :y y}]
                           :when (< v (c/get-point grid np) 9)
                           :when (not (seen np))]
                       np)]
          (recur (inc size)
                 (concat (rest ps) around)
                 (conj seen p))))
      size)))

(rcf/tests
  (build-pool-size test-grid {:x 1 :y 0}) := 3
  (build-pool-size test-grid {:x 9 :y 0}) := 9
  (build-pool-size test-grid {:x 2 :y 2}) := 14
  (build-pool-size test-grid {:x 6 :y 4}) := 9)

(defn solve2
  [grid]
  (let [{:keys [w h]} (c/get-dims grid)
        coords (for [x (range w) y (range h)] {:x x :y y})
        mins (filter #(local-min? grid (:x %) (:y %)) coords)
        areas (map #(build-pool-size grid %) mins)]
    (->> areas 
        sort 
        reverse
        (take 3)
        (reduce *))))

(rcf/tests
  (solve2 test-grid) := 1134)

(comment
  (def my-input (c/my-input "day9.txt"))
  (def my-grid (parse-input my-input))
  (c/get-dims my-grid)
  (solve1 my-grid) ; 580
  (solve2 my-grid))
