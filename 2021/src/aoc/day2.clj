(ns aoc.day2
  (:require [clojure.string :as str]))

(defn parse-command
  [value]
  (let [[cmd n] (str/split value #" ")]
    [cmd (parse-long n)]))

(comment
  (parse-command "forward 5"))

(defn parse
  [f]
  (let [content (slurp f)
        rows (str/split-lines content)]
    (map parse-command rows)))

(defn dispatch-command
  [_position command] 
  (first command))

(defmulti move #'dispatch-command)
(comment
  ; Call this if the method changes
  (ns-unmap *ns* 'move))

(defmethod move "forward"
  [position [_ n]]
  (update position :position + n))

(defmethod move "down"
  [position [_ n]]
  (update position :depth + n))

(defmethod move "up"
  [position [_ n]]
  (update position :depth - n))

(comment
  (def position {:position 1 :depth 2})
  (move position ["forward" 5])
  (move position ["down" 7])
  (move position ["up" 1]))

(defn solve
  [commands]
  (let [initial-position {:position 0 :depth 0}
        final-position (reduce move initial-position commands)]
    (tap> final-position)
    (* (final-position :position) (final-position :depth))))

(defmulti move2 #'dispatch-command)
(comment
  ; Call this if the method changes
  (ns-unmap *ns* 'move2))

(defmethod move2 "forward"
  [position [_ n]]
  (-> position
      (update :position + n)
      (update :depth + (* n (position :aim)))))

(defmethod move2 "down"
  [position [_ n]]
  (update position :aim + n))

(defmethod move2 "up"
  [position [_ n]]
  (update position :aim - n))

(comment
  (def position {:position 10 :depth 20 :aim 3})
  (move2 position ["forward" 5])
  (move2 position ["down" 7])
  (move2 position ["up" 1]))

(defn solve2
  [commands]
  (let [initial-position {:position 0 :depth 0 :aim 0}
        final-position (reduce move2 initial-position commands)]
    (tap> final-position)
    (* (final-position :position) (final-position :depth))))

(comment
  (def test-input ["forward 5"
                   "down 5"
                   "forward 8"
                   "up 3"
                   "down 8"
                   "forward 2"])
  (solve (map parse-command test-input))
  (solve (parse "/home/oliv/tmp/aoc/day2.txt"))
  ; part 2
  (solve2 (map parse-command test-input))
  (solve2 (parse "/home/oliv/tmp/aoc/day2.txt"))) 
