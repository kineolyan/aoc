(ns aoc.day3
  (:require [clojure.string :as str]
            [aoc.common :as c]))

(def test-input ["00100"
                 "11110"
                 "10110"
                 "10111"
                 "10101"
                 "01111"
                 "00111"
                 "11100"
                 "10000"
                 "11001"
                 "00010"
                 "01010"])

(defn key-of-max
  [m]
  (let [ones (get m \1 0)
        zeroes (get m \0 0)]
    (if (>= ones zeroes)
      \1
      \0)))

(defn key-of-min
  [m]
  (let [ones (get m \1 0)
        zeroes (get m \0 0)]
    (if (<= zeroes ones)
      \0
      \1)))

(comment
  (key-of-max {\0 3 \1 1})
  (key-of-max {\0 1 \1 4})
  (key-of-max {\0 2 \1 2})
  (key-of-max {\0 3})

  (key-of-min {\0 3 \1 1})
  (key-of-min {\0 1 \1 4})
  (key-of-min {\0 2 \1 2})
  (key-of-min {\0 3}))

(defn pick-one-bit
  [bits f]
  (let [groups (frequencies bits)]
    (f groups)))

(defn gamma
  [bits]
  (pick-one-bit bits key-of-max))

(defn eps
  [bits]
  (pick-one-bit bits key-of-min))

(defn get-nth-bit
  [i values]
  (map #(nth % i) values))

(defn word
  [f values]
  (let [n (-> values first count)
        bits (->> (range n)
                 (map #(get-nth-bit % values))
                 (map f))]
    bits))

(defn word->long
  [word]
  (reduce
    (fn [r v](+ (* 2 r) (if (= \1 v) 1 0)))
    0
    word))

(comment
  (word->long "1100"))

(defn solve
  [values]
  (let [g (->> values (word gamma) word->long)
        e (->> values (word eps) word->long)]
    (* g e)))

(defn bit-criteria
  [f i values]
  (->> values (get-nth-bit i) f))

(comment
  (bit-criteria eps 0 test-input)
  (bit-criteria eps 1 test-input)
  (bit-criteria gamma 1 test-input))

(defn match-bit-criteria
  [f i xs]
  (let [criteria (bit-criteria f i xs)]
    (filter #(= criteria (nth % i)) xs)))

(comment
  (match-bit-criteria gamma 0 test-input)
  (match-bit-criteria gamma 1 *1)
  (match-bit-criteria gamma 2 *1)
  (match-bit-criteria gamma 3 *1)
  (match-bit-criteria gamma 4 *1)
  
  (match-bit-criteria eps 2 ["01111"
                              "01010"])
  (eps [\1 \0]))

(defn rating
  [f values]
  (let [n (-> values first count)]
    (c/only-first 
      (reduce
        (fn [xs i]
          (let [remaining (match-bit-criteria f i xs)]
            (if (= 1 (count remaining))
              (reduced remaining)
              remaining)))
        values
        (range n)))))

(comment
  (rating gamma test-input))

(defn solve2
  [values]
  (let [oxygen (->> values (rating gamma) word->long)
        co2 (->> values (rating eps) word->long)]
    (* oxygen co2)))

(defn parse
  [f]
  (str/split-lines (slurp f)))

(comment
  (solve test-input)
  (solve (parse "/home/olivier/tmp/aoc/day3.txt"))
  ; part 2
  (solve2 test-input)
  (solve2 (parse "/home/oliv/tmp/aoc/day3.txt")))
