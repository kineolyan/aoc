fn compute_fuel(mass: i32) -> i32 {
    (mass / 3) - 2
}

fn compute_recurs_fuel(mass: i32) -> i32 {
    let mut remaining = mass;
    let seq = std::iter::from_fn(move || {
        remaining = compute_fuel(remaining);
        if remaining > 0 { Some(remaining) } else { None }
    });
    seq.sum()
}

fn solve<F>(values: &[i32], f: F) -> i32
    where F: Fn(i32) -> i32 {
    values.iter().map(|&mass| f(mass)).sum()
}

fn part1() -> util::AppResult<()> {
    let values = util::parse_list_of_numbers("day1.txt")?;
    let result = solve(&values, compute_fuel);
    println!("part1 = {}", result);
    Ok(())
}

fn part2() -> util::AppResult<()> {
    let values = util::parse_list_of_numbers("day1.txt")?;
    let result = solve(&values, compute_recurs_fuel);
    println!("part2 = {}", result);
    Ok(())
}

fn main() -> util::AppResult<()> {
    util::bootstrap_main()?;
    // part1()?;
    part2()?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_fuel() {
        assert_eq!(compute_fuel(12), 2);
        assert_eq!(compute_fuel(14), 2);
        assert_eq!(compute_fuel(1969), 654);
        assert_eq!(compute_fuel(100_756), 33_583);
    }

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve(&[12, 14, 1969], compute_fuel), 2 + 2 + 654);
    }

    #[test]
    fn test_compute_recurs_fuel() {
        assert_eq!(compute_recurs_fuel(12), 2);
        assert_eq!(compute_recurs_fuel(14), 2);
        assert_eq!(compute_recurs_fuel(1969), 966);
        assert_eq!(compute_recurs_fuel(100_756), 50346);
    }
}
