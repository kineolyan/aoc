// From https://doc.rust-lang.org/rust-by-example/trait/iter.html
pub struct IterSeq<T: Sized> {
    f: dyn Fn(T) -> Option<T>,
    value: Option<T>,
}

impl<T> Iterator for IterSeq<T> {
    type Item = &T;

    fn next(&mut self) -> Option<Self::Item> {
        self.value = self.f(self.value);
        if let Some(value) = &self.value {
            Some(value)
        } else {
            None
        }
    }
}

pub fn iterate<T>(start: T, f: F) -> IterSeq<T>
where F: Fn(T) -> Option<T>
{
    IterSeq { value: start, f }
}
