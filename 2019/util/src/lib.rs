extern crate dirs;
extern crate color_eyre;

use eyre::Result;
use std::path::{Path, PathBuf};
use std::vec::Vec;

mod clojure;

pub type AppResult<T> = color_eyre::Result<T>;

fn get_data_dir() -> PathBuf {
   let mut home = dirs::home_dir().expect("Cannot find user home :(");
   home.push("tmp");
    home.push("aoc");
    home
}

pub fn bootstrap_main() -> color_eyre::Result<()> {
    color_eyre::install()?;
    Ok(())
}

pub fn parse_list_of_numbers<P: AsRef<Path>, N: std::str::FromStr>(path: P) -> Result<Vec<N>> {
    let mut root = get_data_dir();
    root.push(path);
    let input_path = root.as_path();
    let input = std::fs::read_to_string(input_path)?;
    let lines = input.split("\n");
    lines
        .filter(|line| !line.is_empty())
        .map(|line| {
            line.parse::<N>()
                .map_err(|_e| color_eyre::eyre::eyre!("cannot parse {}", line))
        })
        .collect::<Result<Vec<N>>>()
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
